<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterFacilityListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_facility_list', function($table) {
			$table->increments('id');
			$table->string('facility_code', 10);
			$table->string('facility_name', 50);
			$table->string('province', 30);
			$table->string('county', 30);
			$table->string('district', 30);
			$table->string('division', 30);
			$table->string('type', 30);
			$table->string('owner', 30);
			$table->string('location', 30);
			$table->string('sub_location', 30);
			$table->text('description_of_location');
			$table->string('constituency', 30);
			$table->string('nearest_town', 30);
			$table->string('beds', 6);
			$table->string('cots', 6);
			$table->string('official_landline', 30);
			$table->string('official_fax', 30);
			$table->string('official_mobile', 30);
			$table->string('official_email', 30);
			$table->string('official_address', 30);
			$table->string('official_alternate_no', 30);
			$table->string('town', 30);
			$table->string('post_code', 30);
			$table->string('in_Charge', 30);
			$table->string('job_title_of_in_charge', 30);
			$table->string('open_24_hours', 5);
			$table->string('open_weekends', 5);
			$table->string('operational_status', 15);
			$table->string('ANC', 5);
			$table->string('ART', 5);
			$table->string('BEOC', 5);
			$table->string('BLOOD', 5);
			$table->string('CAES_SEC', 5);
			$table->string('CEOC', 5);
			$table->string('C_IMCI', 5);
			$table->string('EPI', 5);
			$table->string('FP', 5);
			$table->string('GROWM', 5);
			$table->string('HBC', 5);
			$table->string('HCT', 5);
			$table->string('IPD', 5);
			$table->string('OPD', 5);
			$table->string('OUTREACH', 5);
			$table->string('PMTCT', 5);
			$table->string('RAD_XRAY', 5);
			$table->string('RHTC_RHDC', 5);
			$table->string('TB_DIAG', 5);
			$table->string('TB_LABS', 5);
			$table->string('TB_TREAT', 5);
			$table->string('YOUTH', 30);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_facility_list');
	}

}
