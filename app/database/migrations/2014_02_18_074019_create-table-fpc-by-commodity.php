<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFpcByCommodity extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fpc_by_facility', function($table) {
			$table->increments('id');
		  	$table->string('Period_ID', 10) ;
		  	$table->string('Organisation_unit_ID', 10);
		  	$table->string('Period_UID', 10); 
		  	$table->string('Organisation_unit_UID', 25);
		  	$table->string('Period', 10);
		  	$table->string('Organisation_unit', 50);
		  	$table->string('Period_code', 10); 
		  	$table->string('Organisation_unit_code', 10) ;
		  	$table->string('Period_description', 50);
		  	$table->string('Organisation_unit_description', 100);
		  	$table->string('Reporting_month', 10);
		  	$table->string('Organisation_unit_parameter', 25);
		  	$table->string('Organisation_unit_is_parent', 5);
		  	$table->string('FP_Injections', 10);
		  	$table->string('Pills_Microlut', 10); 
		  	$table->string('Pills_Microgynon', 10);
		  	$table->string('IUCD_insertion', 10);
		  	$table->string('IUCD_Removals', 10) ;
		  	$table->string('Implants_insertion', 10) ;
		  	$table->string('Implants_Removal', 10); 
		  	$table->string('Sterilization_BTL', 10); 
		  	$table->string('Steriliz_Vasectomy', 10); 
		  	$table->string('NFP', 10); 
		  	$table->string('All_others_FP', 10); 
		  	$table->string('Clints_condom', 10); 
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fpc_by_facility');
	}

}
