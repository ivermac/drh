<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(
        	array(
        		'firstname' => 'chuck',
        		'lastname' => 'norris',
        		'phonenum' => '0700000000',
        		'organisation' => 'clinton health access initiative',
                'email' => 'admin@admin.com',
        		'usertype' => 1,
        		'password' => Hash::make('12345678'),
        	)
        );

        User::create(
            array(
                'firstname' => 'demo',
                'lastname' => 'account',
                'phonenum' => '0700000000',
                'organisation' => 'clinton health access initiative',
                'email' => 'guest@admin.com',
                'usertype' => 1,
                'password' => Hash::make('12345678'),
            )
        );

        User::create(
            array(
                'firstname' => 'you',
                'lastname' => 'guy',
                'phonenum' => '0700000000',
                'organisation' => 'clinton health access initiative',
                'email' => 'data@clerk.com',
                'usertype' => 2,
                'password' => Hash::make('12345678'),
            )
        );

         User::create(
            array(
                'firstname' => 'the',
                'lastname' => 'viewer',
                'phonenum' => '0700000000',
                'organisation' => 'clinton health access initiative',
                'email' => 'viewer@viewer.com',
                'usertype' => 3,
                'password' => Hash::make('12345678'),
            )
        );
    }
}