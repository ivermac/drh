<?php
Route::get('/my-queue', function(){
	$listener_running = "tasklist | find \"php.exe\"";
	$output = shell_exec($listener_running);
	Log::info(getcwd());
	if (empty($output))
		$exec_output = shell_exec("php artisan queue:listen");
	Queue::push('GenerateMonthlyReport');
	Queue::push('SendEmail');
	var_dump("Report queued for generation");
});

class GenerateMonthlyReport {
	public function fire($job) {
		chdir('public');
		Report::testShellExecution();
		$job->delete();
	}
}

class SendEmail{
	public function fire($job) {
		SendEmail::withNationalSummaryReport();
		$job->delete();
	}
}

// routes for home controller
Route::group(array('prefix' => '/'), function()
{
	Route::get('', function(){ return View::make('landing-page'); });
	Route::get(
		'home/family-planning-stock-status', 
		array('as' => 'family-planning-stock-status', 'uses' => 'HomeController@showFamilyPlanningStockStatus'));
	Route::post(
		'family-planning-stock/delete', 
		array('as' => 'family-planning-stock-delete', 'uses' => 'HomeController@deleteFamilyPlanningStockData'));
	Route::post(
		'LMIS-data/delete', 
		array('as' => 'LMIS-data-delete', 'uses' => 'HomeController@deleteLMISData'));
	Route::match(array('GET', 'POST'),  'two-pager-pdf', array('as' => 'two-pager-pdf', 'uses' => 'HomeController@twoPagerPDF'));
	Route::get('rht-county-summary/{training}', array('as' => 'rht-county-summary', 'uses' => 'HomeController@rhtCountySummary'));
	Route::get('commodity-summary', array('as' => 'commodity-summary', 'uses' => 'HomeController@commoditySummary'));
	Route::get('custom-summary', array('as' => 'custom-summary', 'uses' => 'HomeController@customSummary'));
});

// routes for statitiscs page / dashboard
Route::group(array('prefix' => 'statistics'), function()
{
	Route::post('/generate-pie-chart-national-data', array('as' => 'generate-pie-chart-national-data', 'uses' => 'AJAXController@getPieChartNationalData'));
	Route::post('/generate-bar-graph-by-county', array('as' => 'generate-bar-graph-by-county', 'uses' => 'AJAXController@getBarGraphByCounty'));
	Route::post('/generate-bar-graph-by-consumption', array('as' => 'generate-bar-graph-by-consumption', 'uses' => 'AJAXController@getBarGraphByConsumption'));
	Route::post('/get-districts-in-county', array('as' => 'get-districts-in-county', 'uses' => 'AJAXController@getDistrictsInCounty'));
	Route::post('/get-counties-in-province', array('as' => 'get-counties-in-province', 'uses' => 'AJAXController@getCountiesInProvince'));
	Route::post('/get-facilities-in-district', array('as' => 'get-facilities-in-district', 'uses' => 'AJAXController@getFacilitiesInDistrict'));
	Route::post('/get-line-consumption-trend', array('as' => 'get-line-graph-consumption-trend', 'uses' => 'AJAXController@getLineGraphConsumptionTrend'));
	Route::post('/get-stock-status-data', array('as' => 'get-stock-status-data', 'uses' => 'AJAXController@getStockStatusData'));
	Route::post('/get-supply-plan-vs-mos', array('as' => 'get-supply-plan-vs-mos', 'uses' => 'AJAXController@getSupplyPlanVsMos'));
	Route::post('/deliver-chart', array('as' => 'deliver-chart', 'uses' => 'AJAXController@deliverChart'));

	Route::post('/get-percentage-of-staff-trained', array('as' => 'get-percentage-of-staff-trained', 'uses' => 'AJAXController@getPercentageOfStaffTrained'));
	Route::post(
		'/get-percentage-of-facilities-with-atleast-one-person-trained', 
		array(
			'as' => 'get-percentage-of-facilities-with-atleast-one-person-trained', 
			'uses' => 'AJAXController@getPercentageOfFacilitiesWithAtleastOnePersonTrained'
		)
	);

	Route::post('/export-csv-to-mysql', array('as' => 'export-csv-to-mysql', 'uses' => 'AJAXController@exportCSVToMysql'));
	Route::post(
		'/get-facility-ownership-rht-training-breakdown', 
		array('as' => 'get-facility-ownership-rht-training-breakdown', 'uses' => 'AJAXController@getFacilityOwnershipRHTTrainingBreakdown'));
	Route::post('/get-lmis-mos-data', array('as' => 'get-lmis-mos-data', 'uses' => 'AJAXController@getLMISMOSData'));
});

// routes for user controller
Route::group(array('prefix' => 'users'), function()
{
	Route::post('/login', array('as' => 'users-login', 'uses' => 'UsersController@login'));
	
	Route::get('/csv', array('as' => 'csv-handle', 'uses' => 'UsersController@csv'));
	Route::get('/logout', array('as' => 'users-logout', 'uses' => 'UsersController@logout'));
});

// routes for admin controller
Route::group(array('prefix' => 'admin'), function()
{
	Route::get('', ['before' => 'auth.basic', function(){ 
		return "welcome to the admin page"; 
	}]);
	Route::group(array('prefix' => '/facilities'), function()
	{
		Route::get('/list', array('as' => 'list-facilities', 'uses' => 'AdminController@listFacilities'));
		Route::get('/new', array('as' => 'new-facility', 'uses' => 'AdminController@newFacilities'));
	});

	Route::group(array('prefix' => '/training'), function()
	{
		// processors
		Route::post('/add-training', array('as' => 'training-add', 'uses' => 'AdminController@addTraining'));
		Route::post('/edit-training', array('as' => 'training-edit', 'uses' => 'AdminController@postEditTraining'));

		// displayers
		Route::get('/list', array('as' => 'list-training', 'uses' => 'AdminController@listTraining'));
		Route::get('/new', array('as' => 'new-training', 'uses' => 'AdminController@newTraining'));
		Route::get('/delete/{id}', array('as' => 'delete-training', 'uses' => 'AdminController@deleteTraining'));
		Route::get('/edit/{id}', array('as' => 'edit-training', 'uses' => 'AdminController@editTraining'));
	});
});

// routes for commodities
Route::group(array('prefix' => 'commodities'), function()
{
	// processors
	Route::post('/rht-search', array('as' => 'rht-search', 'uses' => 'POSTController@postRHTSearch'));
	Route::post('/get-reproductive-health-training-details', 
			array('as' => 'get-reproductive-health-training-details', 
			'uses' => 'AJAXController@postReproductiveHealthTrainingDetails'));
	Route::post('/post-stock-on-hand', array('as' => 'post-stock-on-hand', 'uses' => 'POSTController@postStockOnHand'));
	Route::post('/post-new-consignment', array('as' => 'post-new-consignment', 'uses' => 'POSTController@postNewConsignment'));
	Route::post('/pipeline/edit', array('as' => 'post-edit-pipeline', 'uses' => 'POSTController@postEditPipeline'));
	Route::post('/funding-sources/add', array('as' => 'post-funding-sources', 'uses' => 'POSTController@postNewfundingSources'));
	Route::post('/add', array('as' => 'post-new-commodity', 'uses' => 'POSTController@postNewCommodity'));
	Route::post('/stock-on-hand/get', array('as' => 'get-stock-on-hand-record', 'uses' => 'AJAXController@getStockOnHandRecord'));
	
	// displayers
	Route::get('/received-commodities-listing', array('as' => 'received-commodities-listing', 'uses' => 'CommoditiesController@receivedCommoditiesListing'));
	Route::get('/dashboard', array('as' => 'commodities-dashboard', 'uses' => 'CommoditiesController@showDashboard'));
	Route::get('/training-details', array('as' => 'training-details', 'uses' => 'CommoditiesController@postTrainingDetails'));
	Route::get('/add-stock-on-hand', array('as' => 'add-stock-on-hand', 'uses' => 'CommoditiesController@addStockOnHand'));
	Route::get('/supply-plan/delete/{id}', array('as' => 'delete-supply-plan', 'uses' => 'CommoditiesController@deleteSupplyPlan'));
	Route::match(array('GET', 'POST'), '/supply-plan/list', array('as' => 'supply-plan-listing', 'uses' => 'CommoditiesController@listSupplyPlan'));
	Route::match(array('GET', 'POST'), '/detailed-stock-on-hand', array('as' => 'detailed-stock-on-hand', 'uses' => 'CommoditiesController@detailedStockOnHand'));
	Route::get('/stock-on-hand/delete/{id}', array('as' => 'delete-stock-on-hand', 'uses' => 'CommoditiesController@deleteStockOnHand'));
	Route::get('/pipeline/edit/{id}', array('as' => 'edit-pipeline', 'uses' => 'CommoditiesController@editPipeline'));
	Route::get('/add-new-consignment', array('as' => 'add-new-consignment', 'uses' => 'CommoditiesController@addNewConsignment'));
	Route::get('/listing', array('as' => 'commodities-listing', 'uses' => 'CommoditiesController@listCommodities'));
	Route::get('/delete/{id}', array('as' => 'delete-commodity', 'uses' => 'CommoditiesController@deleteCommodity'));
	Route::get('/funding-sources/delete/{id}', array('as' => 'delete-funding-source', 'uses' => 'CommoditiesController@deleteFundingSource'));
	Route::get('/funding-sources/listing', array('as' => 'funding-sources-listing', 'uses' => 'CommoditiesController@listFundingSources'));

	// routes for reproductive health training
	Route::group(array('prefix' => '/reproductive-health-training'), function()
	{
		// processors
		Route::post('/add-rht', array('as' => 'add-reproductive-health-training', 'uses' => 'POSTController@postAddTrainingDetails'));
		Route::post('/edit-rht', array('as' => 'edit-reproductive-health-training', 'uses' => 'POSTController@postEditTrainingDetails'));
		
		// displayers
		Route::get('/download-rht-csv', array('as' => 'download-rht-csv', 'uses' => 'CommoditiesController@downloadReproductiveHealthTrainingCsv'));
		Route::get('/new', array('as' => 'reproductive-health-training-new', 'uses' => 'CommoditiesController@addReproductiveHealthTraining'));
		Route::get('/delete/{id}', array('as' => 'reproductive-health-training-delete', 'uses' => 'CommoditiesController@deleteReproductiveHealthTraining'));
		Route::get('/edit/{id}', array('as' => 'reproductive-health-training-edit', 'uses' => 'CommoditiesController@editReproductiveHealthTraining'));
		Route::match(array('GET', 'POST'), '/list', array('as' => 'reproductive-health-training-list', 'uses' => 'CommoditiesController@listReproductiveHealthTraining'));
		Route::get('/dashboard', array('as' => 'reproductive-health-training-dashboard', 'uses' => 'CommoditiesController@dashboardReproductiveHealthTraining'));
	});

});

// routes for settings
Route::group(array('prefix' => 'settings'), function()
{
	Route::get('/supply-chain', array('as' => 'supply-chain', 'uses' => 'SettingsController@showSupplyChain'));
	Route::get('/stock-on-hand', array('as' => 'stock-on-hand', 'uses' => 'SettingsController@showStockOnHand'));
	Route::get('/other-settings', array('as' => 'other-settings', 'uses' => 'SettingsController@showOtherSettings'));
});

// routes for settings
Route::group(array('prefix' => 'lmis'), function()
{
	Route::get('/home', array('as' => 'lmis-home', 'uses' => 'LMISController@showHome'));
	
}); 

Route::group(array('prefix' => 'report'), function()
{
	Route::get('', array('as' => 'test-shell-execution', 'uses' => 'ReportController@testShellExecution'));
	
}); 

