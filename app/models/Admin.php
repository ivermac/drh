<?php

class Admin extends Eloquent {
	
	// public static $unguarded = true;
	public static function add($inputs) {
		$name = $inputs['training-name'];
		$identifier = $inputs['identifier'];
		DB::table('trainings')
			->insert(
				array(
					'name' => $name,
					'identifier' => $identifier,
				)
			);
	}

	public static function edit($inputs) {
		$name = $inputs['training-name'];
		$identifier = $inputs['identifier'];
		$id = $inputs['record-id'];

		DB::table('trainings')
	        ->where('id', $id)
	        ->update(
	        	array(
	        		'name' => $name,
					'identifier' => $identifier,
	        	)
	        );
	}

	public static function del($id) {
		DB::table('trainings')->where('id', '=', $id)->delete();
	}
}