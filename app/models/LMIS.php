<?php

class LMIS extends Eloquent {
	protected $table = 'lmis';
	/*
	Aim of the following function is to have an sql that looks like the following so that it can executed to retrieve desired results:
	SELECT 
		sum(`Combined Oral contraceptive Pills, FP (Ending Balance)`) / 
		(
			(
				(SELECT sum(`Combined Oral contraceptive Pills, FP (Dispensed)`) from `lmis` where `Period code` = 201404) + 
	     		(SELECT sum(`Combined Oral contraceptive Pills, FP (Dispensed)`) from `lmis` where `Period code` = 201403) +
	     		(SELECT sum(`Combined Oral contraceptive Pills, FP (Dispensed)`) from `lmis` where `Period code` = 201402)
	     	)/3
	     ) as `Combined Oral contraceptive Pills, FP`
	FROM `lmis` 
	where `Period code` = 201404
	*/
	public static function getLMISMOS($period, $county = "", $district = "") {
		//this closure subtracts '$num' months from period then returns date with YYYYMM format 
		$getPeriod = function($num) use($period){
			$date = new DateTime($period);
			$date->sub(new DateInterval("P".$num."M"));
			return $date->format('Ym');
		};

		$commodities = ['Combined Oral contraceptive Pills, FP', 'Cycle Beads, FP', 'Emergency Contraceptive pills', 'Female Condoms, FP',
						'IUCDs, FP', 'Implants (1-Rod), FP', 'Implants (2-Rod), FP', 'Injectables, FP', 'Male Condoms, FP', 'Progestin only pills, FP'];
		// 'Others, FP',

		// moving average calcalator
		foreach ($commodities as $commodity) {
			$subquery = "(";
			for ($i=0; $i < 3; $i++) { 
				$subquery.= "(SELECT sum(`$commodity (Dispensed)`) from `lmis_summary` where `Period code` = ".$getPeriod($i);
				if (!empty($county)) {
					$subquery.= " and `county` = '$county' ";
					if (!empty($district)) {
						$subquery.= " and `district` = '$district' ";
					}
				}
				$subquery.= ($i + 1 == 3) ? ") ": ") + ";
			}
			$subquery.= ")/3";
			
			$rs = DB::table('lmis_summary')
				->select(
					DB::raw(
						"sum(`$commodity (Ending Balance)`) / ($subquery) as `$commodity`"
					)
				);

			if (!empty($county)) {
				$rs = $rs->where("county", "=", $county);
				if (!empty($district)) {
					$rs = $rs->where("district", "=", $district);
				}
			}

			$rs = $rs->where("Period code", "=", $getPeriod(0));
			$rs = (array)$rs->first();
			$output[$commodity] = round((empty($rs[$commodity])) ? 0 : $rs[$commodity], 2);
		}

		$truncateString = function($commodity){
			$pattern = ['/, FP/'];
			$replacement = [''];
			return preg_replace($pattern, $replacement, $commodity);
		};

		return [
			"commodities" => array_map($truncateString, array_keys($output)),
			"mos" => array_values($output),
			"county" => $county,
			"district" => $district,
			"period" => $period
			];
	}
}