<?php

class FPC extends Eloquent  {
	protected $table = 'fpc_by_facility';
	
    // TODO: create models; this model is clustered with functions not related to FPC

    // TODO: write comments for the functions of this class for easy understandability and refactoring
    // when need be

    //FIX ME : 
    public static function getReceivedCommoditiesData() {
        $results = DB::table('pipeline as p')
            ->select(
                DB::raw(
                   'p.id as id,
                    f.fp_name as commodity,
                    f.unit as unit, fs.funding_source as funding_source, 
                    p.fp_date as ETA, 
                    p.date_receive as date_received, 
                    DATEDIFF(p.date_receive, p.fp_date) as days_delayed, 
                    p.fp_quantity as qty_expected, 
                    p.qty_receive as qty_received,
                    (p.fp_quantity - p.qty_receive) as qty_remaining'
                )
            )
            ->join('fpcommodities as f', 'f.id', '=', 'p.fpcommodity_Id')
            ->join('funding_sources as fs', 'fs.id', '=', 'p.funding_source')
            ->where('p.transaction_type', 'like', '%RECEIVED%')
            ->orderBy('p.date_receive', 'desc')
            ->paginate(10);
        return $results;
    }

	public static function getCommoditiesTotals($year, $county = "all") {
        $commodities  = static::getFamilyPlanningCommodities();
        $totals = [];
        $removals = [];
        $result_set = DB::table('fp_commodities')->get();

        $commodities_alias = static::getFamilyPlanningCommoditiesAliases();

        $append_sum = function($column) {
            return "sum($column) as $column";
        };
        $columns = implode(", ", array_map($append_sum, array_keys($commodities)));
        if ($county == "all"){
            $result = get_object_vars(
                        DB::table('fpc_by_facility')
                         ->select(DB::raw($columns))
                         ->where('Period_code', 'LIKE', "%$year%")
                         ->first()
                    );
        } else {
            $result = get_object_vars(
                        DB::table('county_summary')
                         ->select(DB::raw($columns))
                         ->where('Period_code', 'LIKE', "%$year%")
                         ->where('county', '=', "$county")
                         ->first()
                    );
        }

        foreach ($commodities as $commodity => $total) {
            $alias = $commodities_alias[$commodity];
            if (in_array($alias, ['NFP', 'IUCD Removals', 'Implants Removals'])){
                $removals[$alias] = round($result[$commodity]/$total);
                continue;
            }
            array_push($totals, array($alias, round($result[$commodity]/$total)));
        }

        return ["totals" => $totals, "removals" => $removals];
    }

    // FIXME: move to a new model
    public static function exportCSVToMysql($filename, $module) {
        $file_path = addslashes(
            public_path()
            .DIRECTORY_SEPARATOR."php"
            .DIRECTORY_SEPARATOR."files"
            .DIRECTORY_SEPARATOR.$filename
        );
        if ($module == "service-statistics") {
            $query = "LOAD DATA INFILE '$file_path' 
                      INTO TABLE fpc_by_facility 
                      FIELDS TERMINATED BY ',' 
                      LINES TERMINATED BY '\\r\\n' 
                      IGNORE 1 LINES 
                      (Period_ID,Organisation_unit_ID,Period_UID,Organisation_unit_UID,Period,
                        Organisation_unit,Period_code,Organisation_unit_code,Period_description,
                        Organisation_unit_description,Reporting_month,Organisation_unit_parameter,
                        Organisation_unit_is_parent,FP_Injections,Pills_Microlut,Pills_Microgynon,
                        IUCD_insertion,IUCD_Removals,Implants_insertion,Implants_Removal,
                        Sterilization_BTL,Steriliz_Vasectomy,NFP,All_others_FP,Clints_condom);";
        } else if ($module == "lmis") {
            $query = "LOAD DATA INFILE '$file_path' 
                      INTO TABLE lmis 
                      FIELDS TERMINATED BY ',' 
                      LINES TERMINATED BY '\\r\\n' 
                      IGNORE 1 LINES 
                      (`Organisation unit ID`, `Organisation unit`, `Organisation unit code`, `Organisation unit description`, 
                        `Period ID`, `Period`, `Period code`, `Period description`, `Combined Oral contraceptive Pills, FP (Beginning Balance)`, 
                        `Combined Oral contraceptive Pills, FP (Received This Month)`, `Combined Oral contraceptive Pills, FP (Dispensed)`, 
                        `Combined Oral contraceptive Pills, FP (Losses)`, `Combined Oral contraceptive Pills, FP (Positive)`, 
                        `Combined Oral contraceptive Pills, FP (Negative)`, `Combined Oral contraceptive Pills, FP (Ending Balance)`, 
                        `Combined Oral contraceptive Pills, FP (Quantity Requested)`, `Cycle Beads, FP (Beginning Balance)`, 
                        `Cycle Beads, FP (Received This Month)`, `Cycle Beads, FP (Dispensed)`, `Cycle Beads, FP (Losses)`, 
                        `Cycle Beads, FP (Positive)`, `Cycle Beads, FP (Negative)`, `Cycle Beads, FP (Ending Balance)`, 
                        `Cycle Beads, FP (Quantity Requested)`, `Emergency Contraceptive pills (Beginning Balance)`, 
                        `Emergency Contraceptive pills (Received This Month)`, `Emergency Contraceptive pills (Dispensed)`, 
                        `Emergency Contraceptive pills (Losses)`, `Emergency Contraceptive pills (Positive)`, 
                        `Emergency Contraceptive pills (Negative)`, `Emergency Contraceptive pills (Ending Balance)`, 
                        `Emergency Contraceptive pills (Quantity Requested)`, `Female Condoms, FP (Beginning Balance)`, 
                        `Female Condoms, FP (Received This Month)`, `Female Condoms, FP (Dispensed)`, `Female Condoms, FP (Losses)`, 
                        `Female Condoms, FP (Positive)`, `Female Condoms, FP (Negative)`, `Female Condoms, FP (Ending Balance)`, 
                        `Female Condoms, FP (Quantity Requested)`, `IUCDs, FP (Beginning Balance)`, `IUCDs, FP (Received This Month)`, 
                        `IUCDs, FP (Dispensed)`, `IUCDs, FP (Losses)`, `IUCDs, FP (Positive)`, `IUCDs, FP (Negative)`, 
                        `IUCDs, FP (Ending Balance)`, `IUCDs, FP (Quantity Requested)`, `Implants (1-Rod), FP (Beginning Balance)`, 
                        `Implants (1-Rod), FP (Received This Month)`, `Implants (1-Rod), FP (Dispensed)`, `Implants (1-Rod), FP (Losses)`, 
                        `Implants (1-Rod), FP (Positive)`, `Implants (1-Rod), FP (Negative)`, `Implants (1-Rod), FP (Ending Balance)`, 
                        `Implants (1-Rod), FP (Quantity Requested)`, `Implants (2-Rod), FP (Beginning Balance)`, 
                        `Implants (2-Rod), FP (Received This Month)`, `Implants (2-Rod), FP (Dispensed)`, `Implants (2-Rod), FP (Losses)`, 
                        `Implants (2-Rod), FP (Positive)`, `Implants (2-Rod), FP (Negative)`, `Implants (2-Rod), FP (Ending Balance)`, 
                        `Implants (2-Rod), FP (Quantity Requested)`, `Injectables, FP (Beginning Balance)`, `Injectables, FP (Received This Month)`, 
                        `Injectables, FP (Dispensed)`, `Injectables, FP (Losses)`, `Injectables, FP (Positive)`, `Injectables, FP (Negative)`, 
                        `Injectables, FP (Ending Balance)`, `Injectables, FP (Quantity Requested)`, `Male Condoms, FP (Beginning Balance)`, 
                        `Male Condoms, FP (Received This Month)`, `Male Condoms, FP (Dispensed)`, `Male Condoms, FP (Losses)`, 
                        `Male Condoms, FP (Positive)`, `Male Condoms, FP (Negative)`, `Male Condoms, FP (Ending Balance)`, 
                        `Male Condoms, FP (Quantity Requested)`, `Others, FP (Beginning Balance)`, `Others, FP (Received This Month)`, 
                        `Others, FP (Dispensed)`, `Others, FP (Losses)`, `Others, FP (Positive)`, `Others, FP (Negative)`, `Others, FP (Ending Balance)`, 
                        `Others, FP (Quantity Requested)`, `Progestin only pills, FP (Beginning Balance)`, `Progestin only pills, FP (Received This Month)`, 
                        `Progestin only pills, FP (Dispensed)`, `Progestin only pills, FP (Losses)`, `Progestin only pills, FP (Positive)`, 
                        `Progestin only pills, FP (Negative)`, `Progestin only pills, FP (Ending Balance)`, `Progestin only pills, FP (Quantity Requested)`);
                      ";
            Log::info("file path >>> $file_path");
        }
        return DB::connection()->getpdo()->exec($query);
    }

    // TODO: comment source code
    public static function getSupplyPlanVsMos($year, $commodity_id) {
        $selctedYear = intval($year);
        $returnArray = [];

        $rs = DB::table('fpcommodities')->where('id', '=', $commodity_id)->first();

        // months of the fiscal year
        $months = ['07', '08', '09', '10', '11', '12', '01', '02', '03', '04', '05', '06'];
        $fiscal_year = [];
        foreach ($months as $index => $month) {
            $fiscal_month = "$selctedYear-$month";
            array_push($fiscal_year, $fiscal_month);
            if ($index == 5 || $index == '5') $selctedYear++;
        }
        $returnArray['fiscalYear'] = $fiscal_year;
        $array1 = []; //check if this can be refactored
        $SOH = ['KEMSA' => []];
        $SP = ['KEMSA' => [], 'PSI' => []];
        foreach ($SOH as $supplier => $array) {
            $soh_supplier = 'SOH'.$supplier;
            $sp_supplier = 'SP'.$supplier;
            // FIX ME: 
            // 1.would be better and efficient if one query could be used instead of the iteration
            foreach ($fiscal_year as $yyyy_mm) {
                $results = DB::table('pipeline as pl')
                            ->select(
                                DB::raw("
                                  sum(pl.fp_quantity/fpc.projected_monthly_c) as mos, 
                                  pl.fpcommodity_Id as fpc_id"
                                )
                            )
                            ->join('fpcommodities as fpc', 'pl.fpcommodity_Id', '=', 'fpc.id')
                            ->where('pl.fpcommodity_Id', '=', $commodity_id)
                            ->where('pl.transaction_type', '=', $soh_supplier)
                            ->where('pl.fp_date', 'LIKE', "%$yyyy_mm%")
                            ->first();
                $mos = ($results->mos == null) ? 0 : $results->mos;
                array_push($array, floatval($mos));
           
                if (substr($yyyy_mm, -2, 2) == "07") {
                    $supply_plan_value = $mos;
                } else {
                    $rst = DB::table('pipeline as pl')
                            ->select(
                                DB::raw(
                                   ' sum(pl.fp_quantity/fpc.projected_monthly_c) as mos'
                                )
                            )
                            ->join('fpcommodities as fpc', 'fpc.id', '=', 'pl.fpcommodity_Id')
                            ->where(function($query) use($yyyy_mm, $commodity_id)
                            {
                                $query->where(function($qry){
                                            $qry->where('pl.transaction_type', 'like', "%PENDING%")
                                                ->orWhere('pl.transaction_type', 'like', "%DELAYED%")
                                                ->orWhere('pl.transaction_type', 'like', "%INCOUNTRY%");
                                       })
                                      ->where('pl.fp_date', 'like', "%$yyyy_mm%")
                                      ->where('pl.store_id', '=', "1")
                                      ->where('pl.fpcommodity_Id', '=', $commodity_id);
                            })
                            ->orWhere(function($query) use($yyyy_mm, $commodity_id)
                            {
                                $query->where(function($qry) use($yyyy_mm){
                                            $qry->where('pl.date_receive', 'like', "%$yyyy_mm%")
                                                ->where('pl.transaction_type', 'like', "%RECEIVED%");
                                       })
                                      ->where('pl.store_id', '=', "1")
                                      ->where('pl.fpcommodity_Id', '=', $commodity_id);
                            })
                            ->first();
                    if ($rst->mos == null) {
                        if (intval($supply_plan_value) > 0) {
                            $supply_plan_value = $supply_plan_value - 1;
                        } else {
                            $supply_plan_value = 0;
                        }
                    } else {
                        $supply_plan_value = $rst->mos;
                    }
                }
                array_push($array1, floatval($supply_plan_value));
            }
            $returnArray[$soh_supplier] = $array;
            $returnArray[$sp_supplier] = $array1;
            $returnArray['commodity'] = $rs->fp_name;
        }
        return $returnArray;
    }
   
    // TODO: comment source code
    // FIX: decouple and use functions
    public static function getStockStatusData($supplier, $dateSelected){
        $rs = DB::table('fpcommodities')->get();
        $commodities = [];
        $actual_stock = [];
        $pending_stock = [];
        $container = '';
      
        // $date_today = date("Y-m-d");
        $date_today = $dateSelected;
        $date = date_create($date_today);
        date_add($date, date_interval_create_from_date_string('2 years'));
        $two_years_from_today = date_format($date, 'Y-m-d');

        $this_month = date("m",strtotime($date_today));
        $this_month_full_name = date("F",strtotime($date_today));
        $this_day = date("d",strtotime($date_today));
        $this_year = date("Y",strtotime($date_today));

        foreach ($rs as $row) {
            $commodities[$row->id] = $row->fp_name;
        }

        // container and title assignment
        if ($supplier == "kemsa") {
            $container = "public-sector";
            $title = "Stock status of KEMSA stores as at $this_month_full_name, $this_year";
        } else if ($supplier == "psi") {
            $container = "private-sector";
            $title = "Stock status of PSI stores as at $this_month_full_name, $this_year";
        }

        // actual stock
        $result_set = DB::table('pipeline as pipeline')
                        ->select(
                            DB::raw(
                                "SUM( fp_quantity ) / if (store_id = 1, fpcommodities.projected_monthly_c, fpcommodities.projected_psi) as actual_stock,
                                fpcommodity_Id as commodity_id"))
                        ->join('fpcommodities as fpcommodities', 'fpcommodities.id', '=', 'pipeline.fpcommodity_Id')
                        ->where('transaction_type', 'like', '%SOH%');

        if ($supplier == "kemsa") {
            $result_set = $result_set->where('store_id', '=', '1');
        } else if ($supplier == "psi") {
            $result_set = $result_set->where('store_id', '=', '2');
        }

        $result_set = $result_set->whereRaw('MONTH(fp_date) = ?', [$this_month])
                                 ->whereRaw('YEAR(fp_date) = ?', [$this_year])
                                 ->groupBy('commodity_id')
                                 ->get();
        foreach ($result_set as $row) {
            $actual_stock[$row->commodity_id] = $row->actual_stock;
        }

        // pending stock
        $result_set = DB::table('pipeline as pipeline')
                        ->select(DB::raw("SUM( fp_quantity ) / fpcommodities.projected_monthly_c as pending_stock,  fpcommodity_Id as commodity_id"))
                        ->join('fpcommodities as fpcommodities', 'fpcommodities.id', '=', 'pipeline.fpcommodity_Id');

        if ($supplier == "kemsa") {
            $result_set = $result_set->where(function($query) {
                                $query->where('transaction_type', 'like', '%PENDING%')
                                      ->where('store_id', '=', '1');
                         });
            $result_set = $result_set->orWhere(function($query)
                            {
                                $query->where('transaction_type', 'like', '%DELAYED%')
                                      ->where('store_id', '=', '1');
                            });

        } else if ($supplier == "psi") {
            $result_set = $result_set->where(function($query) {
                                $query->where('transaction_type', 'like', '%PENDING%')
                                      ->where('store_id', '=', '2');
                            });
            /*$result_set = $result_set->Where(function($query) {
                                $query->where('transaction_type', 'like', '%DELAYED%')
                                      ->where('store_id', '=', '2');
                            });*/
        }

        $result_set = $result_set->whereBetween('fp_date', array($date_today, $two_years_from_today))
                                 ->where('date_incountry', '=', '0000-00-00')
                                 ->where('date_receive', '=', '0000-00-00')
                                 ->groupBy('fpcommodity_Id')
                                 ->get();

        foreach ($result_set as $row) {
            $pending_stock[$row->commodity_id] = $row->pending_stock;
        }

        return array(
               'commodities' => $commodities, 
               'actual_stock' => $actual_stock, 
               'pending_stock' => $pending_stock,
               'title' => $title,
               'date_selected' => $dateSelected,
               'supplier' => $supplier,
               'container' => $container);
    }

    public static function getConsumptionTrends($county, $year, $commodity) {
        $column = 'fpc.'.$commodity;
        $result_set = DB::table('fpc_by_facility as fpc')
                     ->select(DB::raw("fpc.Period_code as period, SUM( $column ) as total"))
                     ->join('facilities as facilities', 'facilities.facility_code' ,'=', 'fpc.Organisation_unit_code')
                     ->join('districts as districts', 'districts.id', '=', 'facilities.district')
                     ->join('counties as counties', function($join) use($county)
                     {
                        $join->on('counties.id', '=', 'districts.county')
                             ->where('counties.county', '=', "$county");
                     })
                     ->where('fpc.Period_code', 'LIKE',  "%$year%")
                     ->groupBy('fpc.Period_code')
                     ->get();

        $data = [];
        foreach ($result_set as $row) {
            $data[$row->period] = intval($row->total);
        }
        return $data;
    }

    //FIX ME : change to use only 1 sql, use join with result having facility, district and county
    public static function getReproductiveHealthTrainingDetails($id) {
        $result = DB::table('rhtraining')->where('id', '=', $id)->first();
        $details = [];
        $facility = DB::table('facilities')->where('facility_code', '=', $result->facility_code)->first();
        $details['first_name'] = $result->first_name;
        $details['surname'] = $result->surname;
        $details['personnel_no'] = $result->personnel_no;
        $details['qualification'] = $result->qualification;
        $details['station'] = $result->station;
        $details['training'] = json_decode($result->training, true);
        $details['facility_name'] = $facility->facility_name;
        $details['id'] = $result->id;
        return $details;
    }

    public static function getConsumptionByCounty($commodity, $year, $month) {
        $data = [];
        $former_province = [];
        $totals = [];

        $results = DB::table('former_provinces')->get();
        foreach ($results as $row) {
            $data[$row->name] = []; 
            $former_province[$row->name] = []; 
        }

        $results = DB::table('county_summary')
                     ->select(DB::raw("county, former_province, sum($commodity) as commodity_amount"));
        if ($month == "00" || $month == 00) {
            $results = $results->where('Period_code', 'like', "%$year%");
        } else {
            $results = $results->where('Period_code', 'like', "%$year$month%");
        }
        $results = $results->groupBy('county')
                           ->get();
        foreach ($results as $row) {
            array_push($data[$row->former_province], array($row->county, intval($row->commodity_amount)));
            array_push($former_province[$row->former_province], intVal($row->commodity_amount));
        }

        foreach ($former_province as $province => $totals_array) {
            $totals[$province] = array_sum($totals_array);
        }
        return array('drilldown' => $data, 'totals' => $totals);
    }

    public static function getTotalsByCounty($county, $year, $month){
        $commodities  = static::getFamilyPlanningCommodities();
        $commodities_alias = static::getFamilyPlanningCommoditiesAliases();
        $districts = [];
        $fpc_details = [];
        $all_data = [];
        $selected_columns = "district, county";
        foreach ($commodities as $commodity => $value) {
            $fpc_details[$commodity] = [];
            $selected_columns.=", sum($commodity) as $commodity";
        }

        $result_set = DistrictSummary::select(DB::raw($selected_columns))
                            ->where('county', 'like', "%$county%");

        if ($month == "00" || $month == 00) {
            $result_set = $result_set->where('Period_code', 'like', "%$year%");
        } else {
            $result_set = $result_set->where('Period_code', 'like', "%$year$month%");
        } 
        $result_set = $result_set->groupBy('district')
                                 ->get()
                                 ->toArray();
        foreach ($result_set as $row) {
            array_push($districts, $row['district']);
            foreach ($commodities as $commodity => $value) {
                // divide the commodity sum with the year consumption rate
                array_push($fpc_details[$commodity],$row[$commodity]/$value);
            }
        }

        foreach ($fpc_details as $commodity => $totals) {
            // $stack = array_shift($totals);
            $int_convert = function($val) {
                return intval($val);
            };
            array_push($all_data, array('name' => $commodities_alias[$commodity], 'data' => array_map($int_convert, $totals))); 
        }

        return array('districts' => $districts, 'all_data' => $all_data);
   }

    public static function getFamilyPlanningCommodities(){
        return array(
                  'FP_Injections' => 4, 'Pills_Microlut' => 4, 'Pills_Microgynon' => 4, 
                  'IUCD_insertion' => 1, 'IUCD_Removals' => 1, 'Implants_insertion'=> 1, 
                  'Implants_Removal' => 1, 'Sterilization_BTL' => 1, 'Steriliz_Vasectomy' => 1, 
                  'NFP' => 1, 'All_others_FP' => 1, 'Clints_condom' => 12
                );
    }

    public static function getFamilyPlanningCommoditiesAliases() {
        return array(
          'FP_Injections' => 'Injections', 'Pills_Microlut' => 'POPs', 'Pills_Microgynon' => 'COCs', 
          'IUCD_insertion' => 'IUCDs', 'IUCD_Removals' => 'IUCD Removals', 'Implants_insertion'=> 'Implants', 
          'Implants_Removal' => 'Implants Removals', 'Sterilization_BTL' => 'BTL', 'Steriliz_Vasectomy' => 'Vasectomy', 
          'NFP' => 'NFP', 'All_others_FP' => 'Other', 'Clints_condom' => 'Condoms'
        );
    }

}