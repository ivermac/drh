<?php

class RHTraining extends Eloquent {
	protected $table = 'rhtraining';
	// public static $unguarded = true;

	public static function rules() {
		return array(
			'facility-code' => 'required',
		);
	}

    public static function getPersonnelTrainingData($training, $former_province, $county, $owner) {

        if ($former_province == "--All Provinces--") {
            $groupBYFilter = "former_province";
            $selected_columns = "count(id) as no_of_people_trained, former_province";
        } else {
            if ($county == "--All Counties--") {
                $groupBYFilter = "county";
                $selected_columns = "count(id) as no_of_people_trained, county";
            } else {
                $groupBYFilter = "district";
                $selected_columns = "count(id) as no_of_people_trained, district";
            }
        }

        $rs = DB::table('rhtraining_summary')
                ->select(DB::raw($selected_columns))
                ->where('training', 'like', "%$training%");
        if ($owner != "--All Owners--") {
            $rs = $rs->whereRaw("ownership = '$owner' COLLATE utf8_unicode_ci");
        }
        if ($former_province == "--All Provinces--") {
            $rs = $rs->groupBy($groupBYFilter);
        } else {
            if ($county == "--All Counties--") {
                $rs = $rs->where("former_province", "=", $former_province)
                         ->groupBy($groupBYFilter) ;
            } else {
                $rs = $rs->where("county", "=", $county)
                         ->groupBy($groupBYFilter) ;
            }
        }

        if ($former_province == "--All Provinces--") {
            $func = function($value) {
                return $value->former_province;
            };
            $withData = array_map($func, $rs->get());
            $withoutData = DB::table('former_provinces');
            if (!empty($withData)) {
                 $withoutData = $withoutData->whereNotIn('name', $withData);
            }
            $withoutData = $withoutData->lists('name');
            $func2 = function($value) {
                return [
                    'no_of_people_trained' => 0,
                    'former_province' => $value];
            };
            $allData = array_merge($rs->get(), array_map($func2, $withoutData));

            $func3 = function($value) {
                $val = (array)$value;

                $num = $val['no_of_people_trained'];
                $prov = $val['former_province'];
                $counter = DB::table('rhtraining_summary')->where('former_province', '=', $prov)->count();
                $counter = ($counter == 0) ? 1 : $counter;
                return [
                        'no_of_people_trained' => round(($num / $counter) * 100, 2),
                        'former_province' => $prov];
            };
            $allData = array_map($func3, $allData);            
        } else if ($county == "--All Counties--") {
            $func = function($value) {
                return $value->county;
            };
            $withData = array_map($func, $rs->get());
            $withoutData = DB::table('counties')
                                      ->where(function($query) use($former_province){
                                          $query->whereIn(
                                            'former_province', 
                                            DB::table('former_provinces')->where('name', '=', $former_province)->lists('id')
                                          );
                                       });
            if (!empty($withData)) {
                $withoutData = $withoutData->whereNotIn('county', $withData);
            }
            $withoutData = $withoutData->lists('county');
            $func2 = function($value) {
                return [
                    'no_of_people_trained' => 0,
                    'county' => $value];
            };
            $allData = array_merge($rs->get(), array_map($func2, $withoutData));

            $func3 = function($value) {
                $val = (array)$value;

                $num = $val['no_of_people_trained'];
                $county = $val['county'];
                $counter = DB::table('rhtraining_summary')->where('county', '=', $county)->count();
                $counter = ($counter == 0) ? 1 : $counter;
                return [
                        'no_of_people_trained' => round(($num / $counter) * 100, 2),
                        'county' => $county];
            };
            $allData = array_map($func3, $allData);
        } else {
            $func = function($value) {
                return $value->district;
            };
            $withData = array_map($func, $rs->get());
            $withoutData = DB::table('districts as d')
                                        ->where(function($query) use($county){
                                          $query->whereIn(
                                            'd.county', 
                                            DB::table('counties as c')->where('c.county', '=', $county)->lists('c.id')
                                          );
                                       });
            if (!empty($withData)) {
                $withoutData = $withoutData->whereNotIn('district', $withData);
            }
            $withoutData = $withoutData->lists('district');
            $func2 = function($value) {
                return [
                    'no_of_people_trained' => 0,
                    'district' => $value];
            };
            $allData = array_merge($rs->get(), array_map($func2, $withoutData));

            $func3 = function($value) {
                $val = (array)$value;

                $num = $val['no_of_people_trained'];
                $district = $val['district'];
                $counter = DB::table('rhtraining_summary')->where('district', '=', $district)->count();
                $counter = ($counter == 0) ? 1 : $counter;
                return [
                        'no_of_people_trained' => round(($num / $counter) * 100, 2),
                        'district' => $district];
            };
            $allData = array_map($func3, $allData);
        }

            
        $returnArray['Personnel Data'] = $allData;
        $returnArray['Training'] = $training;
        $returnArray['Owner'] = $owner;
        return $returnArray;
    }

    public static function getDataOfFacilitiesWithAtleastOnePersonTrained ($training, $former_province, $county, $owner) {
        if ($former_province == "--All Provinces--") {
            $groupByColumn = "former_province";
            $selected_columns = "count(distinct facility_code) as facilities_with_atleast_one_person_trained, former_province";
        } else {
            if ($county == "--All Counties--") {
                $groupByColumn = "county";
                $selected_columns = "count(distinct facility_code) as facilities_with_atleast_one_person_trained, county";
            } else {
                $groupByColumn = "district";
                $selected_columns = "count(distinct facility_code) as facilities_with_atleast_one_person_trained, district";
            }
        }
                
        $rs = DB::table('rhtraining_summary')
                    ->select(
                        DB::raw($selected_columns)
                    )
                    ->whereExists(function($query)
                    {
                        $query->select(DB::raw("facility_code"))
                              ->from('facilities');
                    });
        if ($owner != "--All Owners--") {
            $rs = $rs->whereRaw("ownership = '$owner' COLLATE utf8_unicode_ci");
        }
        if ($groupByColumn == "county") {
            if ($former_province != "--All Provinces--") {
                $rs = $rs->where('former_province', '=', $former_province);
            }
        } else if ($groupByColumn == "district") {
            if ($county != "--All Counties--") {
                $rs = $rs->where('county', '=', $county);
            }
        }
        $rs = $rs->where('training', 'like', "%$training%")
                 ->groupBy($groupByColumn); 
                 // ->get();

        if ($former_province == "--All Provinces--") {
            $func = function($value) {
                return $value->former_province;
            };
            $withData = array_map($func, $rs->get());
            $withoutData = DB::table('former_provinces');
            if (!empty($withData)) {
                 $withoutData = $withoutData->whereNotIn('name', $withData);
            }
            $withoutData = $withoutData->lists('name');
            $func2 = function($value) {
                return [
                    'facilities_with_atleast_one_person_trained' => 0,
                    'former_province' => $value];
            };
            $allData = array_merge($rs->get(), array_map($func2, $withoutData));

            $func3 = function($value) {
                $val = (array)$value;

                $num = $val['facilities_with_atleast_one_person_trained'];
                $prov = $val['former_province'];
                $counter = DB::table('rhtraining_summary')->where('former_province', '=', $prov)->count();
                $counter = ($counter == 0) ? 1 : $counter;
                return [
                        'facilities_with_atleast_one_person_trained' => round(($num / $counter) * 100, 2),
                        'former_province' => $prov];
            };
            $allData = array_map($func3, $allData);
        } else if ($county == "--All Counties--") {
            $func = function($value) {
                return $value->county;
            };
            $withData = array_map($func, $rs->get());
            $withoutData = DB::table('counties')
                                      ->where(function($query) use($former_province){
                                          $query->whereIn(
                                            'former_province', 
                                            DB::table('former_provinces')->where('name', '=', $former_province)->lists('id')
                                          );
                                       });
            if (!empty($withData)) {
                $withoutData = $withoutData->whereNotIn('county', $withData);
            }
            $withoutData = $withoutData->lists('county');
            $func2 = function($value) {
                return [
                    'facilities_with_atleast_one_person_trained' => 0,
                    'county' => $value];
            };
            $allData = array_merge($rs->get(), array_map($func2, $withoutData));

            $func3 = function($value) {
                $val = (array)$value;

                $num = $val['facilities_with_atleast_one_person_trained'];
                $county = $val['county'];
                $counter = DB::table('rhtraining_summary')->where('county', '=', $county)->count();
                $counter = ($counter == 0) ? 1 : $counter;
                return [
                        'facilities_with_atleast_one_person_trained' => round(($num / $counter) * 100, 2),
                        'county' => $county];
            };
            $allData = array_map($func3, $allData);
        } else {
            $func = function($value) {
                return $value->district;
            };
            $withData = array_map($func, $rs->get());
            $withoutData = DB::table('districts as d')
                                        ->where(function($query) use($county){
                                          $query->whereIn(
                                            'd.county', 
                                            DB::table('counties as c')->where('c.county', '=', $county)->lists('c.id')
                                          );
                                       });
            if (!empty($withData)) {
                $withoutData = $withoutData->whereNotIn('district', $withData);
            }
            $withoutData = $withoutData->lists('district');
            $func2 = function($value) {
                return [
                    'facilities_with_atleast_one_person_trained' => 0,
                    'district' => $value];
            };
            $allData = array_merge($rs->get(), array_map($func2, $withoutData));

            $func3 = function($value) {
                $val = (array)$value;

                $num = $val['facilities_with_atleast_one_person_trained'];
                $district = $val['district'];
                $counter = DB::table('rhtraining_summary')->where('district', '=', $district)->count();
                $counter = ($counter == 0) ? 1 : $counter;
                return [
                        'facilities_with_atleast_one_person_trained' => round(($num / $counter) * 100, 2),
                        'district' => $district];
            };
            $allData = array_map($func3, $allData);
        }

        $resultArray['Atleast 1 Person Trained'] = $allData;
        $resultArray['Training'] = $training;
        $resultArray['Owner'] = $owner;
        return  $resultArray;
    }

    // FIXME: move to a new model
    public static function getFacilityOwnershipRHTTrainingBreakdown($region, $level, $training) {
        $results = DB::table('facility_summary')
                    ->select(DB::raw("owner, count(owner) as total"))
                    ->whereIn('facility_code', function($query) use($training, $region, $level){
                            $query->select(DB::raw("distinct facility_code"))
                                  ->from('rhtraining_summary')
                                  ->where('training', 'like', "%$training%");
                            if ($level == 1) {
                                $query = $query->where('former_province', 'like', "%$region%");
                            } else if ($level == 2) {
                                $query = $query->where('county', 'like', "%$region%");
                            } else if ($level == 3) {
                                $query = $query->where('district', 'like', "%$region%");
                            }
                    })
                    ->groupBy('owner')
                    ->remember(60)
                    ->lists('total', 'owner');
        return $results;
    }

    // FIXME: move to a new model
    // TODO: check if this can be refactored 
    // TODO: comment source code
    public static function getRHDashboardTable($training) {
        $rhtDashboardTable = [];

        // number of facilities in a county
        $results = DB::table('facilities as f')
            ->select(DB::raw('count(f.facility_code) as total, c.county'))
            ->join('districts as d', 'f.district', '=', 'd.id')
            ->join('counties as c', 'c.id', '=', 'd.county')
            ->groupBy('c.id')
            ->remember(60)
            ->get();

        foreach ($results as $row) {
            $rhtDashboardTable[$row->county] = ["facilities in county" => $row->total];
        }

        // number of personnel/public health workers in a county 
        $results = DB::table('rhtraining_summary as r')
            ->select(DB::raw('c.county, count(r.id) as total'))
            ->join('counties as c', 'c.county', '=', 'r.county', 'right')
            ->groupBy('c.county')
            ->remember(60)
            ->get();

        foreach ($results as $row) {
            $rhtDashboardTable[$row->county] = array_merge( $rhtDashboardTable[$row->county], ["personnel in county" => $row->total]);
        }

        // facilities with at least one person trained in a county
        $getQueryResult = function($selected_columns, $groupByColumn) use($training) {
            return DB::table('rhtraining_summary')
                        ->select(
                            DB::raw($selected_columns)
                        )
                        ->whereExists(function($query)
                        {
                            $query->select(DB::raw("facility_code"))
                                  ->from('facilities');
                        })
                        ->where('training', 'like', "%$training%")
                        ->groupBy($groupByColumn)
                        ->remember(60) 
                        ->get();
        };  
         $results = $getQueryResult(
            "
            count(distinct facility_code) as facilities_with_atleast_one_person_trained,
            county, 
            former_province
            ",
            "county"
        );  


        foreach ($results as $row) {
            $rhtDashboardTable[$row->county] = array_merge( $rhtDashboardTable[$row->county], 
                                                            ["facilities with atleast one person trained" => $row->facilities_with_atleast_one_person_trained]);
        } 
        foreach ($rhtDashboardTable as $county => $county_details) {
            if (!isset($county_details["facilities with atleast one person trained"])) {
                $rhtDashboardTable[$county] = array_merge( $rhtDashboardTable[$county], ["facilities with atleast one person trained" => 0]);
            }
        }

        // number of personnel/public health workers in a county with particular training
        $results = DB::table('rhtraining_summary as r')
            ->select(DB::raw('c.county, count(r.id) as total'))
            ->join('counties as c', 'c.county', '=', 'r.county', 'right')
            ->where('training', 'like', "%$training%")
            ->groupBy('c.county')
            ->remember(60)
            ->get();
        
        foreach ($results as $row) {
            $rhtDashboardTable[$row->county] = array_merge( $rhtDashboardTable[$row->county], ["trained personnel" => $row->total]);
        } 
        foreach ($rhtDashboardTable as $county => $county_details) {
            if (!isset($county_details["trained personnel"])) {
                $rhtDashboardTable[$county] = array_merge( $rhtDashboardTable[$county], ["trained personnel" => 0]);
            }
        }

        // calculating percentages: faciliities with trained personnel and personnel trained
        foreach ($rhtDashboardTable as $county => $county_details) {
            $percent_facilities_with_trained_personnel = ($county_details['facilities with atleast one person trained'] / $county_details['facilities in county']) * 100;
            $percent_facilities_with_trained_personnel = round($percent_facilities_with_trained_personnel, 2);
            $rhtDashboardTable[$county] = array_merge( $rhtDashboardTable[$county], 
                ['% facilites with trained personnel' => $percent_facilities_with_trained_personnel]);

            $denominator = ($county_details['personnel in county'] == 0) ? 1 : $county_details['personnel in county'];
            $percent_personnel_trained = ($county_details['trained personnel'] / $denominator) * 100;
            $percent_personnel_trained = round($percent_personnel_trained, 2);
            $rhtDashboardTable[$county] = array_merge( $rhtDashboardTable[$county], 
                ['% personnel trained' => $percent_personnel_trained]);
        }
        return $rhtDashboardTable;
    }
}