<?php

class CountyReport extends Eloquent {
    
    public static function facilityMonthsOfStock($period="2014-03", $county="Nairobi") {
        $initial_period = $period;
        $period = str_replace("-", "", $period);
        /*$rs = DB::table('lmis as l')
                ->select(
                    DB::raw(
                        "`Organisation unit`,
                        `Combined Oral contraceptive Pills, FP (Ending Balance)`, 
                        `Cycle Beads, FP (Ending Balance)`, 
                        `Emergency Contraceptive pills (Ending Balance)`, 
                        `Female Condoms, FP (Ending Balance)`, 
                        `IUCDs, FP (Ending Balance)`, 
                        `Implants (1-Rod), FP (Ending Balance)`, 
                        `Implants (2-Rod), FP (Ending Balance)`, 
                        `Injectables, FP (Ending Balance)`, 
                        `Male Condoms, FP (Ending Balance)`, 
                        `Others, FP (Ending Balance)`, 
                        `Progestin only pills, FP (Ending Balance)`"
                    )
                )
                ->join('facility_summary as f', 'Organisation unit code', '=', 'facility_code')
                ->where('f.county', '=', "$county")   
                ->where('Period code', '=', "$period")
                ->get();*/

        $getPeriod = function($num) use($initial_period){
            $date = new DateTime($initial_period);
            $date->sub(new DateInterval("P".$num."M"));
            return $date->format('Ym');
        };

        $periods = [];
        for ($i=0; $i < 3; $i++) { 
            array_push($periods, $getPeriod($i));
        }
        // return gettype($periods);
        $rs = DB::table('lmis as l')
                ->select(
                    DB::raw(
                        "`Organisation unit`,
                        `Combined Oral contraceptive Pills, FP (Dispensed)`, 
                        `Cycle Beads, FP (Dispensed)`, 
                        `Emergency Contraceptive pills (Dispensed)`, 
                        `Female Condoms, FP (Dispensed)`, 
                        `IUCDs, FP (Dispensed)`, 
                        `Implants (1-Rod), FP (Dispensed)`, 
                        `Implants (2-Rod), FP (Dispensed)`, 
                        `Injectables, FP (Dispensed)`, 
                        `Male Condoms, FP (Dispensed)`, 
                        `Others, FP (Dispensed)`, 
                        `Progestin only pills, FP (Dispensed)`"
                    )
                )
                ->join('facility_summary as f', 'Organisation unit code', '=', 'facility_code')
                ->where('f.county', '=', "$county")   
                ->whereIn('Period code', $periods)
                ->get();


        return $rs;
    }

    
}