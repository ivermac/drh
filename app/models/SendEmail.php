<?php

class SendEmail extends Eloquent {
    
    // public static $unguarded = true;
    public static function withNationalSummaryReport () {
        $pdfPath = public_path().DIRECTORY_SEPARATOR."report-charts".DIRECTORY_SEPARATOR.'monthly-summary.pdf';
        $email = 'mark.ekisa@gmail.com';
        $name = 'mark ekisa';
        $recipients = [
        'mekisa@ona.io',
        'ekisa_10@live.org'
        ];
        Mail::send('emails.notifications.ping', $data = array(), function($message) use ($email, $name, $recipients, $pdfPath)
        {
            $message->from('system@drh.com', 'DRH System');
            $message->to($email, $name)->cc($recipients)->subject('Test report');
            $message->attach($pdfPath);
            // $message->attach(public_path()."\\Family planning national report March 2014.pdf");
        });
    }

}