<?php
// set_time_limit(6000);

class Report extends Eloquent {
	// public static $unguarded = true;
	public static function testShellExecution($period = "2014-04") {
		$data['Ending Balance'] = 'sum(`Injectables, FP (Ending Balance)`) as injectables, county';
		$data['Dispensed'] = 'sum(`Injectables, FP (Dispensed)`) as injectables, county';
		$data['Period'] = $period;
		$data['Commodity'] = 'injectables';

		$returned_data = static::get_data($data);
		$data_to_write = $returned_data["bottom-ten"];
		static::writeToOptionsFile($data_to_write, false, "Injectable Months of Stock");
		$output = shell_exec(static::getCommand("injectables-bottom-ten"));

		$data_to_write = $returned_data["top-ten"];
		static::writeToOptionsFile($data_to_write, false, "Injectable Months of Stock");
		$output = shell_exec(static::getCommand("injectables-top-ten"));

		$data['Ending Balance'] = 'sum(`Implants (1-Rod), FP (Ending Balance)`) + sum(`Implants (2-Rod), FP (Ending Balance)`) as implants, county';
		$data['Dispensed'] = 'sum(`Implants (1-Rod), FP (Dispensed)`) + sum(`Implants (2-Rod), FP (Dispensed)`) as implants, county';
		$data['Period'] = $period;
		$data['Commodity'] = 'implants';

		$returned_data = static::get_data($data);
		$data_to_write = $returned_data["bottom-ten"];
		static::writeToOptionsFile($data_to_write, false, "Implant Months of Stock");
		$output = shell_exec(static::getCommand("implants-bottom-ten"));

		$data_to_write = $returned_data["top-ten"];
		static::writeToOptionsFile($data_to_write, false, "Implant Months of Stock");
		$output = shell_exec(static::getCommand("implants-top-ten"));

		// generate stacked bar graph options file for national stock status
		$data_to_write = static::getNationalStockStatus($period);
		static::writeToOptionsFile($data_to_write, true);
		$output = shell_exec(static::getCommand("national-stock-status"));

		//echo "<pre>$output</pre>"; //comment this when testing for pdf generation
		// $period = "2014-04";
		static::generatePDF($period);
	}

	public static function generate_graph_for_national_stock() {
		$data_to_write = static::getNationalStockStatus();
		static::writeToOptionsFile($data_to_write, true);
		$output = shell_exec(static::getCommand("national-stock-status"));
	}

	public static function getNationalContraceptiveServices($period) {
		$append_sum = function($column) {
            return "sum($column) as $column";
        };
        $commodities_alias = array(
          'FP_Injections' => 'Injections', 'Pills_Microlut' => 'POPs', 'Pills_Microgynon' => 'COCs', 
          'IUCD_insertion' => 'IUCDs', 'IUCD_Removals' => 'iucd removals', 'Implants_insertion'=> 'Implants', 
          'Implants_Removal' => 'implants removals', 'Clints_condom' => 'Condoms'
        );
        $columns = implode(", ", array_map($append_sum, array_keys($commodities_alias)));
        $getPeriod = function($num) use($period){
			$date = new DateTime($period);
			$date->sub(new DateInterval("P".$num."M"));
			return $date->format('Y-m');
		};
		$formatPeriod = function($period) {
			$date = new DateTime($period);
			return $date->format('M, Y');
		};
		$aggregates = [];
        for ($i=0; $i < 3; $i++) { 
        	$retrievedPeriod = $getPeriod($i);
        	$formattedPeriod = str_replace("-", "", $retrievedPeriod);
	        $result = get_object_vars(
	                        DB::table('fpc_by_facility')
	                         ->select(DB::raw($columns))
	                         ->where('Period_code', '=', $formattedPeriod)
	                         ->first()
	                    );
	        $aggregates[$formatPeriod($retrievedPeriod)] = ['Injections' => $result['FP_Injections'],
	        								 'POPs' => $result['Pills_Microlut'],
	        								 'COCs' => $result['Pills_Microgynon'],
	        								 'Condoms' => $result['Clints_condom'],
	        								 'IUCDs' => $result['IUCD_insertion'] + $result['IUCD_Removals'],
	        								 'Implants' => $result['Implants_insertion'] + $result['Implants_Removal']
	        								 ];
        }
        return $aggregates;
	}

	public static function writeToOptionsFile($data_to_write, $national_stock = false, $title = "graph title"){
		$options_file = public_path().DIRECTORY_SEPARATOR."report-charts".DIRECTORY_SEPARATOR.'column-graph-options.js';
		$fh = fopen($options_file, 'w') or die("can't open file");
		if ($national_stock == true) {
			$string_json = static::generateStackedBarChartJson($data_to_write);
		} else {
			$string_json = static::generateBarChartJson($data_to_write, $title);
		}
		fwrite($fh, $string_json);
		fclose($fh);
	}

	public static function getCommand($file_name){
		$phantom_js_src = public_path()
									.DIRECTORY_SEPARATOR."report-charts"
									.DIRECTORY_SEPARATOR."phantomjs";
		if (PHP_OS == "Darwin") {
			$phantom_js_src = $phantom_js_src
									.DIRECTORY_SEPARATOR."macos"
									.DIRECTORY_SEPARATOR."phantomjs";
			$command = 'cd report-charts && '.$phantom_js_src;
		} else if (PHP_OS == "WINNT") {
			$phantom_js_src = $phantom_js_src
									.DIRECTORY_SEPARATOR."windows"
									.DIRECTORY_SEPARATOR."phantomjs.exe";
			$command = 'cd report-charts & '.$phantom_js_src;
		}
		$chart = $file_name.'.png';
		$command = $command.'  highcharts-convert.js -infile column-graph-options.js -outfile '.$chart.' -scale 2.5 -width 500 ';
		return $command;
	}

	public static function generatePDF($period = "2014-04"){
		$original_period_format = $period;
		$period = str_replace("-", "", $period);
		$getPath = function($value) {
			$path = public_path().DIRECTORY_SEPARATOR."report-charts";
			if ($value == "to-file") {
				$path = $path.DIRECTORY_SEPARATOR;
			}
			return addslashes($path);
		};
		$files = scandir($getPath("to-folder"));
		$png = [];
		foreach ($files as $file) {
			if (ends_with($file, '.png')) {
				$file_name = basename($file, ".png");
				$png[$file_name] = $getPath("to-file").$file;
				// array_push($png, $getPath("to-file").$file);
			}
		}
		$reporting_rate = static::getCDRRReportingRate($period);
		asort($reporting_rate);
		$national_contraceptive_services = static::getNationalContraceptiveServices($original_period_format);
		$html = View::make('pdf/report')
					->with('png', $png)
					->with('national_contraceptive_services', $national_contraceptive_services)
					->with('reporting_rate', $reporting_rate);

		$outputName = "monthly-summary";					
		$pdfPath = public_path().DIRECTORY_SEPARATOR."report-charts".DIRECTORY_SEPARATOR.$outputName.'.pdf';
		File::put($pdfPath, PDF::load($html, 'A4', 'portrait')->output());

		$email = 'mark.ekisa@gmail.com';
		$name = 'mark ekisa';
		$recipients = [
		'mekisa@ona.io',
		'ekisa_10@live.org'
		];
		Mail::send('emails.notifications.ping', $data = array(), function($message) use ($email, $name, $recipients, $pdfPath)
		{
			$message->from('system@drh.com', 'DRH System');
		    $message->to($email, $name)->cc($recipients)->subject('Test report');
		    $message->attach($pdfPath);
		    // $message->attach(public_path()."\\Family planning national report March 2014.pdf");
		});
		var_dump("monthly summary pdf generated");
		
		// return PDF::load($html, 'A4', 'portrait')->show("monthly-summary");
	}

	public static function generateStackedBarChartJson($data, $title = "National Stock Status"){
		$lmis_totals = $data['lmis-totals'];
		$actual_stock = $data['actual-stock'];
		$categories = array_keys($lmis_totals);
		$series1 = array_values($lmis_totals);
		$series2 = array_values($actual_stock);
		$json_array = [
	        "chart" => [
	            "type" => 'bar'
	        ],
	        "title" => [
	            "text" => $title
	        ],
	        "xAxis" => [
	            "categories" => $categories
	        ],
	        "yAxis" => [
	            "min" => 0,
	            "title" => [
	                "text" => $title
	            ]
	        ],
	        "legend" => [
	            "reversed" => true
	        ],
	        "plotOptions" => [
	            "series" => [
	                "stacking" => 'normal'
	            ]
	        ],
	        "series" => [
	        	[
	            	"name" => 'Central Store MoS',
	            	"data" => $series1
	        	], [
	            	"name" => 'Aggregate Facility MoS',
	            	"data" => $series2
	        	]
	        ]
		];
		return json_encode($json_array);
	}

	public static function generateBarChartJson($values, $title = "Month of Stock") {
		$categories = array_keys($values);
		$data = array_values($values);

		$json_array = [
			'chart' => [
		        "type" => 'column'
		    ],
		    "title" => [
		        "text" => $title
		    ],
		    "subtitle" => [
		        "text" => ''
		    ],
		    "xAxis" => [
		        "categories" => $categories,
		    ],
		    "yAxis" => [
		    	"type" => "logarithmic", //"linear"
		        // "min" => 0,
		        "title" => [
		            "text" => ''
		        ],
		        "plotLines" => [[
		            "value" => 3,
		            "color" => 'red',
		            "width" => 2,
		        ], [
		            "value" => 6,
		            "color" => 'green',
		            "width" => 2,
		        ]]  
		    ],
		    "tooltip" => [
		        "headerFormat" => '<span style="font-size:10px">{point.key}</span><table>',
		        "pointFormat" => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
		        "footerFormat" => '</table>',
		        "shared" => true,
		        "useHTML" => true
		    ],
		    "plotOptions" => [
		        "column" => [
		            "pointPadding" => 0.2,
		            "borderWidth" => 0
		        ]
		    ],
		    "series" => [[
		        "name" => 'Month of Stock',
		        "data" => $data
		    ]]
		];
		return json_encode($json_array);
	}

	public static function get_result_set($custom_column, $period, $commodity){
		$results = LMIS::select(
		                DB::raw(
		                   $custom_column
		                )
		            )
		            ->join('facility_summary', 'Organisation unit code', '=', 'facility_code')
		            ->where('Period code', '=', $period)
		            ->groupBy('county')
		            ->get()
		            ->toArray();

        $flattened = [];
        foreach ($results as $row) {
        	$injectables = $row[$commodity];
        	$county = $row['county'];
        	$flattened[$county] = $injectables;
        }
        return $flattened;
	}

	public static function get_data($data) {
		$ending_balance_columns = $data['Ending Balance'];
		$dispensed_columns = $data['Dispensed'];
		$period = $data['Period'];
		$commodity = $data['Commodity'];

		$getPeriod = function($num) use($period){
			$date = new DateTime($period);
			$date->sub(new DateInterval("P".$num."M"));
			return $date->format('Ym');
		};

		$current = static::get_result_set(
						$ending_balance_columns,
						$getPeriod(0),
						$commodity);
		$dispensed_one = static::get_result_set(
							$dispensed_columns,
							$getPeriod(0),
							$commodity);
		$dispensed_two = static::get_result_set(
							$dispensed_columns,
							$getPeriod(1),
							$commodity);
		$dispensed_three = static::get_result_set(
							$dispensed_columns,
							$getPeriod(2),
							$commodity);

        $get_value = function($array, $value){
        	// return 0 is value is not set
        	return (isset($array[$value]))? $array[$value] : 0;
        };

        $counties = Counties::get(['county'])->toArray();
        $values = [];

        foreach ($counties as $row) {
        	$current_month = $get_value($current, $row['county']);
        	$month_one = $get_value($dispensed_one, $row['county']);
        	$month_two = $get_value($dispensed_two, $row['county']);
        	$month_three = $get_value($dispensed_three, $row['county']);
        	$three_month_total = $month_one + $month_two + $month_three;
        	$three_month_total = ($three_month_total > 0) ? $three_month_total : 1;
        	$moving_average = $three_month_total/3;
        	$values[$row['county']] = intval($current_month/$moving_average);
        }
        arsort($values); //use 'asort' also
        $top_ten = array_slice($values, 0, 10);
        $bottom_ten = array_slice($values, count($values) - 10);
        return ["top-ten" => $top_ten, "bottom-ten" => $bottom_ten];
	}

	public static function getNationalStockStatus($period = "2014-04"){
		$results = LMIS::getLMISMOS($period);
		$commodities = $results['commodities'];
		$mos = $results['mos'];
		$lmis_temp_holder = [];
		foreach ($commodities as $num => $commodity) {
			$lmis_temp_holder[$commodity] = $mos[$num];
		}

		$results = DB::table('fpcommodities')->lists('fp_name', 'Description');
		$lmis_totals = [];
		foreach ($results as $description => $fp_name) {
			if (isset($lmis_temp_holder[$description]))
				$lmis_totals[$fp_name] = $lmis_temp_holder[$description];
		}

		$results = FPC::getStockStatusData('kemsa', $period);
		$commodities = $results['commodities'];
		$actual_stock = $results['actual_stock'];
		$aggregate_actual_stock = [];
		foreach ($commodities as $id => $commodity) {
			if (isset($actual_stock[$id])) {
				$aggregate_actual_stock[$commodity] = round(floatval($actual_stock[$id]), 2);
			} else {
				$aggregate_actual_stock[$commodity] = 0.0;
			}
		}
		return ["lmis-totals" => $lmis_totals,
				"actual-stock" => $aggregate_actual_stock];
	}

	public static function getCDRRReportingRate($period = "201404"){
		/*get facilities that have at least a value more than one in any of the columns used for the query f
		or a specific month*/
		$results = DB::table('lmis')
					->select(DB::raw('count(`Organisation unit code`) as facilities, county'))
					->join('facility_summary', 'facility_code', '=', 'Organisation unit code')
					->where(function($query) {
				        $query->where('Combined Oral contraceptive Pills, FP (Beginning Balance)', '!=', "")
							  ->orWhere('Combined Oral contraceptive Pills, FP (Received This Month)', '!=', "")	
							  ->orWhere('Combined Oral contraceptive Pills, FP (Dispensed)', '!=', "")	 
							  ->orWhere('Combined Oral contraceptive Pills, FP (Losses)', '!=', "")	 
							  ->orWhere('Combined Oral contraceptive Pills, FP (Positive)', '!=', "")	 
							  ->orWhere('Combined Oral contraceptive Pills, FP (Negative)', '!=', "")	 
							  ->orWhere('Combined Oral contraceptive Pills, FP (Ending Balance)', '!=', "")	 
							  ->orWhere('Combined Oral contraceptive Pills, FP (Quantity Requested)', '!=', "")	 
							  ->orWhere('Cycle Beads, FP (Beginning Balance)', '!=', "")	
							  ->orWhere('Cycle Beads, FP (Received This Month)', '!=', "")	
							  ->orWhere('Cycle Beads, FP (Dispensed)', '!=', "")	
							  ->orWhere('Cycle Beads, FP (Losses)', '!=', "")	
							  ->orWhere('Cycle Beads, FP (Positive)', '!=', "")	
							  ->orWhere('Cycle Beads, FP (Negative)', '!=', "")	
							  ->orWhere('Cycle Beads, FP (Ending Balance)', '!=', "")	
							  ->orWhere('Cycle Beads, FP (Quantity Requested)', '!=', "")	
							  ->orWhere('Emergency Contraceptive pills (Beginning Balance)', '!=', "")	
							  ->orWhere('Emergency Contraceptive pills (Received This Month)', '!=', "")	
							  ->orWhere('Emergency Contraceptive pills (Dispensed)', '!=', "")	
							  ->orWhere('Emergency Contraceptive pills (Losses)', '!=', "")	
							  ->orWhere('Emergency Contraceptive pills (Positive)', '!=', "")	
							  ->orWhere('Emergency Contraceptive pills (Negative)', '!=', "")	
							  ->orWhere('Emergency Contraceptive pills (Ending Balance)', '!=', "")	
							  ->orWhere('Emergency Contraceptive pills (Quantity Requested)', '!=', "")	
							  ->orWhere('Female Condoms, FP (Beginning Balance)', '!=', "")	
							  ->orWhere('Female Condoms, FP (Received This Month)', '!=', "")	
							  ->orWhere('Female Condoms, FP (Dispensed)', '!=', "")	
							  ->orWhere('Female Condoms, FP (Losses)', '!=', "")	
							  ->orWhere('Female Condoms, FP (Positive)', '!=', "")	
							  ->orWhere('Female Condoms, FP (Negative)', '!=', "")	
							  ->orWhere('Female Condoms, FP (Ending Balance)', '!=', "")	
							  ->orWhere('Female Condoms, FP (Quantity Requested)', '!=', "")	
							  ->orWhere('IUCDs, FP (Beginning Balance)', '!=', "")	
							  ->orWhere('IUCDs, FP (Received This Month)', '!=', "")	
							  ->orWhere('IUCDs, FP (Dispensed)', '!=', "")	
							  ->orWhere('IUCDs, FP (Losses)', '!=', "")	
							  ->orWhere('IUCDs, FP (Positive)', '!=', "")	
							  ->orWhere('IUCDs, FP (Negative)', '!=', "")	
							  ->orWhere('IUCDs, FP (Ending Balance)', '!=', "")	
							  ->orWhere('IUCDs, FP (Quantity Requested)', '!=', "")	
							  ->orWhere('Implants (1-Rod), FP (Beginning Balance)', '!=', "")	
							  ->orWhere('Implants (1-Rod), FP (Received This Month)', '!=', "")	
							  ->orWhere('Implants (1-Rod), FP (Dispensed)', '!=', "")	
							  ->orWhere('Implants (1-Rod), FP (Losses)', '!=', "")	
							  ->orWhere('Implants (1-Rod), FP (Positive)', '!=', "")	
							  ->orWhere('Implants (1-Rod), FP (Negative)', '!=', "")	
							  ->orWhere('Implants (1-Rod), FP (Ending Balance)', '!=', "")	
							  ->orWhere('Implants (1-Rod), FP (Quantity Requested)', '!=', "")	
							  ->orWhere('Implants (2-Rod), FP (Beginning Balance)', '!=', "")	
							  ->orWhere('Implants (2-Rod), FP (Received This Month)', '!=', "")	
							  ->orWhere('Implants (2-Rod), FP (Dispensed)', '!=', "")	
							  ->orWhere('Implants (2-Rod), FP (Losses)', '!=', "")	
							  ->orWhere('Implants (2-Rod), FP (Positive)', '!=', "")	
							  ->orWhere('Implants (2-Rod), FP (Negative)', '!=', "")	
							  ->orWhere('Implants (2-Rod), FP (Ending Balance)', '!=', "")	
							  ->orWhere('Implants (2-Rod), FP (Quantity Requested)', '!=', "")	
							  ->orWhere('Injectables, FP (Beginning Balance)', '!=', "")	
							  ->orWhere('Injectables, FP (Received This Month)', '!=', "")	
							  ->orWhere('Injectables, FP (Dispensed)', '!=', "")	
							  ->orWhere('Injectables, FP (Losses)', '!=', "")	
							  ->orWhere('Injectables, FP (Positive)', '!=', "")	
							  ->orWhere('Injectables, FP (Negative)', '!=', "")	
							  ->orWhere('Injectables, FP (Ending Balance)', '!=', "")	
							  ->orWhere('Injectables, FP (Quantity Requested)', '!=', "")	
							  ->orWhere('Male Condoms, FP (Beginning Balance)', '!=', "")	
							  ->orWhere('Male Condoms, FP (Received This Month)', '!=', "")	
							  ->orWhere('Male Condoms, FP (Dispensed)', '!=', "")	
							  ->orWhere('Male Condoms, FP (Losses)', '!=', "")	
							  ->orWhere('Male Condoms, FP (Positive)', '!=', "")	
							  ->orWhere('Male Condoms, FP (Negative)', '!=', "")	
							  ->orWhere('Male Condoms, FP (Ending Balance)', '!=', "")	
							  ->orWhere('Male Condoms, FP (Quantity Requested)', '!=', "")	
							  ->orWhere('Others, FP (Beginning Balance)', '!=', "")	
							  ->orWhere('Others, FP (Received This Month)', '!=', "")	
							  ->orWhere('Others, FP (Dispensed)', '!=', "")	
							  ->orWhere('Others, FP (Losses)', '!=', "")	
							  ->orWhere('Others, FP (Positive)', '!=', "")	
							  ->orWhere('Others, FP (Negative)', '!=', "")	
							  ->orWhere('Others, FP (Ending Balance)', '!=', "")	
							  ->orWhere('Others, FP (Quantity Requested)', '!=', "")	
							  ->orWhere('Progestin only pills, FP (Beginning Balance)', '!=', "")	
							  ->orWhere('Progestin only pills, FP (Received This Month)', '!=', "")	
							  ->orWhere('Progestin only pills, FP (Dispensed)', '!=', "")	
							  ->orWhere('Progestin only pills, FP (Losses)', '!=', "")	
							  ->orWhere('Progestin only pills, FP (Positive)', '!=', "")	
							  ->orWhere('Progestin only pills, FP (Negative)', '!=', "")	
							  ->orWhere('Progestin only pills, FP (Ending Balance)', '!=', "")	
							  ->orWhere('Progestin only pills, FP (Quantity Requested)', '!=', "");	
					 })
					 ->where('Period code', '=', $period)
					 ->groupBy('county')
					 ->get();

		$facilities_reported_per_county = [];
        foreach ($results as $row) {
        	$county = $row->county;
        	$facilities = $row->facilities;
        	$facilities_reported_per_county[$county] = $facilities;
        }

        $results = DB::table('facility_summary')
			        ->select(DB::raw('count(facility_code) as facilities, county'))
			        ->groupBy('county')
			        ->get();

		$facilities_count_per_county = [];
        foreach ($results as $row) {
        	$county = $row->county;
        	$facilities = $row->facilities;
        	$facilities_count_per_county[$county] = $facilities;
        }

        $reporting_rate = [];
        foreach ($facilities_count_per_county as $county => $total) {
        	if (isset($facilities_reported_per_county[$county])) 
        		$value = $facilities_reported_per_county[$county] / $total;
        	else 
        		$value = 1 / $total;
        	$reporting_rate[$county] = intval($value * 100);
        }

        return $reporting_rate;
	}
}