<?php
class UsersController extends BaseController {

	public function login()
	{
		if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
			if (Auth::user()->usertype == 2) {
				return Redirect::route('reproductive-health-training-new')->with('message', 'You are now logged in!');
			}
		   	return Redirect::route('family-planning-stock-status')->with('message', 'You are now logged in!');
		}
		return Redirect::to('/')
		      ->with('message', 'Your email/password combination was incorrect')
		      ->withInput();
	}

	public function logout() {
	   	Auth::logout();
	   	return Redirect::to('/')
	   		->with('message', 'Your are now logged out!')
	   		->with('custom-alert', 'alert-message-warning');
	}

	public function csv(){
		echo php_uname('s');/* Operating system name */
		echo "<br />";
		echo php_uname('n');/* Host name */
		echo "<br />";
		echo php_uname('r');/* Release name */
		echo "<br />";
		echo php_uname('v');/* Version information */
		echo "<br />";
		echo php_uname('m');/* Machine type */
		echo "<br />";
		echo PHP_OS;/* constant will contain the operating system PHP was built on */
		var_dump(strlen(decbin(~0)));
		echo "<br>";

		/*$output = shell_exec('ls -lart');
		echo "<pre>$output</pre>";*/

		/*SELECT 
			sum(`Injectables, FP (Ending Balance)`) as Injectables,
		    county
		FROM `lmis`, facility_summary
		where `Period code` = 201404 and
			`Organisation unit code` = `facility_code`
		group by county*/

		// $results = Report::get_cdrr_reporting_rate();
		
		/*$results = LMIS::getLMISMOS("2014-04");
		$commodities = $results['commodities'];
		$mos = $results['mos'];
		$lmis_totals = [];
		foreach ($commodities as $num => $commodity) {
			$lmis_totals[$commodity] = $mos[$num];
		}

		$results = DB::table('fpcommodities')->lists('fp_name', 'Description');
		$final_lmis_totals = [];
		foreach ($results as $description => $fp_name) {
			$final_lmis_totals[$fp_name] = $lmis_totals[$description];
		}
		var_dump($final_lmis_totals);

		$results = FPC::getStockStatusData('kemsa', '2014-04');
		$commodities = $results['commodities'];
		$actual_stock = $results['actual_stock'];
		$output = [];
		foreach ($commodities as $id => $commodity) {
			if (isset($actual_stock[$id])) {
				$output[$commodity] = floatval($actual_stock[$id]);
			} else {
				$output[$commodity] = 0.0;
			}
		}*/

		$period = "2014-04";
		$period = str_replace("-", "", $period);
		var_dump($period);
		// $ch = curl_init();
		/*cURL has not been configured to trust the server’s HTTPS certificate / Certificate Authority.
		Using 'CURLOPT_SSL_VERIFYPEER' and setting it to 'false' causes cURL to blindly accept any server 
		certificate, without doing any verification as to which CA signed it, and whether
		or not that CA is trusted*/
		/*curl_setopt_array($ch, array(
		    CURLOPT_URL => 'https://ona.io/api/v1/forms',
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_USERPWD => 'ivermac:book3rac'
		));*/
		// var_dump(curl_getinfo($ch));
		/*$output = curl_exec($ch);
		$output = json_decode($output, true);
		foreach ($output as $value) {
			var_dump($value);
		}
		curl_close($ch);*/

        // var_dump(DB::select("call rht_filter('%PRC%')"));

		// *****************************************************PERMS*****************************************************************
        
        /*$perms = fileperms($filename);
        echo "<br>".substr(sprintf('%o',  $perms), -4);
        chmod($filename, 0755);
        $perms = fileperms($filename);
        echo "<br>".substr(sprintf('%o',  $perms), -4);
        var_dump($perms);*/

		// *****************************************************EMAIL*****************************************************************
        /*$email = "jokeyo@clintonhealthaccess.org";
		$name = "joshua okeyo";
		$recipients = [
		'jayuma@clintonhealthaccess.org',
		'gmacharia@clintonhealthaccess.org',
		'ANgatia@clintonHealthAccess.org',
		'bwariari@clintonhealthaccess.org',
		'ggimaiyo@clintonhealthaccess.org',
		'jhungu@clintonhealthaccess.org',
		'jlusike@clintonhealthaccess.org',
		'jhagembe@clintonhealthaccess.org',
		'onjathi@clintonhealthaccess.org',
		'otarow9@gmail.com',
		'rkihoto@clintonhealthaccess.org',
		'smutheu@clintonhealthaccess.org',
		'skadima@clintonhealthaccess.org',
		'tngugi@clintonhealthaccess.org',
		'toyier@clintonhealthaccess.org',
		'pnjiri@clintonhealthaccess.org',
		'dkarambi@clintonhealthaccess.org',
		'eongute@clintonhealthaccess.org'
		];
		Mail::send('emails.notifications.ping', $data = array(), function($message) use ($email, $name, $recipients)
		{
			$message->from('system@drh.com', 'DRH System');
		    $message->to($email, $name)->cc($recipients)->subject('Familiy Planning National Report March 2014');
		    $message->attach(public_path()."\\Family planning national report March 2014.pdf");
		});*/
		// ******************************************************CSV*****************************************************************
		$email = "mark.ekisa@gmail.com";
		$name = "mark ekisa";
		$recipients = [
		'mekisa@ona.io',
		'ekisa_10@live.com'
		];
		Mail::send('emails.notifications.ping', $data = array(), function($message) use ($email, $name, $recipients)
		{
			$message->from('system@drh.com', 'DRH System');
		    $message->to($email, $name)->cc($recipients)->subject('Familiy Planning National Report March 2014');
		    $message->attach(public_path()."\\Family planning national report March 2014.pdf");
		});
		// var_dump(FPC::getRHDashboardData("LAPM"));

		/*$csv_path = public_path().'\test_makueni.csv';
		$csvFile = new Keboola\Csv\CsvFile($csv_path);
		$counter = 0;
		$keys = [];
		foreach($csvFile as $row) {
			if ($counter == 0) {
				foreach ($row as $key => $value) {
					if ($key > 3) {
						if (strpos($value, 'FP') == null) {
							$row_as_array = explode("(", $value);
							$row_as_array[0] = trim($row_as_array[0], " , "); 
							$row_as_array[1] = trim($row_as_array[1], " ( ) "); 
							$keys[$row_as_array[0]][$row_as_array[1]] = "";
						} else {
							$row_as_array = explode("FP", $value);
							$row_as_array[0] = trim($row_as_array[0], " , "); 
							$row_as_array[1] = trim($row_as_array[1], " () ");
							$keys[$row_as_array[0]][$row_as_array[1]] = "";
						}
					}
				}
			} else {
				$index = 4;
				foreach ($keys as $commodity => $status_array) {
					foreach ($status_array as $status => $value) {
						$keys[$commodity][$status] = $row[$index];
						$index++;
					}
				}
				var_dump($keys);
			}
			


			if ($counter == 1) {
				break;
			}

			$counter++;
		}*/
		// echo trim("....mark ekisa,,,", ".,");

	}
	public function postEditPipeline() {
		var_dump(Input::all());
	}
}