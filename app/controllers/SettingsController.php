<?php

class SettingsController extends BaseController {

	protected $layout = "layouts.dashboard";
	public function __construct() {
	   	$this->beforeFilter('csrf', array('on'=>'post'));
	   	$this->beforeFilter('auth', array('only'=>array('showOtherSettings', 
	   													'showStockOnHand', 
	   													'showSupplyChain')));
	}

	public function showOtherSettings(){
		$this->layout->with('active', 'settings');
		$this->layout->content = View::make('settings/other-settings');
		$this->layout->custom_script = View::make('scripts/settings');
		$this->layout->custom_style = "";
	}

	public function showStockOnHand(){
		$this->layout->with('active', 'settings');
		$this->layout->content = View::make('settings/stock-on-hand');
		$this->layout->custom_script = View::make('scripts/settings');
		$this->layout->custom_style = "";
	}

	public function showSupplyChain(){
		$this->layout->with('active', 'settings');
		$this->layout->content = View::make('settings/supply-chain');
		$this->layout->custom_script = View::make('scripts/settings');
		$this->layout->custom_style = "";
	}
}