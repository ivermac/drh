<?php

class CommoditiesController extends BaseController {

	protected $layout = "layouts.dashboard";
	public function __construct() {
	   	$this->beforeFilter('csrf', array('on'=>'post'));
	   	$this->beforeFilter('auth', array('only'=>array(
	   												'showDashboard', 
	   												'editPipeline', 
	   												'addStockOnHand', 
	   												'listCommodities',
	   												'addNewConsignment', 
	   												'listSupplyPlan',
	   												'deleteSupplyPlan',
	   												'listFundingSources',
	   												'detailedStockOnHand',
	   												'receivedCommoditiesListing',
	   												'addReproductiveHealthTraining',
	   												'editReproductiveHealthTraining',
	   												'deleteReproductiveHealthTraining',
	   												'dashboardReproductiveHealthTraining',
	   												'downloadReproductiveHealthTrainingCsv', 
													'listReproductiveHealthTraining')));
	}

	public function receivedCommoditiesListing() {
		$receivedCommodities = FPC::getReceivedCommoditiesData();
		$this->layout->with('active', 'commodities');
		$this->layout->custom_script = View::make('scripts/commodities-received');
		$this->layout->custom_style = View::make('styles/commodities/received');
		$this->layout->content = View::make('commodities/received-commodities-listing')
									->with('receivedCommodities', $receivedCommodities);
	}

	public function listCommodities () {
		$fpcommodities = DB::table('fpcommodities')->paginate(7);
		$this->layout->with('active', 'settings');
		$this->layout->custom_script = View::make('scripts/commodities-dashboard');
		$this->layout->custom_style = View::make('styles/commodities/dashboard');
		$this->layout->content = View::make('commodities/listing')
									->with('fpcommodities', $fpcommodities);
	}

	public function deleteCommodity ($id) {
		DB::table('fpcommodities')->where('id', '=', $id)->delete();
		return Redirect::route('commodities-listing')
				->with('success-message', 'The record has been successfully deleted');
	}

	public function listFundingSources() {
		$funding_sources = DB::table('funding_sources')->paginate(7);
		$this->layout->with('active', 'settings');
		$this->layout->custom_script = View::make('scripts/commodities-dashboard');
		$this->layout->custom_style = View::make('styles/commodities/dashboard');
		$this->layout->content = View::make('commodities/funding-sources/listing')
									->with('funding_sources', $funding_sources);
	}

	public function deleteFundingSource ($id) {
		DB::table('funding_sources')->where('id', '=', $id)->delete();
		return Redirect::route('funding-sources-listing')
				->with('success-message', 'The record has been successfully deleted');
	}

	public function addNewConsignment() {
		$commodities = DB::table('fpcommodities')->lists('fp_name', 'id');
		$stores = DB::table('stores')->lists('name', 'id');
		$funding_sources = DB::table('funding_sources')->lists('funding_source', 'id');
		$procuring_agencies = $funding_sources;
		$this->layout->with('active', 'commodities');
		$this->layout->custom_script = View::make('scripts/commodities-add-new-consignment');
		$this->layout->custom_style = View::make('styles/commodities/add-new-consignment');
		$this->layout->content = View::make('commodities/add-new-consignment')
										->with('funding_sources', $funding_sources)
										->with('procuring_agencies', $procuring_agencies)
										->with('commodities', $commodities)
										->with('stores', $stores);
	}

	public function showDashboard(){
		$fpc = array_merge(["0" => ""], DB::table('fpcommodities')->lists('fp_name', 'id')) ;
		$fiscal_years = [ "" => ""];
		$year = intval(date("Y"));
		for ($value=0; $value < 5; $value++) { 
			$fiscal_year = $year - $value;
			$fiscal_years[$fiscal_year] = "Year beginning $fiscal_year";
		}
		$this->layout->with('active', 'commodities');
		$this->layout->custom_script = View::make('scripts/commodities-dashboard');
		$this->layout->custom_style = View::make('styles/commodities/dashboard');
		$this->layout->content = View::make('commodities/dashboard')
									->with('fpc', $fpc)
									->with('fiscal_years', $fiscal_years);
	}

	public function editPipeline($id) {
		$result = DB::table('pipeline')->where('id', '=', $id)->first();
		$funding_source = DB::table('funding_sources')->lists('funding_source', 'id');
		$commodities = DB::table('fpcommodities')->lists('fp_name', 'id');
		$stores = DB::table('stores')->lists('name', 'id');
		$status = ['RECEIVED' => 'Recieved-In Store', 
					'PENDING' => 'Pending', 
					'DELAYED' => 'Delayed', 
					'INCOUNTRY' => 'Arrived Awaiting Clearance', 
					'CANCELLED' => 'Cancelled'];

		$getStatus = function($pipelineStatus) use($status){
			$replaceKemsaOrPsi = function($status) {
				$string = $status;
				$patterns = ['/(KEMSA|PSI)/'];
				$replacements = [''];
				return preg_replace($patterns, $replacements, $string);
			};

			foreach ($status as $key => $value) {
				if ($replaceKemsaOrPsi($pipelineStatus) == $key) {
					return $key;
				} 
			}
			return "no match";
		};

		$this->layout->with('active', 'commodities');
		$this->layout->custom_style = View::make('styles/commodities/edit-pipeline');
		$this->layout->custom_script = View::make('scripts/edit-pipeline');
		$this->layout->content = View::make('commodities/pipeline-edit')
										->with('funding_source', $funding_source)
									    ->with('commodities', $commodities)
										->with('status', $status)
										->with('getStatus', $getStatus)
										->with('stores', $stores)
										->with('result', $result);
	}


	// TODO: comment source code
	public function listSupplyPlan(){
		$supply_plan = DB::table('supply_plan')->paginate(8);
		$inputs = [];

		$commodities = array_merge([""=>""], DB::table('fpcommodities')->lists('fp_name', 'fp_name'));
		$funding_sources = array_merge([""=>""], DB::table('funding_sources')->lists('funding_source', 'funding_source'));
		$stores = array_merge([""=>""], DB::table('stores')->lists('name', 'name'));
		$statuses = ['' => '',
					'RECEIVED' => 'Recieved-In Store', 
					'PENDING' => 'Pending', 
					'DELAYED' => 'Delayed', 
					'INCOUNTRY' => 'Arrived Awaiting Clearance', 
					'CANCELLED' => 'Cancelled'];
		$date_categories = ['' => '',
							'E.T.A' => 'E.T.A',
							'delayed-to' => 'Delayed To',
							'arrived-on' => 'Arrival Date'];

		$search = Input::get('search');
		$commodity = Input::get('commodity');
		$store = Input::get('store');
		$status = Input::get('status');
		$date_category = Input::get('date-category');
		$supply_plan_listing_date_picker = Input::get('supply-plan-listing-date-picker');
		$funding_source = Input::get('funding_source');

		$results = DB::table('supply_plan');

		if (isset($search)) {
			if (!empty($commodity)) {
				$results->where('commodity_name', 'like', "%$commodity%");
			}

			if (!empty($funding_source)) {
				$results->where('funding_source', 'like', "%$funding_source%");
			}

			if (!empty($store)) {
				$results->where('store_name', 'like', "%$store%");
			}

			if (!empty($status)) {
				$results->where('status', 'like', "%$status%");
			}

			if (!empty($date_category)) {
				if ($date_category == 'E.T.A') {
					$results->where('ETA', 'like', "%$supply_plan_listing_date_picker%");
				} else if ($date_category == 'delayed-to') {
					$results->where('delayed_to', 'like', "%$supply_plan_listing_date_picker%");
				} else if ($date_category == 'arrived-on') {
					$results->where('date_receive', 'like', "%$supply_plan_listing_date_picker%");
				}
			}

			$supply_plan = $results->paginate(8); 
			$inputs = array_except(Input::all(), 'page');
		} 

		$replaceKemsaOrPsi = function($status) {
			$string = $status;
			$patterns = ['/(KEMSA|PSI)/'];
			$replacements = [''];
			return preg_replace($patterns, $replacements, $string);
		};

		$this->layout->with('active', 'commodities');
		$this->layout->custom_script = View::make('scripts/list-supply-plan');	
		$this->layout->custom_style = View::make('styles/commodities/list-supply-plan');
		$this->layout->content = View::make('commodities/supply-plan-listing')
									->with('supply_plan', $supply_plan)
									->with('funding_sources', $funding_sources)
									->with('inputs', $inputs)
									->with('replaceKemsaOrPsi', $replaceKemsaOrPsi)
									->with('stores', $stores)
									->with('date_categories', $date_categories)
									->with('statuses', $statuses)
									->with('commodities', $commodities);
	}

	public function deleteSupplyPlan($id) {
		DB::table('pipeline')->where('id', '=', $id)->delete();
		return Redirect::route('supply-plan-listing')
				->with('success-message', 'The record has been successfully deleted');
	}

	public function addStockOnHand(){
		$commodities = DB::table('fpcommodities')->lists('fp_name', 'id');
		$stores = DB::table('stores')->lists('name', 'id');
		$this->layout->with('active', 'commodities');
		$this->layout->custom_script = View::make('scripts/commodities-add-soh');	
		$this->layout->custom_style = View::make('styles/commodities/add-soh');
		$this->layout->content = View::make('commodities/add-stock-on-hand')
									->with('commodities', $commodities)
									->with('stores', $stores);
	}

	// TODO: comment source code
	public function detailedStockOnHand(){
		$search_engine = function($date, $store) {
			$date_filter = "%$date%";
			$results = DB::table('pipeline as p')
						->select(
				            DB::raw(
				            	"p.id,
				            	f.fp_name,
								f.Unit, 
								p.fp_quantity,
								if ($store = 1, f.projected_monthly_c, f.projected_psi) as projected_consumption, 
								p.fp_quantity/if ($store = 1, f.projected_monthly_c, f.projected_psi) as soh_mos,
								p.fp_date"
							)
				        )
				        ->join('fpcommodities as f ', 'f.id', '=', 'p.fpcommodity_Id')
				        ->where('fp_date', 'like', $date_filter)
				        ->where('store_id', '=', $store )
				        ->where('transaction_type', 'like', '%SOH%');
			
			return $results->paginate(10);
		};

		$getLabelDisplayables = function($month, $year, $store) {
			$months = [
						'01'=>'January',
						'02'=>'February',
						'03'=>'March',
						'04'=>'April',
						'05'=>'May',
						'06'=>'June',
						'07'=>'July',
						'08'=>'August',
						'09'=>'September',
						'10'=>'October',
						'11'=>'November',
						'12'=>'December'
						];
			$rs = DB::table('stores')->where('id', '=', $store)->first();
			return ["month" => $months[$month], "year" => $year, "store" => $rs->name];
		};

		$search = Input::get('search');
		if (isset($search) && $search == 'search') {
			$month = Input::get('month');
			$year = Input::get('year');
			$seach_date = "$year-$month";
			$store = Input::get('store');

			$results = $search_engine($seach_date, $store);
			$search_inputs = array_except(Input::all(), 'page'); //allows the page variable to change according to that specific page
			$labelDisplayables = $getLabelDisplayables($month, $year, $store);
		} else {
			$results = $search_engine(date('Y-m'), 1);
			$search_inputs = [];
			$labelDisplayables = $getLabelDisplayables(date('m'), date('Y'), 1);
		}

		$stores = DB::table('stores')->lists('name', 'id');
		$commodities = DB::table('fpcommodities')->lists('fp_name', 'id');
		$this->layout->with('active', 'commodities');
		$this->layout->custom_script = View::make('scripts/commodities-detailed-soh');		
		$this->layout->custom_style = View::make('styles/commodities/detailed-stock-on-hand');
		$this->layout->content = View::make('commodities/detailed-stock-on-hand')
									->with('results', $results)
									->with('labelDisplayables', $labelDisplayables)
									->with('commodities', $commodities)
									->with('search_inputs', $search_inputs)
									->with('stores', $stores);
	}

	public function deleteStockOnHand($id) {
		DB::table('pipeline')->where('id', '=', $id)->delete();
		return Redirect::route('detailed-stock-on-hand')
				->with('success-message', 'The record has been successfully deleted');
	}

	public function addReproductiveHealthTraining(){
		$output = static::setValues();

		$this->layout->with('active', 'rht');
		$this->layout->custom_script = View::make('scripts/commodities-rht');
		$this->layout->custom_style = View::make('styles/commodities/reproductive-health-training/rht');
		$this->layout->content = View::make('commodities/reproductive-health-training/new')
									->with('counties', $output['counties'])
									->with('trainingsA', $output['trainingsA'])
									->with('trainingsB', $output['trainingsB']);
	}

	// FIX ME : move login code to commidity model
	public function editReproductiveHealthTraining($id) {
		$rhtraining = RHTraining::find($id);
		$checked_trainings = json_decode($rhtraining->training, true);
		$facility = DB::table('facilities')->where('facility_code', '=', $rhtraining->facility_code)->first();
		$output = static::setValues($checked_trainings);
		$this->layout->with('active', 'rht');
		$this->layout->custom_script = View::make('scripts/commodities-rht');		
		$this->layout->custom_style = View::make('styles/commodities/reproductive-health-training/rht');
		$this->layout->content = View::make('commodities/reproductive-health-training/edit')
									->with('facility_name', $facility->facility_name)
									->with('rhtraining', $rhtraining)
									->with('checked_trainings', $output['checked'])
									->with('counties', $output['counties'])
									->with('trainingsA', $output['trainingsA'])
									->with('trainingsB', $output['trainingsB']);
	}

	public function deleteReproductiveHealthTraining($id) {
		DB::table('rhtraining')->where('id', '=', $id)->delete();
		return Redirect::route('reproductive-health-training-list')
				->with('success-message', 'The record has been successfully deleted');
	}

	public function downloadReproductiveHealthTrainingCsv(){
		$csv_path = public_path().DIRECTORY_SEPARATOR.'rht-data.csv';
		$csvFile = new Keboola\Csv\CsvFile($csv_path);

		$column_names = ['First Name', 'Surname', 'Personnel no', 'Qualification', 'Station', 'Facility Code'];
		$trainings = DB::table('trainings')->lists('name', 'identifier');
		$column_names = array_merge($column_names, array_values($trainings));
		$csvFile->writeRow($column_names);

		$results = RHTraining::all()->toArray();
		foreach ($results as $index => $row) {
			$csv_row = [
						$row['first_name'], 
						$row['surname'], 
						$row['personnel_no'], 
						$row['qualification'], 
						$row['station'], 
						$row['facility_code']
					];
			$training_received_assoc_array = json_decode($row['training'], true);

			foreach ($trainings as $identifier => $name) {
				// check if the a user has a particular training
				 if (in_array($identifier, array_keys($training_received_assoc_array))) {
				 	array_push($csv_row, $training_received_assoc_array[$identifier]);
				 } else {
				 	array_push($csv_row, 'N');
				 }
			}
			$csvFile->writeRow($csv_row);
		}
		$headers = [
	        'Content-Type' => 'text/csv',
	        'Content-Disposition' => 'attachment; filename="rht-data.csv"',
		];
		return Response::download($csv_path, 'rht-data.csv', $headers);
	}

	// FIX ME : create commodity model and move this snippet there because it's logic code
	public static function setValues($checked_values = null){
		$result_set = DB::table('counties')->get();
		$counties = array(""=>"");
		foreach ($result_set as $row) {
		   	$counties[$row->id] = $row->county;
		}

		$result_set = DB::table('trainings')->get();
		$mid_point = (count($result_set) % 2 == 0) ? count($result_set)/2 : count($result_set)/2 + 1;

		$trainingsA = [];
		$trainingsB = [];
		$checked = [];
		foreach ($result_set as $row) {
			if ($row->id <= $mid_point) {
		   		$trainingsA[$row->id] = $row->identifier;
		   		if ($checked_values != null && in_array($row->name, array_keys($checked_values))) {
		   			$checked[$row->identifier] = $checked_values[$row->name];
		   			// array_push($checked, $row->identifier);
		   		}
			} else {
		   		$trainingsB[$row->id] = $row->identifier;
		   		if ($checked_values != null && in_array($row->name, array_keys($checked_values))) {
		   			$checked[$row->identifier] = $checked_values[$row->name];
		   			// array_push($checked, $row->identifier);
		   		}
			}
		}
		return array('counties'=>$counties, 'trainingsA' => $trainingsA, 'trainingsB' => $trainingsB, 'checked' => $checked);
	}

	// FIX ME : 
	// 1.move logic code to commodity model
	// 2.use closure for repeating logic
	public function listReproductiveHealthTraining() {
		$rhtrainings = [];
		$searched_commodity = '';

		$trainings = array_merge(["0"=>""], DB::table('trainings')->lists('name', 'id'));
		$counties = array_merge(["0"=>""], DB::table('counties')->lists('county', 'id'));
		$facility_owners = array_merge(["0"=>""], DB::table('facility_owners')->lists('owner', 'owner'));
		$facility_types = array_merge(["0"=>""], DB::table('facility_types')->lists('type', 'type'));

		// this is a hidden field
		$search = Input::get('search');
		if ($search == 'search') {
			$search = static::search();
			$result_set = $search['result_set'];
			$search_inputs = $search['inputs'];
		} else {
			$result_set = DB::table('rhtlisting')->paginate(7);
			$search_inputs = [];
		}
		foreach ($result_set as $result) {
			$rhtrainings[$result->id] = json_decode($result->training, true) ;
		}
		$this->layout->with('active', 'rht');
		$this->layout->custom_script = View::make('scripts/commodities-rht');
		$this->layout->custom_style = View::make('styles/commodities/reproductive-health-training/rht');
		$this->layout->content = View::make('commodities/reproductive-health-training/list')
									->with('search_inputs', $search_inputs)
									->with('rhtrainings', $rhtrainings)
									->with('results', $result_set)
									->with('searched_commodity', $searched_commodity)
									->with('trainings', $trainings)
									->with('facility_owners', $facility_owners)
									->with('facility_types', $facility_types)
									->with('counties', $counties);
	}

	public static function search() {
		$commodity_id = Input::get('commodity');
		$district_id = Input::get('district');
		$county_id = Input::get('county');
		$facility_code = Input::get('facility-code');
		$owner = Input::get('facility-owners');
		$type = Input::get('facility-types');
		
		$trainings = array_merge(["0"=>""], DB::table('trainings')->lists('name', 'id'));

		$result_set = DB::table('rhtlisting');
		// TODO: use key: value [id: resultset] and foreach 
		if (!empty($commodity_id)) {
			$searched_commodity = $trainings[$commodity_id]; 
			$result_set = $result_set->where('training', 'LIKE', "%$searched_commodity%");
		}
		if (!empty($district_id)) {
			$result_set = $result_set->where('district_id', '=', $district_id);
		}
		if (!empty($county_id)) {
			$result_set = $result_set->where('county_id', '=', $county_id);
		}
		if (!empty($facility_code)) {
			$result_set = $result_set->where('facility_code', '=', $facility_code);
		}
		if (!empty($owner)) {
			$result_set = $result_set->where('owner', '=', $owner);
		}
		if (!empty($type)) {
			$result_set = $result_set->where('type', '=', $type);
		}
		return array('result_set' => $result_set->paginate(7), 'inputs' => array_except(Input::all(), 'page'));
	}

	public function dashboardReproductiveHealthTraining() {
		$trainings = DB::table('trainings')->lists('identifier', 'name');
		$former_provinces = array_merge(["0" => "--All Provinces--"], DB::table('former_provinces')->lists('name', 'id'));
		$owners = array_merge(["all" => "--All Owners--"],DB::table('rhtraining_summary')->groupBy('ownership')->lists('ownership', 'ownership'));

		$uc_first_changer = function($val) {
			return ucfirst($val);
		};
		$owners = array_map($uc_first_changer, $owners);

		$this->layout->with('active', 'rht');
		$this->layout->custom_script = View::make('scripts/commodities-rht-dashboard');
		$this->layout->custom_style = "";
		$this->layout->content = View::make('commodities/reproductive-health-training/dashboard')
										->with('trainings', $trainings)
										->with('owners', $owners)
										->with('former_provinces', $former_provinces);
	}
}