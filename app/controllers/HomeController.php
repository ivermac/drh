<?php

class HomeController extends BaseController {

	protected $layout = "layouts.dashboard";

	public function __construct() {
	   	// $this->beforeFilter('csrf', array('on'=>'post'));
	   	$this->beforeFilter('auth', array('only'=>array('showFamilyPlanningStockStatus')));
	}

	public function landingPage()
	{
		return View::make('landing-page');
	}

	// FIX ME : 
	// 1.move this code snippet to home model; create it if it's non-existant
	// 2.use list() in resultset to avoid foreach
	public function showFamilyPlanningStockStatus(){
		$result_set = DB::table('counties')->get();
		$counties = array();
		foreach ($result_set as $row) {
		   	$counties[$row->county] = $row->county;
		}

		$commodities = DB::table('fp_commodities')->lists('name', 'identifier');

		$years = function(){
			$theYears;
		    foreach (range(date('Y'), 2008) as $year) {
		       $theYears[$year] = $year;
		    }
		    return $theYears;
		};

		$months = function(){
			$theMonths["00"] = "--All months--";
		    for ($i=1; $i <=12 ; $i++) { 
		        $jd = gregoriantojd($i,13,1998);
		        $month = jdmonthname($jd,1);
		        $theMonths[($i < 10) ? "0$i" : $i] = $month;
		    }
		    return $theMonths;
		};

		$this->layout->with('active', 'service-statistics');
		$this->layout->content = View::make('service-statistics/home')
									->with('counties', $counties)
									->with('years', $years)
									->with('months', $months)
									->with('commodities', $commodities);
		$this->layout->custom_script = View::make('scripts/service-statistics', array('url'=>URL::to('csv-uploads')));
		$this->layout->custom_style = View::make('styles/service-statistics');
	}

	public function deleteFamilyPlanningStockData() {
		$period = Input::get('period');
		$num = DB::table('fpc_by_facility')->where('Period_code', 'like', "%$period%")->count();
		if ($num) {
			DB::table('fpc_by_facility')->where('Period_code', 'like', "%$period%")->delete();
			return $num;
		}
		return 0;
	}

	// TODO: delete also data in pubilc/php/files folder
	public function deleteLMISData() {
		$period = Input::get('period');
		$num = DB::table('lmis')->where('Period code', 'like', "%$period%")->count();
		if ($num) {
			DB::table('lmis')->where('Period code', 'like', "%$period%")->delete();
			
			$path = public_path()
					.DIRECTORY_SEPARATOR."php"
            		.DIRECTORY_SEPARATOR."files"
            		.DIRECTORY_SEPARATOR."LMIS_$period.csv";
			if (File::exists($path)) {
				Log::info("LMIS_$period.csv exists");
				File::delete($path);
				Log::info("LMIS_$period.csv has been deleted exists");
			}

			return $num;
		}
		return 0;	
	}

	public function twoPagerPDF() {
		$monthSelected =  Input::get('month-selected');
		$yearSelected = Input::get('year-selected');
		$dateGenerated = "$yearSelected-$monthSelected-05";

		$results = DB::table('fpcommodities')->lists('fp_name');
		foreach ($results as $fp_name) {
			$commodities[$fp_name] = [];
		}
		$commodity_names = $results;

		$getTwoYearsFromToday = function($dateValue) {
			$date = date_create($dateValue);
	        date_add($date, date_interval_create_from_date_string('2 years'));
	        $two_years_from_today = date_format($date, 'Y-m-d');
	        return $two_years_from_today;
		};

		$getTwoPagerData = function ($date_today, $store_id) use($commodities, $getTwoYearsFromToday){
	        $two_years_from_today = $getTwoYearsFromToday($date_today);
			$year_month = date_format(date_create($date_today), 'Y-m');
			
			$appendData = function($results, $category, $commodities){
				foreach ($results as $row) {
					$commodities[$row->commodity][$category] = ($row->total == null) ? 0 : number_format($row->total);
				}
				return $commodities;
			};

			// SOH
	       	$results = DB::table('fpcommodities as f')
	       				->select(DB::raw('f.fp_name as commodity, sum(p.fp_quantity) as total'))
				        ->leftJoin('pipeline as p', function($join) use($year_month, $store_id)
				        {
				            $join->on('f.id', '=', 'p.fpcommodity_Id')
				                 ->where('fp_date', 'like', "%$year_month%")
	             				 ->where('transaction_type', 'like', '%SOH%')
	             				 ->where('store_id', '=', $store_id);
				        })
				        ->groupBy('f.fp_name')
				        ->get();
			$commodities = $appendData($results, 'SOH', $commodities);

			// RECEIVED
			$results = DB::table('fpcommodities as f')
	       				->select(DB::raw('f.fp_name as commodity, sum(p.fp_quantity) as total'))
				        ->leftJoin('pipeline as p', function($join) use($year_month, $store_id)
				        {
				            $join->on('f.id', '=', 'p.fpcommodity_Id')
				                 ->where('date_receive', 'like', "%$year_month%")
	             				 ->where('transaction_type', 'like', "%RECEIVED%")
	             				 ->where('store_id', '=', $store_id);
				        })
				        ->groupBy('f.fp_name')
				        ->get();	
			$commodities = $appendData($results, 'RECEIVED', $commodities);		        

			// PENDING
			$results = DB::select(
						"select 
							f.fp_name as commodity, sum(p.fp_quantity) as total 
						from 
							fpcommodities as f 
							left join pipeline as p on f.id = p.fpcommodity_Id and 
							fp_date between ? and ? and 
							date_incountry like ? and 
							date_receive like ? and 
							transaction_type like ? and 
							store_id = ? 
						group by f.fp_name", 
						[$date_today, $two_years_from_today, '%0000-00-00%', '%0000-00-00%', '%PENDING%', $store_id]
						);
			$commodities = $appendData($results, 'PENDING', $commodities);
			return $commodities;
		};

		// retrieve 2-pager info for kemsa and psi using the closure above
		$kemsaData = $getTwoPagerData($dateGenerated, 1);
		$psiData = $getTwoPagerData($dateGenerated, 2);

		// retrieve the pending consignments for public sector pipeline
		$pendingConsignments = DB::table('pipeline as p')
								->select(
									DB::raw(
									   'fp.fp_name as commodity, 
										fp.unit as unit, 
										fs.funding_source as funding_source, 
										p.transaction_type as status,
										if (p.delay_to = "0000-00-00", p.fp_date, delay_to) as ETA, 
										if (p.qty_receive = 0, p.fp_quantity, p.fp_quantity - p.qty_receive) as qty_expected,
										p.qty_receive as qty_received,
										(if (p.qty_receive = 0, p.fp_quantity, p.fp_quantity - p.qty_receive) - p.qty_receive) as qty_remaining,
										DATEDIFF(NOW(), if (p.delay_to = "0000-00-00", p.fp_date, delay_to)) as no_of_days_delayed_by'
									)
								)
								->join('fpcommodities as fp', 'fp.id', '=', 'p.fpcommodity_Id')
								->join('funding_sources as fs', 'fs.id', '=', 'p.funding_source')
								->where(function($query)
					            {
					                $query->where('transaction_type', 'like', '%PENDING%')
					                      ->orWhere('transaction_type', 'like', '%INCOUNTRY%')
					                      ->orWhere('transaction_type', 'like', '%DELAYED%');
					            })
					            ->whereBetween('fp_date', [$dateGenerated, $getTwoYearsFromToday($dateGenerated)])
					            ->where('store_id', '=', '1')
					            ->get();

		$dateFormatter = function($dateValue) {
			$theDate = date_create($dateValue);
			$theDate = date_format($theDate, 'm-d-Y');
			$dateArray = explode("-", $theDate);
			$jd = gregoriantojd($dateArray[0], $dateArray[1], $dateArray[2]);
			
			//returns Month Day, Year e.g. May 08, 2014
			return jdmonthname($jd,0)." $dateArray[1], $dateArray[2]" ;
		};

		// this closure returns Month, Year e.g Jan, 2014
		$getSelectedPeriod = function($dateValue) {
			$pattern = '/(\w+) (\d+), (\d+)/i';
			$replacement = '${1}, $3';
			return preg_replace($pattern, $replacement, $dateValue);
		};
		$selectedPeriod = $getSelectedPeriod($dateFormatter($dateGenerated));

		date_default_timezone_set('Africa/Nairobi');
		$time_stamp = date('l jS \of F Y h:i:s A');

		$html = View::make('pdf/two-pager')
					->with('time_stamp', $time_stamp)
					->with('kemsaData', $kemsaData)
					->with('psiData', $psiData)
					->with('selectedPeriod', $selectedPeriod)
					->with('pendingConsignments', $pendingConsignments)
					->with('dateFormatter', $dateFormatter)
					->with('commodity_names', $commodity_names);
		return PDF::load($html, 'A4', 'portrait')->show("Family planning stock status report as at $monthSelected-$yearSelected");
	}

	public function rhtCountySummary($training = "LAPM") {
		$results = RHTraining::getRHDashboardTable($training);

		date_default_timezone_set('Africa/Nairobi');
		$time_stamp = date('l jS \of F Y h:i:s A');

		$html = View::make('pdf/rht-table')
					->with('time_stamp', $time_stamp)
					->with('training', $training)
					->with('results', $results);
		return PDF::load($html, 'A4', 'landscape')->show("rht-table");
	}

	public function customSummary() {
		$getPath = function($value) {
			$path = public_path().DIRECTORY_SEPARATOR."chart-images";
			if ($value == "to-file") {
				$path = $path.DIRECTORY_SEPARATOR;
			}
			return addslashes($path);
		};

		$files = scandir($getPath("to-folder"));
		$png = [];
		foreach ($files as $file) {
			if (ends_with($file, '.png')) {
				array_push($png, $getPath("to-file").$file);
			}
		}

		$html = View::make('pdf/custom-summary')
					->with('png', $png);
		
		return PDF::load($html, 'A4', 'portrait')->show("custom-summary");
	}
}