<?php
class ReportController extends BaseController {
	// TODO: check if there is a json method for elqouent/database query
	public function __construct() {
		$this->beforeFilter('csrf', array('on'=>'post'));
	   	/*$this->beforeFilter('auth', array('only'=>array(
	   										'testShellExecution')));*/
	}

	public function testShellExecution() {
		// $output = shell_exec('curl -H "Content-Type: application/json" -X POST -d \'{"infile":"{xAxis: {categories: [\"Jan\", \"Feb\", \"Mar\", \"Apr\", \"May\", \"Jun\", \"Jul\", \"Aug\", \"Sep\", \"Oct\", \"Nov\", \"Dec\"]},series: [{data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]}]};","callback":"function(chart) {chart.renderer.arc(200, 150, 100, 50, -Math.PI, 0).attr({fill : \"#FCFFC5\",stroke : \"black\",\"stroke-width\" : 1}).add();}","constr":"Chart", "outfile":"chart.png"}\' 127.0.0.1:3003');

		/*$data = '\'{"infile":"{xAxis: {categories: [\"Jan\", \"Feb\", \"Mar\", \"Apr\", \"May\", \"Jun\", \"Jul\", \"Aug\", \"Sep\", \"Oct\", \"Nov\", \"Dec\"]},series: [{data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]}]};","callback":"function(chart) {chart.renderer.arc(200, 150, 100, 50, -Math.PI, 0).attr({fill : \"#FCFFC5\",stroke : \"black\",\"stroke-width\" : 1}).add();}","constr":"Chart","outfile":"chart.png"}\'';
		$command = 'curl -H "Content-Type: application/json" -X POST -d '.$data.' 127.0.0.1:3003';
		$output = shell_exec($command);
		echo "<pre>$output</pre>";*/

		/*$data['Ending Balance'] = 'sum(`Injectables, FP (Ending Balance)`) as injectables, county';
		$data['Dispensed'] = 'sum(`Injectables, FP (Dispensed)`) as injectables, county';
		$data['Period'] = '2014-04';
		$data['Commodity'] = 'injectables';

		$returned_data = static::get_data($data);
		$data_to_write = $returned_data["descending"];
		static::writeToOptionsFile($data_to_write);
		$output = shell_exec(static::getCommand(1));

		$data_to_write = $returned_data["ascending"];
		static::writeToOptionsFile($data_to_write);
		$output = shell_exec(static::getCommand(2));

		$data['Ending Balance'] = 'sum(`Implants (1-Rod), FP (Ending Balance)`) + sum(`Implants (2-Rod), FP (Ending Balance)`) as implants, county';
		$data['Dispensed'] = 'sum(`Implants (1-Rod), FP (Dispensed)`) + sum(`Implants (2-Rod), FP (Dispensed)`) as implants, county';
		$data['Period'] = '2014-04';
		$data['Commodity'] = 'implants';

		$returned_data = static::get_data($data);
		$data_to_write = $returned_data["descending"];
		static::writeToOptionsFile($data_to_write);
		$output = shell_exec(static::getCommand(3));

		$data_to_write = $returned_data["ascending"];
		static::writeToOptionsFile($data_to_write);
		$output = shell_exec(static::getCommand(4));*/

		Report::generate_graph_for_national_stock();

		//echo "<pre>$output</pre>"; //comment this when testing for pdf generation

		static::generatePDF();
	}

	public function writeToOptionsFile($data_to_write){
		$options_file = public_path().DIRECTORY_SEPARATOR."report-charts".DIRECTORY_SEPARATOR.'column-graph-options.js';
		$fh = fopen($options_file, 'w') or die("can't open file");
		$string_json = static::generateJson($data_to_write);
		fwrite($fh, $string_json);
		fclose($fh);
	}

	public function getCommand($num){
		$phantom_js_src = public_path()
									.DIRECTORY_SEPARATOR."report-charts"
									.DIRECTORY_SEPARATOR."phantomjs";
		if (PHP_OS == "Darwin") {
			$phantom_js_src = $phantom_js_src
									.DIRECTORY_SEPARATOR."macos"
									.DIRECTORY_SEPARATOR."phantomjs";
			$command = 'cd report-charts && '.$phantom_js_src;
		} else if (PHP_OS == "WINNT") {
			$phantom_js_src = $phantom_js_src
									.DIRECTORY_SEPARATOR."windows"
									.DIRECTORY_SEPARATOR."phantomjs.exe";
			$command = 'cd report-charts & '.$phantom_js_src;
		}
		$chart = 'chart'.$num.'.png';
		$command = $command.'  highcharts-convert.js -infile column-graph-options.js -outfile '.$chart.' -scale 2.5 -width 500 ';
		Log::info($command);
		return $command;
	}

	public function generatePDF($period = "2014-04"){
		$original_period_format = $period;
		$period = str_replace("-", "", $period);
		$getPath = function($value) {
			$path = public_path().DIRECTORY_SEPARATOR."report-charts";
			if ($value == "to-file") {
				$path = $path.DIRECTORY_SEPARATOR;
			}
			return addslashes($path);
		};
		$files = scandir($getPath("to-folder"));
		$png = [];
		foreach ($files as $file) {
			if (count($png) == 1)
				break;
			if (ends_with($file, '.png')) {
				$file_name = basename($file, ".png");
				$png[$file_name] = $getPath("to-file").$file;
			}
		}
		$reporting_rate = Report::getCDRRReportingRate($period);
		arsort($reporting_rate);
		$national_contraceptive_services = Report::getNationalContraceptiveServices($original_period_format);
		$html = View::make('pdf/report')
					->with('png', $png)
					->with('national_contraceptive_services', $national_contraceptive_services)
					->with('reporting_rate', $reporting_rate);

		$outputName = "monthly-summary";					
		$pdfPath = public_path().DIRECTORY_SEPARATOR."report-charts".DIRECTORY_SEPARATOR.$outputName.'.pdf';
		File::put($pdfPath, PDF::load($html, 'A4', 'portrait')->output());
		var_dump("monthly summary pdf generated");
		
		// return PDF::load($html, 'A4', 'portrait')->show("monthly-summary");
	}

	public function generateJson($values, $title = "Month of Stock") {
		$categories = array_keys($values);
		$data = array_values($values);

		$json_array = [
			'chart' => [
		        "type" => 'column'
		    ],
		    "title" => [
		        "text" => $title
		    ],
		    "subtitle" => [
		        "text" => ''
		    ],
		    "xAxis" => [
		        "categories" => $categories,
		    ],
		    "yAxis" => [
		        "min" => 0,
		        "title" => [
		            "text" => ''
		        ],
		        "plotLines" => [[
		            "value" => 3,
		            "color" => 'red',
		            "width" => 2,
		        ], [
		            "value" => 6,
		            "color" => 'green',
		            "width" => 2,
		        ]]  
		    ],
		    "tooltip" => [
		        "headerFormat" => '<span style="font-size:10px">{point.key}</span><table>',
		        "pointFormat" => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
		        "footerFormat" => '</table>',
		        "shared" => true,
		        "useHTML" => true
		    ],
		    "plotOptions" => [
		        "column" => [
		            "pointPadding" => 0.2,
		            "borderWidth" => 0
		        ]
		    ],
		    "series" => [[
		        "name" => 'Month of Stock',
		        "data" => $data
		    ]]
		];
		return json_encode($json_array);
	}

	public function get_result_set($custom_column, $period, $commodity){
		$results = LMIS::select(
		                DB::raw(
		                   $custom_column
		                )
		            )
		            ->join('facility_summary', 'Organisation unit code', '=', 'facility_code')
		            ->where('Period code', '=', $period)
		            ->groupBy('county')
		            ->get()
		            ->toArray();

        $flattened = [];
        foreach ($results as $row) {
        	$injectables = $row[$commodity];
        	$county = $row['county'];
        	$flattened[$county] = $injectables;
        }
        return $flattened;
	}

	public function get_data($data) {
		$ending_balance_columns = $data['Ending Balance'];
		$dispensed_columns = $data['Dispensed'];
		$period = $data['Period'];
		$commodity = $data['Commodity'];

		$getPeriod = function($num) use($period){
			$date = new DateTime($period);
			$date->sub(new DateInterval("P".$num."M"));
			return $date->format('Ym');
		};

		$current = static::get_result_set(
						$ending_balance_columns,
						$getPeriod(0),
						$commodity);
		$dispensed_one = static::get_result_set(
							$dispensed_columns,
							$getPeriod(0),
							$commodity);
		$dispensed_two = static::get_result_set(
							$dispensed_columns,
							$getPeriod(1),
							$commodity);
		$dispensed_three = static::get_result_set(
							$dispensed_columns,
							$getPeriod(2),
							$commodity);

        $get_value = function($array, $value){
        	// return 0 is value is not set
        	return (isset($array[$value]))? $array[$value] : 0;
        };

        $counties = Counties::get(['county'])->toArray();
        $values = [];

        foreach ($counties as $row) {
        	$current_month = $get_value($current, $row['county']);
        	$month_one = $get_value($dispensed_one, $row['county']);
        	$month_two = $get_value($dispensed_two, $row['county']);
        	$month_three = $get_value($dispensed_three, $row['county']);
        	$three_month_total = $month_one + $month_two + $month_three;
        	$three_month_total = ($three_month_total > 0) ? $three_month_total : 1;
        	$moving_average = $three_month_total/3;
        	$values[$row['county']] = intval($current_month/$moving_average);
        }
        asort($values); //use 'arsort' for reverse sorting
        $ascending = array_slice($values, 0, 10);
        $descending = array_slice($values, count($values) - 10);
        return ["ascending" => $ascending, "descending" => $descending];
	}
}