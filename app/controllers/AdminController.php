<?php

class AdminController extends BaseController {

	protected $layout = "layouts.dashboard";
	public function __construct() {
	   	$this->beforeFilter('csrf', array('on'=>'post'));
	   	$this->beforeFilter('auth', array('only'=>array('listFacilities', 
	   													'newFacilities', 
	   													'listTraining', 
	   													'newTraining', 
	   													'editTraining',
														'addTraining',
														'postEditTraining',
														'deleteTraining')));
	}

	public function listFacilities(){
		$facilities = DB::table('facility_summary')->paginate(10);
		$this->layout->with('active', 'settings');
		$this->layout->content = View::make('admin/facilities/list')->with('facilities', $facilities);
		$this->layout->custom_script = "";
		$this->layout->custom_style = "";
	}

	public function newFacilities(){
		$districts = DB::table('districts')->lists('district', 'id') ;
		$this->layout->content = View::make('admin/facilities/new')->with('districts', $districts);
		$this->layout->custom_script = "";
		$this->layout->custom_style = "";	
	}

	// *********************************************TRAINING**************************************************************
	public function listTraining() {
		$training = DB::table('trainings')->paginate(8);
		$this->layout->with('active', 'settings');
		$this->layout->content = View::make('admin/training/list')->with('training', $training);
		$this->layout->custom_script = "";
		$this->layout->custom_style = "";
	}

	public function newTraining() {
		$this->layout->with('active', 'settings');
		$this->layout->content = View::make('admin/training/new');
		$this->layout->custom_script = "";
		$this->layout->custom_style = "";		
	}

	public function editTraining($id) {
		$this->layout->with('active', 'settings');
		$training = DB::table('trainings')->where('id', '=', $id)->first();
		$this->layout->content = View::make('admin/training/edit')->with('training', $training);
		$this->layout->custom_script = "";
		$this->layout->custom_style = "";		
	}

	public function addTraining() {
		Admin::add(Input::all());
		$this->layout->with('active', 'settings');
		return Redirect::route('list-training')->with('splash-message', 'Training record successfully added');
	}

	public function postEditTraining() {
		Admin::edit(Input::all());
	    return Redirect::route('list-training')->with('splash-message', 'Training record successfully edited');		
	}

	public function deleteTraining($id) {
		Admin::del($id);
		$this->layout->with('active', 'settings');
		return Redirect::route('list-training')->with('splash-message', 'Training record successfully deleted');
	}
	// ******************************************************************************************************************

}