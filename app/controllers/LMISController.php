<?php

class LMISController extends BaseController {

	protected $layout = "layouts.dashboard";
	public function __construct() {
	   	$this->beforeFilter('csrf', array('on'=>'post'));
	   	$this->beforeFilter('auth', array('only'=>array('showHome')));
	}

	public function showHome(){
		// $districts = array_merge(["all" => "--All Districts--"], DB::table('districts')->lists('district', 'district'));
		$districts = ["all" => "--All sub-counties--"];
		$counties = array_merge(["all" => "--All counties--"], DB::table('counties')->lists('county', 'county'));
		$years = function(){
			$theYears;
		    foreach (range(date('Y'), 2008) as $year) {
		       $theYears[$year] = $year;
		    }
		    return $theYears;
		};

		$months = function(){
		    for ($i=1; $i <=12 ; $i++) { 
		        $jd = gregoriantojd($i,13,1998);
		        $month = jdmonthname($jd,1);
		        $theMonths[($i < 10) ? "0$i" : $i] = $month;
		    }
		    return $theMonths;
		};

		$this->layout->with('active', 'LMIS');
		$this->layout->content = View::make('lmis/home')
									 ->with('months', $months())
									 ->with('years', $years())
									 ->with('districts', $districts)
									 ->with('counties', $counties);
		$this->layout->custom_script = View::make('scripts/lmis');
		$this->layout->custom_style = View::make('styles/lmis');
	}

}