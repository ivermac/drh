<?php

class AJAXController extends BaseController {
	// TODO: check if there is a json method for elqouent/database query
	public function __construct() {
		// $this->beforeFilter('csrf', array('on'=>'post'));
	   	$this->beforeFilter('auth', array('only'=>array(
	   										'deliverChart',
	   										'getLMISMOSData',
	   										'getFacilityOwnershipRHTTrainingBreakdown',
	   										'getPieChartNationalData', 
	   										'getBarGraphByCounty', 
	   										'getBarGraphByConsumption', 
	   										'getDistrictsInCounty', 
	   										'getCountiesInProvince', 
	   										'getFacilitiesInDistrict',
	   										'getLineGraphConsumptionTrend',
	   										'getStockStatusData',
	   										'exportCSVToMysql',
	   										'getStockStatusPSI',
	   										'getPercentageOfStaffTrained',
	   										'getPercentageOfFacilitiesWithAtleastOnePersonTrained',
	   										'getSupplyPlanVsMos',
	   										'postReproductiveHealthTrainingDetails')));
	   	$this->return = "not Ajax request";
	}

	// TODO: refactor by removing all calls to 'json_encode' and adding them to the model class responsible
	// TODO: write a test for the model that will be created; test for json return type

	public function deliverChart() {
		if (Request::ajax()){
			$svg = Input::get('svg');
			$chartName = Input::get('chartName');
			$folder_path = addslashes(
			    public_path()
			    .DIRECTORY_SEPARATOR."chart-images"
			    .DIRECTORY_SEPARATOR
			);

			file_put_contents($folder_path."$chartName.svg", $svg);

			$initial_file = $folder_path."$chartName.svg";
			$new_file = $folder_path."$chartName.png";
			if (PHP_OS == "WINNT") {
				// for 64 bit 
				$path = "C:"
						.DIRECTORY_SEPARATOR."Program Files"
						.DIRECTORY_SEPARATOR."ImageMagick-6.8.9-Q16"
						.DIRECTORY_SEPARATOR."convert";
				$convert = addslashes($path);
				$cmd = "\"$convert\" $initial_file $new_file";
			} 
			else if (PHP_OS == "FreeBSD") {} 
			else if (PHP_OS == "Linux") {}
				
			exec($cmd, $output, $rc);
			return json_encode($rc);
		}
		return $this->return;
	}

	public function getLMISMOSData() {
		if (Request::ajax()){
			$county = Input::get('county');
			$district = Input::get('district');
			$year = Input::get('year');
			$month = Input::get('month');

			return json_encode(LMIS::getLMISMOS("$year-$month", ($county == "all") ? "" : $county, ($district == "all") ? "" : $district));
		}
		return $this->return;
	}

	public function getStockOnHandRecord() {
		if (Request::ajax()){
			$id = Input::get('id');
			return json_encode(DB::table('pipeline')->where('id', '=', $id)->first());
		}
		return $this->return;
	}

	public function getFacilityOwnershipRHTTrainingBreakdown() {
		if (Request::ajax()){
			$region = Input::get('region');
			$level = Input::get('level');
			$training = Input::get('training');
			return json_encode(RHTraining::getFacilityOwnershipRHTTrainingBreakdown($region, $level, $training));
		}
		return $this->return;
	}

	public function exportCSVToMysql() {
		if (Request::ajax()){
			$filename = Input::get('filename');
			$module = Input::get('module');
			return json_encode(FPC::exportCSVToMysql($filename, $module));
		}
		return $this->return;
	}

	public function getPercentageOfStaffTrained() {
		if (Request::ajax()){
			$training = Input::get('training');
			$owner = Input::get('owner');
			$former_province = Input::get('former-province');
			$county = Input::get('county');
			return json_encode(RHTraining::getPersonnelTrainingData($training, $former_province, $county, $owner));
		}
		return $this->return;
	}

	public function getPercentageOfFacilitiesWithAtleastOnePersonTrained() {
		if (Request::ajax()){
			$training = Input::get('training');
			$owner = Input::get('owner');
			$former_province = Input::get('former-province');
			$county = Input::get('county');
			return json_encode(RHTraining::getDataOfFacilitiesWithAtleastOnePersonTrained($training, $former_province, $county, $owner));
		}
		return $this->return;
	}

	public function getSupplyPlanVsMos() {
		if (Request::ajax()){
			$year = Input::get('year');
			$commodity_id = Input::get('commodity_id');
			return json_encode(FPC::getSupplyPlanVsMos($year, $commodity_id));
		}
		return $this->return;
	}

	public function postReproductiveHealthTrainingDetails() {
		if (Request::ajax()){
			$id = Input::get('id');
			return json_encode(FPC::getReproductiveHealthTrainingDetails($id));
		}
		return $this->return;
	}

	public function getPieChartNationalData() {
		if (Request::ajax()){
			$year = Input::get('year');
			$county = Input::get('county');
			return json_encode(FPC::getCommoditiesTotals($year, $county));
		}
		return $this->return;
	}

	public function getStockStatusData() {
		if (Request::ajax()){
			$supplier = Input::get('supplier');
			$dateSelected = Input::get('dateSelected');
			return json_encode(FPC::getStockStatusData($supplier, $dateSelected));
		}
		return $this->return;
	}

	public function getStockStatusPSI() {
		if (Request::ajax()){
			return json_encode(FPC::getStockStatusPSI());
		}
		return $this->return;
	}

	public function getLineGraphConsumptionTrend() {
		if (Request::ajax()){
			$county = Input::get('county');
			$year = Input::get('year');
			$commodity = Input::get('commodity');
			return json_encode(FPC::getConsumptionTrends($county, $year, $commodity));
		}
		return $this->return;
	}

	public function getBarGraphByCounty() {
		if (Request::ajax()){
			$county = Input::get('county');
			$year = Input::get('year');
			$month = Input::get('month');
			return json_encode(FPC::getTotalsByCounty($county, $year, $month));
		}
		return $this->return;
	}

	public function getBarGraphByConsumption() {
		if (Request::ajax()){
			$commodity = Input::get('commodity');
			$year = Input::get('year');
			$month = Input::get('month');
			return json_encode(FPC::getConsumptionByCounty($commodity, $year, $month));
		}
		return $this->return;
	}

	// FIX ME: 
	// 1.move logic code to model 
	// 2.use list() in query to avoid foreach
	public function getDistrictsInCounty() {
		if (Request::ajax()){
			$county = Input::get('id');
			$countyName = Input::get('county-name');
			if (!empty($county)) {
				$data = Districts::select('id', 'district')->whereCounty($county)->lists('district', 'id');
			} else {
				$rs = DB::table('counties')->where('county', '=', $countyName)->first();
				$data = DB::table('districts')->where('county', '=', $rs->id)->lists('district', 'district');
			}
			return json_encode($data);
		}
		return $this->return;
	}

	public function getCountiesInProvince() {
		if (Request::ajax()){
			$former_province = Input::get('id');
			$data = DB::table('counties')->where('former_province', '=', $former_province)->lists('county', 'county');
			return json_encode($data);
		}
		return $this->return;
	}

	// FIX ME: 
	// 1.move logic code to model
	// 2.use list() in query to avoid foreach
	public function getFacilitiesInDistrict() {
		if (Request::ajax()){
			$district = Input::get('id');

			$results = Facilities::select('facility_code', 'facility_name')->whereDistrict($district)->get();
			$data = [];
			foreach ($results as $rs) {
			   $data[$rs->facility_code] = $rs->facility_name;
			}
			return json_encode($data);
		}
		return $this->return;
	}

}