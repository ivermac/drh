<?php

class POSTController extends BaseController {
	// TODO: check if there is a json method for elqouent/database query
	public function __construct() {
		$this->beforeFilter('csrf', array('on'=>'post'));
	   	$this->beforeFilter('auth', array('only'=>array(
	   										'postNewfundingSources',
	   										'postAddTrainingDetails',
	   										'postEditTrainingDetails',
	   										'postNewConsignment',
	   										'postEditPipeline',
	   										'postStockOnHand')));
	}

	public function postNewfundingSources() {
		$funding_source = Input::get('funding-source');
		$procuring_agency = Input::get('procuring-agency');
		$service_status = Input::get('service-status');
		$funding_source_id = Input::get('funding-source-id');
		$table = DB::table('funding_sources');
		$data = array(
					'funding_source' => $funding_source,
					'procuring_a' => $procuring_agency,
					'service_active' => $service_status,
				);

		if ( !empty($funding_source_id) ) {
			$table->where('id', $funding_source_id)
	              ->update($data);
		} else {
			$table->insert($data);
		}

		return Redirect::route('funding-sources-listing')
				->with('success-message', 'The record has been successfully added');
	}

	public function postNewCommodity() {
		$commodity_name = Input::get("commodity-name");
		$commodity_description = Input::get("commodity-description");
		$commodity_unit = Input::get("commodity-unit");
		$monthly_consumption_kemsa = Input::get("monthly-consumption-kemsa");
		$monthly_consumption_psi = Input::get("monthly-consumption-psi");
		$commodity_date_as_of = Input::get("commodity-date-as-of");
		$commodity_id = Input::get('commodity-id');

		$table = DB::table('fpcommodities');
		$data = array(
					'fp_name' => $commodity_name,
					'Description' => $commodity_description,
					'Unit' => $commodity_unit,
					'projected_monthly_c' => $monthly_consumption_kemsa,
					'projected_psi' => $monthly_consumption_psi,
					'as_of' => $commodity_date_as_of,
				);

		if (! empty($commodity_id) ) {
			$table->where('id', $commodity_id)
	              ->update($data);
		} else {
			$table->insert($data);
		}

		return Redirect::route('commodities-listing')
				->with('success-message', 'The record has been successfully added');
	}

	public function postNewConsignment() {
		// NB: these fields are arrays
		$commodity = Input::get('commodity');
		$store = Input::get('store');
		$funding_source = Input::get('funding-source');
		$ETA = Input::get('ETA');
		$procuring_agency = Input::get('procuring-agency');
		$quantity = Input::get('quantity');

		foreach ($commodity as $index => $value) {
			DB::table('pipeline')
			->insert(
				array(
					'store_id' => $store[$index],  
					'fpcommodity_Id' => $commodity[$index],  
					'funding_source' => $funding_source[$index],  
					'procuring_agency' => $procuring_agency[$index],  
					'fp_quantity' => $quantity[$index], 
					'fp_date' => $ETA[$index],  
					'transaction_type' => 'PENDING',
					)
				);
		}

		return Redirect::route('supply-plan-listing')
				->with('success-message', 'The record has been successfully added');
	}

	public function postEditPipeline() {
	  $id = Input::get('id');
	  $fp_commodity_id = Input::get('fp-commodity');
	  $store_id = Input::get('store');
	  $comment = Input::get('comment');
	  $funding_source = Input::get('funding-source');
	  $qty_expected = Input::get('quantity-expected');
	  $date_expected = Input::get('date-expected');
	  $transaction_type = Input::get('status');
	  $delayed_to = Input::get('date-delayed-to');
	  $date_incountry = Input::get('date-incountry');
	  $qty_incountry = Input::get('quantity-incountry');
	  $cancel_date = Input::get('date-cancelled');
	  $date_receive = Input::get('date-received');
	  $qty_receive = Input::get('quantity-received');
	  $edit_array = array(
        		'store_id' => $store_id,  
        		'fpcommodity_Id' => $fp_commodity_id,  
        		'funding_source' => $funding_source,
        		'fp_quantity' => $qty_expected,  
        		'date_incountry'=>  $date_incountry, 
        		'qty_incountry' => $qty_incountry,  
        		'fp_date' => $date_expected,  
        		'transaction_type' => $transaction_type,  
        		'date_receive' => $date_receive,  
        		'qty_receive' => $qty_receive,  
        		'delay_to' => $delayed_to,  
        		'comment' => $comment,  
        		'cancel_date' => $cancel_date,
        	);
		DB::table('pipeline')->where('id', $id)->update(
        	$edit_array
		);

		if ($transaction_type == "RECEIVED") {
			return Redirect::route('received-commodities-listing')
					->with('success-message', 'The record has been successfully edited');
		}
		return Redirect::route('supply-plan-listing')
				->with('success-message', 'The record has been successfully edited');
	}

	// FIX ME: move logic code to model
	public function postAddTrainingDetails(){
		$validator = Validator::make(Input::all(), RHTraining::rules());
		if ($validator->fails())
		{
			return Redirect::back()
					->with('error-message', 'kindly select a facility')
					->withErrors($validator)
					->withInput();
		}

		// NB: these fields are arrays
		$first_name = Input::get('first-name');
		$surname = Input::get('surname');
		$personnel_no = Input::get('personnel-no');
		$qualification = Input::get('qualification');
		$station = Input::get('station');
		$selected_training = Input::get('training');
		$training_dates = Input::get('training-date');
		$facility_code = Input::get('facility-code');

		$trainings = []; //to store key-value data of id and name of selected training
		$user_trainings = []; //to store the trainings a single user has done

		if (!empty($selected_training) ){
			//get trainings from the db table that match the trainings selected
			$result_set = DB::table('trainings')->whereIn('id', array_keys($selected_training))->get();
			foreach ($result_set as $row) {
				$trainings[$row->id] = $row->name; 
			}
		}

		// for each cloned row... (i.e. for each rht entry which is simply a single user record)
		foreach ($first_name as $index => $value) {
			if (!empty($selected_training) ){
				// for each selected training...
				foreach ($selected_training as $key => $value) {
					// if the index of the row is a key in the selected training i.e
					// if that particular user has done a training (by 'done' i mean the training has been checked )...
					if (in_array($index, array_keys($value))){
						$training = $trainings[$key]; //get the name of the training
						$training_date = $training_dates[$key][$index]; //get the value of the date field i.e. when training took place
						$user_trainings[$training] = (empty($training_date)) ? 'Y' : $training_date;
					}
				}
			}

			DB::table('rhtraining')
			->insert(
				array(
					'first_name' => $first_name[$index],
					'surname' => $surname[$index],
					'personnel_no' => $personnel_no[$index],
					'qualification' => $qualification[$index],
					'station' => $station[$index],
					'training' => json_encode($user_trainings),
					'facility_code' => $facility_code,
					)
				);
			$user_trainings = [];
		}
		
		return Redirect::route('reproductive-health-training-list')
				->with('success-message', 'New record(s) successfully added');
	}

	// FIX ME: move logic code to model
	public function postEditTrainingDetails(){
		$validator = Validator::make(Input::all(), RHTraining::rules());
		if ($validator->fails())
		{
			return Redirect::back()
					->with('error-message', 
						'All text fields should be filled, all dropdowns should be selected and the personal no value should be numeric')
					->withErrors($validator)
					->withInput();
		}

		$id = Input::get('id');
		$first_name = Input::get('first-name');
		$surname = Input::get('surname');
		$personnel_no = Input::get('personnel-no');
		$qualification = Input::get('qualification');
		$station = Input::get('station');
		$selected_training = Input::get('training');
		$training_dates = Input::get('training-date');
		$facility_code = Input::get('facility-code');

		$result_set = DB::table('trainings')->whereIn('id', array_keys($selected_training))->get();
		$training = array();
		foreach ($result_set as $row) {
			$training[$row->name] = (empty($training_dates[$row->id])) ? 'Y' : $training_dates[$row->id];
		}

		DB::table('rhtraining')
		            ->where('id', $id)
		            ->update(
		            	array(
		            		'first_name' => $first_name,
							'surname' => $surname,
							'personnel_no' => $personnel_no,
							'qualification' => $qualification,
							'station' => $station,
							'training' => json_encode($training),
							'facility_code' => $facility_code,
		            		)
		            	);
		
		return Redirect::route('reproductive-health-training-list')
				->with('success-message', 'The record has been successfully edited');
	}

	public function postStockOnHand() {

		// NB: these fields are arrays
		$commodity = Input::get('commodity');
		$store = Input::get('store');
		$date_as_of = Input::get('date-as-of');
		$quantity = Input::get('quantity');
		$id = Input::get('id');

		if (!empty($id)) {
			DB::table('pipeline')
	            ->where('id', $id)
	            ->update(
	            	array(
	            		'fpcommodity_Id' => $commodity,
						'store_id' => $store,
						'fp_date' => $date_as_of,
						'fp_quantity' => $quantity,
						'transaction_type' => "SOH"
	            		)
	            	);
	        $return_message = "The record has been successfully edited";
		} else {
			foreach ($commodity as $index => $value) {
				DB::table('pipeline')
				->insert(
					array(
						'fpcommodity_Id' => $commodity[$index],
						'store_id' => $store[$index],
						'fp_date' => $date_as_of[$index],
						'fp_quantity' => $quantity[$index],
						'transaction_type' => "SOH"
						)
					);
			}
	        $return_message = "The record has been successfully added";
		}

		return Redirect::route('detailed-stock-on-hand')
				->with('success-message', $return_message);
	}
}