{{ HTML::script('js/chosen.jquery.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.core.js') }}
{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.datepicker.js') }}
<script type="text/javascript">
    var
        urlCalImagePath = "{{ URL::asset('images/calendar.gif') }}",
        urlGetDistrictsInCounty = "{{ URL::route('get-districts-in-county') }}",
        urlGetStockStatusData = "{{ URL::route('get-stock-status-data') }}",
        urlGetSupplyPlanVsMos = "{{ URL::route('get-supply-plan-vs-mos') }}",
        urlGetFacilitiesInDistricts = "{{ URL::route('get-facilities-in-district') }}",
        urlDeleteRHTRecord = "{{ URL::to('commodities/reproductive-health-training/delete/') }}",
        urlViewDetailOfRHTRecord = "{{ URL::route('get-reproductive-health-training-details') }}";
</script>
{{ HTML::script('js/custom/commodities-add-soh.js') }}
