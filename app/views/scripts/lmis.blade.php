{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery.iframe-transport.js') }}
{{ HTML::script('js/jquery.fileupload.js') }}
{{ HTML::script('js/highcharts/highcharts.js') }}
{{ HTML::script('js/highcharts/modules/exporting.js') }}
{{-- HTML::script('js/highcharts/modules/drilldown.js') --}}
<script type="text/javascript">
	var
		urlLoadingImage = "{{ URL::to('images/loading.gif') }}",
		urlUploadProcessor = "{{ URL::to('php/index.php') }}",
		urlGetExportCsvToMysql = "{{ URL::route('export-csv-to-mysql') }}",
		urlGetDistrictsInCounty = "{{ URL::route('get-districts-in-county') }}",
		urlGetLMISMOSData = "{{ URL::route('get-lmis-mos-data') }}",
		urlLMISDataDelete = "{{ URL::route('LMIS-data-delete') }}",
		loadingImage = $('<img>', {alt: "...", src: urlLoadingImage });
</script>
{{ HTML::script('js/custom/lmis.js') }}