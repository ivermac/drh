{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery.iframe-transport.js') }}
{{ HTML::script('js/jquery.fileupload.js') }}
<!-- <script src="http://code.highcharts.com/highcharts.js"> </script>
<script src="http://code.highcharts.com/modules/exporting.js"> </script>
<script src="http://code.highcharts.com/modules/drilldown.js"> </script> -->
{{ HTML::script('js/highcharts/highcharts.js') }}
{{ HTML::script('js/highcharts/modules/exporting.js') }}
{{ HTML::script('js/highcharts/modules/drilldown.js') }}
<script type="text/javascript">
	var
		urlLoadingImage = "{{ URL::to('images/loading.gif') }}",
		urlDashboard = "{{ URL::route('family-planning-stock-status') }}"
		urlGeneratePieChartNationalData = "{{ URL::route('generate-pie-chart-national-data') }}",
		urlGenerateBarGraphByCounty = "{{ URL::route('generate-bar-graph-by-county') }}",
		urlGenerateBarGraphByConsumption = "{{ URL::route('generate-bar-graph-by-consumption') }}",
		urlFamilyPlanningStockDelete = "{{ URL::route('family-planning-stock-delete') }}",
		urlUploadProcessor = "{{ URL::to('php/index.php') }}",
		urlGetExportCsvToMysql = "{{ URL::route('export-csv-to-mysql') }}",
		urlDeliverChart = "{{ URL::route('deliver-chart') }}",
		urlGetLineGraphConsumptionTrend = "{{ URL::route('get-line-graph-consumption-trend') }}",
		loadingImage = $('<img>', {alt: "...", src: urlLoadingImage });
</script>
{{ HTML::script('js/custom/service-statistics.js') }}
<script type="text/javascript">
	$(function() {
		service_statistics.init();
	})
</script>