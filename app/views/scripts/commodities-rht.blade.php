{{ HTML::script('js/highcharts/highcharts.js') }}
{{ HTML::script('js/highcharts/highcharts-more.js') }}
{{ HTML::script('js/highcharts/modules/exporting.js') }}
{{ HTML::script('js/highcharts/modules/drilldown.js') }}
{{ HTML::script('js/chosen.jquery.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.core.js') }}
{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.datepicker.js') }}
{{-- HTML::script('js/jquery.dataTables.min.js') --}}
<script type="text/javascript">
    var
        urlCalImagePath = "{{ URL::asset('images/calendar.gif') }}",
        urlGetDistrictsInCounty = "{{ URL::route('get-districts-in-county') }}",
        urlGetFacilitiesInDistricts = "{{ URL::route('get-facilities-in-district') }}",
        urlDeleteRHTRecord = "{{ URL::to('commodities/reproductive-health-training/delete/') }}",
        urlViewDetailOfRHTRecord = "{{ URL::route('get-reproductive-health-training-details') }}",
        urlGetFacilityOwnershipRHTTrainingBreakdown = "{{ URL::route('get-facility-ownership-rht-training-breakdown') }}",
        urlDashboard = "{{ URL::route('family-planning-stock-status') }}";
</script>
{{ HTML::script('js/custom/commodities-rht.js') }}
