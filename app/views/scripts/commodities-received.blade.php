{{ HTML::script('js/jquery-ui/jquery.ui.core.js') }}
{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.datepicker.js') }}
<script type="text/javascript">
    var
    	urlDeleteSupplyPlanRecord = "{{ URL::to('commodities/supply-plan/delete/') }}",
        urlLoadingImage = "{{ URL::to('images/loading.gif') }}",
        loadingImage = $('<img>', {alt: "...", src: urlLoadingImage });
</script>
{{ HTML::script('js/custom/commodities-received.js') }}