{{ HTML::script('js/chosen.jquery.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.core.js') }}
{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.datepicker.js') }}
<script type="text/javascript">
    var
        urlCalImagePath = "{{ URL::asset('images/calendar.gif') }}",
        urlDeleteSupplyPlanRecord = "{{ URL::to('commodities/supply-plan/delete/') }}";
</script>
{{ HTML::script('js/custom/list-supply-plan.js') }}
