{{ HTML::script('js/highcharts/highcharts.js') }}
{{ HTML::script('js/highcharts/highcharts-more.js') }}
{{ HTML::script('js/highcharts/modules/exporting.js') }}
{{ HTML::script('js/highcharts/modules/drilldown.js') }}
{{ HTML::script('js/jquery.ui.widget.js') }}
<script type="text/javascript">
    var
        urlGetFacilityOwnershipRHTTrainingBreakdown = "{{ URL::route('get-facility-ownership-rht-training-breakdown') }}",
        urlGetPercentageOfStaffTrained = "{{ URL::route('get-percentage-of-staff-trained') }}",
        urlGetCountiesInProvince = "{{ URL::route('get-counties-in-province') }}",
        urlGetPercentageOfFacilitiesWithAtleastOnePersonTrained = "{{ URL::route('get-percentage-of-facilities-with-atleast-one-person-trained') }}",
        urlRHTCountySummary = "{{ URL::to('rht-county-summary') }}";
</script>
{{ HTML::script('js/custom/commodities-rht-dashboard.js') }}
