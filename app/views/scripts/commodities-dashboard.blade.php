{{ HTML::script('js/highcharts/highcharts.js') }}
{{ HTML::script('js/highcharts/highcharts-more.js') }}
{{ HTML::script('js/highcharts/modules/exporting.js') }}
{{ HTML::script('js/highcharts/modules/data.js') }}
{{ HTML::script('js/chosen.jquery.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.core.js') }}
{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.datepicker.js') }}
<script type="text/javascript">
    var
        urlCalImagePath = "{{ URL::asset('images/calendar.gif') }}",
        urlGetStockStatusData = "{{ URL::route('get-stock-status-data') }}",
        urlGetSupplyPlanVsMos = "{{ URL::route('get-supply-plan-vs-mos') }}",
        urlLoadingImage = "{{ URL::to('images/loading.gif') }}",
        loadingImage = $('<img>', {alt: "...", src: urlLoadingImage });
</script>
{{ HTML::script('js/custom/commodities-dashboard.js') }}
<script type="text/javascript">
	$(function() {
		commodities_dashboard.init();
	})
</script>