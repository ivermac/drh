{{ HTML::script('js/jquery.dataTables.min.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.core.js') }}
{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.datepicker.js') }}
<script type="text/javascript">
    var
    	urlCalImagePath = "{{ URL::asset('images/calendar.gif') }}",
    	urlGetStockOnHandRecord = "{{ URL::route('get-stock-on-hand-record') }}",
        urlDeleteStockOnHandRecord = "{{ URL::to('commodities/stock-on-hand/delete/') }}";
</script>
{{ HTML::script('js/custom/detailed-stock-hand.js') }}