{{ HTML::script('js/chosen.jquery.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.core.js') }}
{{ HTML::script('js/jquery.ui.widget.js') }}
{{ HTML::script('js/jquery-ui/jquery.ui.datepicker.js') }}
<script type="text/javascript">
    var
        urlCalImagePath = "{{ URL::asset('images/calendar.gif') }}";
</script>
{{ HTML::script('js/custom/edit-pipeline.js') }}
