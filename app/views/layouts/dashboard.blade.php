
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

        <title>Division of Reproductive Health</title>
        {{ HTML::style('images/coat-of-arms.png', array('type'=>'image/x-icon', 'rel'=>'icon')) }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('css/starter-template.css') }}
        {{ HTML::style('css/general.css') }}
        {{ $custom_style }}

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <!--navigation-->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#">Division Of Reproductive Health</a> -->
                    <a class="navbar-brand" href="#">Reproductive & Maternal Health Services Unit</a>
                </div>
                <div class="collapse navbar-collapse">
                    @include('navigation')
                </div>
            </div>
        </div>
        <!--navigation-->

        <!--content-->
        <div class="container">
            {{ $content }}
        </div>
        <!--content-->

        <!-- downlaod modal -->
        <div class="modal fade" id="download-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Download</h4>
                    </div>
                    {{ Form::open(array('route' => 'two-pager-pdf', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form' )) }}
                    <div class="modal-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Year</label>
                                <div class="col-sm-10">
                                {{ Form::select(
                                    'year-selected', 
                                    call_user_func(function(){
                                        foreach (range(date('Y'), 2010) as $year) {
                                           $theYears[$year] = $year;
                                        }
                                        return $theYears;
                                    }), 
                                    '', 
                                    array('id'=>'year-selected', 'class'=>'form-control')) 
                                }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Month</label>
                                <div class="col-sm-10">
                                {{ Form::select(
                                    'month-selected', 
                                    call_user_func(function(){
                                        for ($i=1; $i <=12 ; $i++) { 
                                            $jd = gregoriantojd($i,13,1998);
                                            $month = jdmonthname($jd,1);
                                            $theMonths[($i < 10) ? "0$i" : $i] = $month;
                                        }
                                        return $theMonths;
                                    }), 
                                    '', 
                                    array('id'=>'month-selected', 'class'=>'form-control')) 
                                }}
                                </div>
                            </div>
                        {{-- @include('download.accordion') --}}
                    </div>
                    <div class="modal-footer">
                        {{ Form::submit('Preview PDF', array('class'=>'btn btn-success')) }}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- download modal -->

        <!--js scripts-->
        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
        {{ HTML::script('js/jquery-1.9.1.min.js') }}
        {{ HTML::script('js/bootstrap.min.js') }}
        {{ $custom_script }}
        <!--js scripts-->
    </body>
</html>
