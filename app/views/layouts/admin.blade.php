
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

        <title>Division of Reproductive Health</title>
        {{ HTML::style('images/coat-of-arms.png', array('type'=>'image/x-icon', 'rel'=>'icon')) }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('css/starter-template.css') }}
        {{ HTML::style('css/general.css') }}
        {{ $custom_style }}

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <!--content-->
        <div class="container">
        	<div class="col-md-12">
          		{{ $content }}
        	</div>
        </div>
        <!--content-->

        <!--js scripts-->
        {{ HTML::script('js/jquery-1.9.1.min.js') }}
        {{ HTML::script('js/bootstrap.min.js') }}
        {{ $custom_script }}
        <!--js scripts-->
  </body>
</html>
