<h3>Stock on Hand</h3>
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<td>FP Commodity</td>
					<td>Unit</td>
					<td>Available Quantity</td>
					<td>Stock Date as of</td>
					<td>Store</td>
					<td>Action</td>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		<tr>
		  			<td>demo data</td>
		  			<td>demo data</td>
		  			<td>demo data</td>
		  			<td>demo data</td>
		  			<td>demo data</td>
		  			<td>demo data</td>
		  		</tr>
		  	</tbody>
		</table>
	</div>
</div>