@extends('layouts.error')

@section('content')
	<p><h1>Fatal Error</h1></p>
	<div>{{ var_dump($exception) }}</div>
@stop