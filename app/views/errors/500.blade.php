@extends('layouts.error')

@section('content')
	<p><h1>Error Code {{ $exception->getStatusCode() }}</h1></p>
	<div>{{ $exception }}</div>
@stop