@extends('layouts.error')

@section('content')
	<p><h1>Oops, page not found!</h1></p>
	<p>A couple of things might have happended:</p>
	<ul>
		<li>The file may have been dropped or deleted</li>
		<li>You may have mistyped the URL. Please check your spelling</li>
	</ul>
@stop