<!-- Static Modal -->
<div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center">
                     {{ Form::image(URL::asset('images/loading.gif'), '', array('class' => 'icon' )) }}
                    <!-- <img src="http://www.travislayne.com/images/loading.gif" class="icon" /> -->
                    <h4>Processing... </h4>
                </div>
            </div>
        </div>
    </div>
</div>
