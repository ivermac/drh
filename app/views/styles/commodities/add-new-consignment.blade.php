{{ HTML::style('css/chosen/chosen.min.css') }}
{{ HTML::style('css/jquery-ui-themes/custom-theme/minified/jquery-ui.min.css') }}

<style type="text/css">
	.add-row, .remove-row {
		padding: 5px 10px 5px 10px;
	}

	.input-group .input-group-addon {
		border-radius: 0px !important;
	}

	.add-new-consignment-inputs {
	    padding: 2px;
	    height: 30px;
	}

	.ETA {
	    height: 30px;
	}

	#add-new-consignment-table-body .input-group {
	    width: 100px;
	}
</style>