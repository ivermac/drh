<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#download-service-stats">
                    Service Statistics
                </a>
            </h4>
        </div>
        <div id="download-service-stats" class="panel-collapse collapse in">
            <div class="panel-body">
               ...
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#download-commodities">
                    Commodities
                </a>
            </h4>
        </div>
        <div id="download-commodities" class="panel-collapse collapse">
            <div class="panel-body">
                ...
            </div>
        </div>
    </div>
</div>
