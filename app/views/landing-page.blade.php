
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Division of Reproductive Health</title>

    {{ HTML::style('images/coat-of-arms.png', array('type'=>'image/x-icon', 'rel'=>'icon')) }}
    {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('css/sticky-footer.css') }}
    {{ HTML::style('css/main.css') }}
    {{ HTML::style('css/general.css') }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Wrap all page content here -->
    <div id="wrap">
        <div class="container">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-2">
                        {{ HTML::image('images/coat-of-arms-logo.png', '...') }}
                    </div>
                    <div class="col-md-8">
                        <h1>Ministry of Health</h1>
                        <h2>Health Commodities Management Platform</h2>
                        <h3>Division Of Reproductive Health(DRH)</h3>
                    </div>
                </div>
            </div>
            <div class="row sign-in-form">
                @if(Session::has('message'))
                    <p class="alert-message {{ (Session::has('custom-alert')) ? Session::get('custom-alert') : 'alert-message-danger' }} alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </p>
                @endif
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::open(array('route' => 'users-login', 'method' => 'post', 'class'=>'form-horizontal')) }}
                        <fieldset>

                            <legend><h3>Sign In</h3></legend>

                            <div class="form-group">
                                <label class="col-md-1 control-label" for="email"></label>  
                                <div class="col-md-12">
                                    <input id="email" name="email" type="text" placeholder="Email" class="form-control input-md">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-1 control-label" for="password"></label>
                                <div class="col-md-12">
                                    <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-1 control-label" for="login"></label>
                                <div class="col-md-5">
                                    <button id="login" name="login" class="btn btn-block btn-primary">Login</button>
                                </div>
                                <div class="col-md-5">
                                    <button id="forpass" name="forpass" class="btn btn-block btn-primary">Forgot password?</button>
                                </div>  
                            </div>
                        </fieldset>
                    {{ Form::close() }}
                </div>
            </div> <!--./row -->
        </div> <!--./container -->
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">Government of Kenya © 2014. All Rights Reserved</p>
      </div>
    </div>
    {{ HTML::script('js/jquery-1.9.1.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
  </body>
</html>
