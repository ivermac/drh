<!DOCTYPE html>
<html>
	<head>
		<title>Monthly Summary</title>
		<style type="text/css">
			h1, h2, h3, h4, h5, h6, body {
			   font-family: DejaVu Serif;
			   font-size: 12px;
			}
			.table-div {
				float:left;
				width:250px; 
			}
			p {
				text-align: center;
			}

			.row {
				width: 100%;
				text-align: center;
			}

		</style>
	</head>
	<body>
		<div class="row" id="national-stock-status">
			@if (isset($png["national-stock-status"]))
			<p>National Stock Status</p>
			<div class="graph"><img src={{ $png["national-stock-status"] }} alt='...'></div>
			@endif
		</div>
		<div class="row" id="implants-top-ten">
			@if (isset($png["implants-top-ten"]))
			<p>Implants Top Ten</p>
			<div class="graph"><img src={{ $png["implants-top-ten"] }} alt='...'></div>
			@endif
		</div>
		<div class="row" id="implants-bottom-ten">
			@if (isset($png["implants-bottom-ten"]))
			<p>Implant Bottom Ten</p>
			<div class="graph"><img src={{ $png["implants-bottom-ten"] }} alt='...'></div>
			@endif
		</div>
		<div class="row" id="injectables-top-ten">
			@if (isset($png["injectables-top-ten"]))
			<p>Injectables Top Ten</p>
			<div class="graph"><img src={{ $png["injectables-top-ten"] }} alt='...'></div>
			@endif
		</div>
		<div class="row" id="injectables-bottom-ten">
			@if (isset($png["injectables-bottom-ten"]))
			<p>Injectables Bottom Ten</p>
			<div class="graph"><img src={{ $png["injectables-bottom-ten"] }} alt='...'></div>
			@endif
		</div>
		<div class="row" id="national-contraceptive-serives">
			@if (isset($national_contraceptive_services))
				<table>
					<thead>
						<tr>
							<th>Month</th>
							<th>POPs</th>
							<th>COCs</th>
							<th>IUCDs</th>
							<th>Implants</th>
							<th>FP Injections</th>
							<th>Condoms</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($national_contraceptive_services as $period => $data)
						<tr>
							<td>{{ $period }}</td>
							<td>{{ $data['POPs'] }}</td>
							<td>{{ $data['COCs'] }}</td>
							<td>{{ $data['IUCDs'] }}</td>
							<td>{{ $data['Implants'] }}</td>
							<td>{{ $data['Injections'] }}</td>
							<td>{{ $data['Condoms'] }}</td>
						</tr>		
					@endforeach
					</tbody>
				</table> 
			@endif
		</div>
		<div class="row" id="reporting-rate">
			@if (isset($reporting_rate))
			<div class="row">
				<div class="table-div">
					<table>
						<thead>
							<tr>
								<th>County</th>
								<th>Reporting Rate</th>
							</tr>
						</thead>
						<tbody>
							@foreach (array_slice($reporting_rate, 0, 16) as $county => $rate) 
								<tr>
									<td>{{ $county }}</td>
									<td>{{ $rate }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="table-div">
					<table>
						<thead>
							<tr>
								<th>County</th>
								<th>Reporting Rate</th>
							</tr>
						</thead>
						<tbody>
							@foreach (array_slice($reporting_rate, 16, -16) as $county => $rate) 
								<tr>
									<td>{{ $county }}</td>
									<td>{{ $rate }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="table-div">
					<table>
						<thead>
							<tr>
								<th>County</th>
								<th>Reporting Rate</th>
							</tr>
						</thead>
						<tbody>
							@foreach (array_slice($reporting_rate, -16) as $county => $rate) 
								<tr>
									<td>{{ $county }}</td>
									<td>{{ $rate }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			@endif
		</div>
	</body>
</html>
