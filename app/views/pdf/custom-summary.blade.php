<!DOCTYPE html>
<html>
	<head>
		<title>Custom SUmmary</title>
			<style type="text/css">
				h1, h2, h3, h4, h5, h6, body {
				   font-family: DejaVu Serif;
				   font-size: 12px;
				}
				.table-bordered th,
				.table-bordered td {
				  border: 1px solid #ddd !important;
				}

				.table thead > tr > th,
				.table tbody > tr > th,
				.table tfoot > tr > th,
				.table thead > tr > td,
				.table tbody > tr > td,
				.table tfoot > tr > td {
				  padding: 4px;
				  line-height: 1.428571429;
				  vertical-align: top;
				  border-top: 1px solid #dddddd;
				}

				.table thead > tr > th {
				  vertical-align: bottom;
				  border-bottom: 2px solid #dddddd;
				}

				.table tbody + tbody {
				  border-top: 2px solid #dddddd;
				}

				.row {
					text-align: center;
				}

				#data-table-div {
					width: 160%;
				}

				#main-title {
					margin-bottom: 10px
				}

				#rht-breakdown-table {
					padding-left: 50px;
				}

				#timestamp {
					text-align: right;
				}
			</style>
	</head>
	<body>
		@if (empty($png))
			{{ "there are no chart images to be rendered" }}
		@else
			@foreach ($png as $img_file)
				<div class="row"><img src={{ $img_file }} alt='...'></div>
			@endforeach
		@endif
	</body>
</html>