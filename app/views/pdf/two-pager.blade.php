<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- HTML::style('packages/bootstrap/css/bootstrap.min.css') --}}
	<style type="text/css">
		h1, h2, h3, h4, h5, h6, body {
		   font-family: DejaVu Serif;
		   font-size: 12px;
		}

		.table-bordered th,
		.table-bordered td {
		  border: 1px solid #ddd !important;
		}

		.table thead > tr > th,
		.table tbody > tr > th,
		.table tfoot > tr > th,
		.table thead > tr > td,
		.table tbody > tr > td,
		.table tfoot > tr > td {
		  padding: 4px;
		  line-height: 1.428571429;
		  vertical-align: top;
		  border-top: 1px solid #dddddd;
		}

		.table thead > tr > th {
		  vertical-align: bottom;
		  border-bottom: 2px solid #dddddd;
		}

		.table tbody + tbody {
		  border-top: 2px solid #dddddd;
		}

		.row {
			text-align: center;
		}

		#data-table-div {
			width: 100%;
		}

		#data-table-1, #data-table-2 {
			width: inherit;
		}

		#pending-consignment-title {
			margin-top: 30px;
			margin-bottom: 10px;
		}

		#main-title {
			margin-bottom: 10px
		}

		#timestamp {
			text-align: right;
		}

	</style>
</head>
<body>
	<div id="main-title">
		<p id="timestamp">{{ $time_stamp }}</p>
		<div class="row"><img src={{ public_path()."\images\coat-of-arms-logo.png"}} alt='...'></div>
		<div class="row">Ministry Of Health</div>
		<div class="row">Reproductive & Maternal Health Services Unit</div>
		<div class="row">Family Planning Commodities Stock Status Report As Of End of {{ $selectedPeriod }}</div>
	</div>
	
	<div id="data-table-div">
		<table class="table table-bordered" id="data-table-1">
			<thead>
				<tr>
					<th rowspan="2">COMMODITY</th>
					<th colspan="2">SOH</th>
					<th colspan="2">PENDING</th>
					<th colspan="2">RECEIVED</th>
				</tr>
				<tr>
					<th>KEMSA</th>
					<th>PSI</th>
					<th>KEMSA</th>
					<th>PSI</th>
					<th>KEMSA</th>
					<th>PSI</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($commodity_names as $commodity)
					<tr>
						<td>{{ $commodity }}</td>
						<td>{{ $kemsaData[$commodity]['SOH'] }}</td>
						<td>{{ $psiData[$commodity]['SOH'] }}</td>
						<td>{{ $kemsaData[$commodity]['PENDING'] }}</td>
						<td>{{ $psiData[$commodity]['PENDING'] }}</td>
						<td>{{ $kemsaData[$commodity]['RECEIVED'] }}</td>
						<td>{{ $psiData[$commodity]['RECEIVED'] }}</td>
					</tr>
				@endforeach	
			</tbody>
		</table>

		<div class="row" id="pending-consignment-title">Estimated Time Of Arrival Of Pending FP Consignments (Public Sector Pipeline)</div>
		<table class="table table-bordered" id="data-table-2">
			<thead>
				<tr>
					<th>Commodity</th>
					<th>Unit</th>
					<th>Source</th>
					<th>E.T.A</th>
					<th>Delayed Days</th>
					<th>Expected</th>
					<th>Received</th>
					<th>Remaining</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($pendingConsignments as $row)
					<tr>
						<td>{{ $row->commodity }}</td>
						<td>{{ $row->unit }}</td>
						<td>{{ $row->funding_source }}</td>
						<td>{{ $dateFormatter($row->ETA) }}</td>
						<td>{{ $row->no_of_days_delayed_by }}</td>
						<td>{{ number_format($row->qty_expected) }}</td>
						<td>{{ number_format($row->qty_received) }}</td>
						<td>{{ number_format($row->qty_remaining) }}</td>
						<td>{{ trim($row->status, 'KEMSA') }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</body>
</html>
