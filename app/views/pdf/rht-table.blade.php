<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style type="text/css">
			h1, h2, h3, h4, h5, h6, body {
			   font-family: DejaVu Serif;
			   font-size: 12px;
			}

			.table-bordered th,
			.table-bordered td {
			  border: 1px solid #ddd !important;
			}

			.table thead > tr > th,
			.table tbody > tr > th,
			.table tfoot > tr > th,
			.table thead > tr > td,
			.table tbody > tr > td,
			.table tfoot > tr > td {
			  padding: 4px;
			  line-height: 1.428571429;
			  vertical-align: top;
			  border-top: 1px solid #dddddd;
			}

			.table thead > tr > th {
			  vertical-align: bottom;
			  border-bottom: 2px solid #dddddd;
			}

			.table tbody + tbody {
			  border-top: 2px solid #dddddd;
			}

			.row {
				text-align: center;
			}

			#data-table-div {
				width: 160%;
			}

			#main-title {
				margin-bottom: 10px
			}

			#rht-breakdown-table {
				padding-left: 50px;
			}

			#timestamp {
				text-align: right;
			}
		</style>
	</head>
	<body>
		<div id="main-title">
			<p id="timestamp">{{ $time_stamp }}</p>
			<div class="row"><img src={{ public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."coat-of-arms-logo.png"}} alt='...'></div>
			<div class="row">Ministry Of Health</div>
			<div class="row">Reproductive & Maternal Health Services Unit</div>
			<div class="row">Reproductive Health Training - {{ $training}} County Summary</div>
		</div>
		<div id="data-table-div" >
			<table class="table" class="row" id="rht-breakdown-table">
				<thead>
					<tr>
					    <th>County</th>
					    <th>Facilities</th>
					    <th>Staff</th>
					    <th>Facilities with atleast 1 person trained</th>
					    <th>Staff trained</th>
					    <th>% Facilities with Trained Staff</th>
					    <th>% Staff trained</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($results as $county => $row)
					<tr>
						<td>{{ $county }}</td>
						<td>{{ $row['facilities in county'] }}</td>
						<td>{{ $row['personnel in county'] }}</td>
						<td>{{ $row['facilities with atleast one person trained'] }}</td>
						<td>{{ $row['trained personnel'] }}</td>
						<td>{{ ($row['% facilites with trained personnel'] == 0) ? 0.0 : round($row['% facilites with trained personnel'], 1) }}</td>
						<td>{{ ($row['% personnel trained'] == 0) ? 0.0 : round($row['% personnel trained'], 1) }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</body>
</html>