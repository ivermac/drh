<div class="">
  <h3>System Home</h3>
  <div class="row">
  		<div class="col-md-3">
        @if (in_array(Auth::user()->usertype, [1]))
        <!-- <button class="btn btn-success" id="save-chart-as-image">Save chart as image</button> -->
        {{-- 
            HTML::decode(
                HTML::link(
                    URL::route('custom-summary'), 
                    '<span class="glyphicon glyphicon-file"></span> PDF', 
                    array('class' => 'btn btn-warning')
                )
            ) 
        --}}
        <!-- <button class="btn btn-warning"><span class="glyphicon glyphicon-file"></span>PDF</button> -->
        @endif
            <div class="panel-group" id="accordion">
                @if (in_array(Auth::user()->usertype, [1]))
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpload">
                                Upload csv
                            </a>
                        </h4>
                    </div>
                    <div id="collapseUpload" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <span class="btn btn-primary fileinput-button btn-block">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input id="fileupload" type="file" name="files[]" multiple>
                            </span>
                            <br>
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <div id="files" class="files"></div>
                        </div>
                        <div class="panel-footer">
                            <a 
                                href="#service-statistics-delete-modal" 
                                data-toggle="modal" 
                                data-target="#service-statistics-delete-modal" 
                                class="btn btn-danger form-control">
                                Delete Data
                            </a>
                        </div>
                    </div>
                </div>
                @endif
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                National and county method mix
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
                                {{ Form::select(
                                            'year', 
                                            $years(), 
                                            '', 
                                            array('class'=>'form-control', 'id'=>'national-data-years')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::select('counties', array_merge(['all' => '--All counties--'], $counties), '', array('class'=>'form-control', 'id'=>'national-data-county')) }}
                            </div>
                            {{ Form::button('Generate Pie Chart', array('class'=>'btn btn-primary btn-block', 'id'=>'generate-pie-chart-national-data')) }}
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                Sub-county method mix
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body form-group">
                            <div class="form-group">
                                {{ Form::select('counties', $counties, '', array('class'=>'form-control county', 'id'=>'compare-by-counties')) }}
                            </div>
                            <div class="form-group">
                                 {{ Form::select(
                                            'year', 
                                            $years(), 
                                            '', 
                                            array('class'=>'form-control', 'id'=>'compare-by-county-years')) }}
                            </div>
                            <div class="form-group">
                                 {{ Form::select(
                                            'month', 
                                            $months(), 
                                            '', 
                                            array('class'=>'form-control', 'id'=>'compare-by-county-months')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::button('Generate Bar Graph', array('class'=>'btn btn-primary btn-block', 'id'=>'generate-bar-graph-by-county')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                Services by counties
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
                                {{ Form::select('comodities', $commodities, '', array('class'=>'form-control', 'id'=>'consumption-by-counties')) }}
                            </div>
                            <div class="form-group">
                                 {{ Form::select(
                                            'year', 
                                            $years(), 
                                            '', 
                                            array('class'=>'form-control', 'id'=>'consumption-by-county-years')) }}
                            </div>
                            <div class="form-group">
                                 {{ Form::select(
                                            'month', 
                                            $months(), 
                                            '', 
                                            array('class'=>'form-control', 'id'=>'consumption-by-county-months')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::button('Generate Bar Graph', array('class'=>'btn btn-primary btn-block', 'id'=>'generate-bar-graph-by-consumption')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                Forecast
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            {{ Form::select(
                                        'counties', 
                                        $counties, 
                                        '', 
                                        array('class'=>'form-control county', 'id'=>'forecast-by-county')
                                        ) }}
                        </div>
                    </div>
                </div> -->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                Consumption trends
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group">
                                {{ Form::select('counties', $counties, '', array('class'=>'form-control county', 'id'=>'consumption-trend-county')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::select('comodities', $commodities, '', array('class'=>'form-control', 'id'=>'consumption-trend-commodity')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::select(
                                            'year', 
                                            $years(), 
                                            '', 
                                            array('class'=>'form-control', 'id'=>'consumption-trend-year')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::button('Generate Line Graph', array('class'=>'btn btn-primary btn-block', 'id'=>'generate-line-graph-consumption-trends')) }}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
  			<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#upload-modal">upload csv</button> -->
  		</div>
  		<div class="col-md-9">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title graph-title"> <p></p></h4>
                </div>
                <div class="panel-body">
                    <div id="graph-section" style="">

                    </div>
                </div>
            </div>
            <div id="removals-section" style="height: 100px;">
                <div id="removals-column-graph"></div>
            </div>
        </div>
  </div>
</div>

<!-- Service Statistics delete modal -->
<div class="modal fade" id="service-statistics-delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Service Statistics Delete Modal</h4>
            </div>
            <div class="modal-body" >
                <div class="alert-message alert-message-warning alert-dismissable" id="delete-modal-alert">
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="year" class="col-sm-4 control-label">Year</label>
                        <div class="col-sm-8">
                            {{ Form::select(
                                'service-statistics-year', 
                                $years(), 
                                '', 
                                array('class'=>'form-control', 'id'=>'service-statistics-year')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="month" class="col-sm-4 control-label">Month</label>
                        <div class="col-sm-8">
                            {{ Form::select(
                                       'service-statistics-month', 
                                       array_except($months(), '00'), 
                                       '', 
                                       array('class'=>'form-control', 'id'=>'service-statistics-month')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="delete-service-statistics-period-data">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- Service Statistics delete modal -->

@include('centered-processing-modal')
