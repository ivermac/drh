@include('lmis.header')

<div class="row">
	<div class="col-md-3">
		<div class="panel-group" id="accordion">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Filter
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse-in">
                    <div class="panel-body">
                    	<div class="form-group">
                            {{ Form::select('counties', $counties, '', array('class'=>'form-control', 'id'=>'county')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::select('districts', $districts, '', array('class'=>'form-control', 'id'=>'district')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::select('years', $years, '', array('class'=>'form-control', 'id'=>'year')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::select('months', $months, '', array('class'=>'form-control', 'id'=>'month')) }}
                        </div>
                        <div class="form-group">
                        	<button class="btn btn-primary form-control" id="lmis-filter">Filter</button>
                        </div>
                    </div>
                     @if (in_array(Auth::user()->usertype, [1]))
                    <div class="panel-footer">
                        <a 
                            href="#LMIS-delete-modal" 
                            data-toggle="modal" 
                            data-target="#LMIS-delete-modal" 
                            class="btn btn-danger form-control">
                            Delete Data
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
	</div>
	<div class="col-md-9">
		<div class="panel panel-primary">
		    <div class="panel-heading">
		        <h4 class="panel-title graph-title">
		            Graph
		        </h4>
		    </div>
		    <div class="panel-body">
		        <div id="lmis-graph-section" style="">

		        </div>
		    </div>
		</div>
	</div>
</div>

<!-- LMIS upload modal -->
<div class="modal fade" id="LMIS-upload-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">LMIS CSV upload</h4>
            </div>
            <div class="modal-body" id="lmis-modal-body">
                <span class="btn btn-primary fileinput-button btn-block">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Select files...</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="LMIS-CSV-upload" type="file" name="files[]" multiple>
                </span>
                <br>
                <!-- The global progress bar -->
                <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success" ></div>
                </div>
                <div class="alert-message alert-message-danger alert-dismissable" id="non-csv">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    File(s) NOT uploaded (Correct file format is 'LMIS_YYYYMM.csv'):
                </div>
                <div id="files" class="files alert-message alert-message-success alert-dismissable">
                    File(s) successfully uploaded
                </div>
            </div>
        </div>
    </div>
</div>
<!-- LMIS upload modal -->

<!-- LMIS delete modal -->
<div class="modal fade" id="LMIS-delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Service Statistics Delete Modal</h4>
            </div>
            <div class="modal-body" >
                <div class="alert-message alert-message-warning alert-dismissable" id="delete-modal-alert">
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="year" class="col-sm-4 control-label">Year</label>
                        <div class="col-sm-8">
                            {{ Form::select(
                                'LMIS-year', 
                                $years, 
                                '', 
                                array('class'=>'form-control', 'id'=>'LMIS-year')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="month" class="col-sm-4 control-label">Month</label>
                        <div class="col-sm-8">
                            {{ Form::select(
                                       'LMIS-month', 
                                       array_except($months, '00'), 
                                       '', 
                                       array('class'=>'form-control', 'id'=>'LMIS-month')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="delete-LMIS-period-data">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- LMIS delete modal -->

@include('centered-processing-modal')
