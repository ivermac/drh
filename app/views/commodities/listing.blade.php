<h3>Commodities Listing</h3>
<div class="col-md-3 col-sm-offset-9"> 
	<button class="btn btn-primary btn-xs form-control" data-toggle="modal" data-target="#fpcommodities">Add new</button>
</div>

<table class="table">
	<thead>
		<tr>
			<th rowspan="2">Name</th>
			<th rowspan="2">Description</th>
			<th rowspan="2">Unit</th>
			<th colspan="2">Projected Monthly Consumption</th>
			<th rowspan="2">As of</th>
			<th rowspan="2">Action</th>
		</tr>
		<tr>
			<th>KEMSA</th>
			<th>PSI</th>
		</tr>
	</thead>
	<tbody>
		@foreach($fpcommodities as $row)
			<tr>
				<td>{{ $row->fp_name }}</td>
				<td>{{ $row->Description }}</td>
				<td>{{ $row->Unit }}</td>
				<td>{{ $row->projected_monthly_c }}</td>
				<td>{{ $row->projected_psi }}</td>
				<td>{{ $row->as_of }}</td>
				<td>
					<div class="btn-group">
					  <button type="button" class="btn btn-primary btn-xs dropdown-toggle form-control" data-toggle="dropdown">
					    Select <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" role="menu">
					    <li> {{ HTML::link(
					    		'#', 
					    		'Edit',
					    		array(
					    			'class' => 'edit-commodity',
					    			'commodity-id' => $row->id,
					    			'commodity-name' => $row->fp_name,
					    			'description' => $row->Description,
					    			'unit' => $row->Unit,
					    			'kemsa' => $row->projected_monthly_c,
					    			'psi' => $row->projected_psi,
					    			'as-of' => $row->as_of,
					    		)
					    ) }}</li>
					    <li>
					    	{{ HTML::link(
					    		'#', 
					    		'Delete', 
					    		array(
					    			'class' => 'delete-commodity',
					    			'delete-url' => URL::route('delete-commodity', array('id'=>$row->id))
					    		)
					    	) }} 
					    </li>
					  </ul>
					</div>
				</td>
			</tr>
		@endforeach 
	</tbody>
</table>
{{ $fpcommodities->links() }}

<!-- Funding Modal -->
<div class="modal fade" id="fpcommodities" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    {{ Form::open(array('route' => 'post-new-commodity', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form' )) }}
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Add New Commodity</h4>
      	</div>
      	<div class="modal-body" id="commodity-body-modal">
      		<div class="form-group">
      	    	<label for="name" class="col-sm-4 control-label">Name</label>
      	    	<div class="col-sm-8">
      	        	<input type="text" class="form-control" placeholder="commodity name comes here" name="commodity-name">
      	    	</div>
      		</div>
      		<div class="form-group">
      	    	<label for="name" class="col-sm-4 control-label">Description</label>
      	    	<div class="col-sm-8">
      	        	<input type="text" class="form-control" placeholder="description comes here" name="commodity-description">
      	    	</div>
      		</div>
      		<div class="form-group">
      	    	<label for="name" class="col-sm-4 control-label">Unit</label>
      	    	<div class="col-sm-8">
      	        	<input type="text" class="form-control" placeholder="unit comes here" name="commodity-unit">
      	    	</div>
      		</div>
      		<div class="form-group">
      	    	<label for="name" class="col-sm-4 control-label">Monthly consumption (KEMSA)</label>
      	    	<div class="col-sm-8">
      	        	<input type="text" class="form-control" placeholder="monthly consumption KEMSA comes here" name="monthly-consumption-kemsa">
      	    	</div>
      		</div>
      		<div class="form-group">
      	    	<label for="name" class="col-sm-4 control-label">Monthly consumption (PSI)</label>
      	    	<div class="col-sm-8">
      	        	<input type="text" class="form-control" placeholder="monthly consumption PSI comes here" name="monthly-consumption-psi">
      	    	</div>
      		</div>
      		<div class="form-group">
      	    	<label for="name" class="col-sm-4 control-label">As of</label>
      	    	<div class="col-sm-8">
      	    		<div class="input-group">
      	        		<input type="text" class="form-control" placeholder="date comes here" name="commodity-date-as-of" id="commodity-date-as-of" readonly="readonly">
      	        	</div>
      	    	</div>
      		</div>
      	</div>
      	<div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        	<button type="submit" class="btn btn-primary">Submit</button>
      	</div>
	{{ Form::close() }}
    </div>
  </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Delete Commodity</h4>
      	</div>
      	<div class="modal-body">
      		<p>Are you sure you want to delete this row?</p>
      	</div>
      	<div class="modal-footer">
        	<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
        	<a href="" class="btn btn-danger" id="delete-commodity-btn">Yes</a>
      	</div>
    </div>
  </div>
</div>