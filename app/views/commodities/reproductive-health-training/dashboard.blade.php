<h3>Reproductive Health Training</h3>

<ul class="nav nav-pills">
@if (in_array(Auth::user()->usertype, [1,2]))
    <li>{{ HTML::link(URL::route('reproductive-health-training-new'), 'New') }}</li>
@endif
    <li>{{ HTML::link(URL::route('reproductive-health-training-list'), 'List') }}</li>
    <li class="active">{{ HTML::link(URL::route('reproductive-health-training-dashboard'), 'Dashboard') }}</li>
    <li>{{ HTML::link("#", 'Trainings Definitions', array('data-toggle'=>'modal', 'data-target'=>'#myModal')) }}</li>
</ul>
<!-- Nav tabs -->
<ul class="nav nav-pills">
    <li class="active"><a href="#graph-data" data-toggle="tab">Graph Data</a></li>
    <li><a href="" id="county-summary-link">County Summary</a></li>
    <!-- <li><a href="#table-data" data-toggle="tab">Table Data</a></li> -->
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="graph-data">
        <div class="row">
            <div class="col-md-3">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    Filter
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse-in">
                            <div class="panel-body">
                                <div class="form-group">
                                     {{ Form::select('trainings', $trainings, '', array('id'=>'trainings', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                     {{ Form::select('owner', $owners, '', array('id'=>'owner', 'class'=>'form-control')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::select('former_provinces', $former_provinces, '', array('class'=>'form-control', 'id'=>'former-province')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::select('counties', ['--All Counties--'], '', array('class'=>'form-control', 'id'=>'county')) }}
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success form-control" id="btn-filter">Filter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="alert alert-warning">
                    <p>NOTE</p>
                    <p><span class="label label-info">Percentage of staff trained</span> and </p>
                    <p><span class="label label-info">Percentage of facilities with at ...</span></p>
                    <p>at the top of the graph are buttons used for filtering once you have made your selections on the filter panel above</p>
                </div>
            </div>
            <div class="col-md-9">
                <ul class="nav nav-pills">
                    <li class="active">
                        <a href="#graph-1" data-toggle="tab" data-tab="staff-trained">Percentage of staff trained</a>
                    </li>
                    <li>
                        <a href="#graph-2" data-toggle="tab" data-tab="facilities-with-atleast-one=person-trained">
                            Percentage of facilities with at least 1 person trained
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="graph-1">
                        <div class="panel panel-primary">
                            <div class="panel-heading">DrillDown Bar Graphs</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12" id="personnel-training-data"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="graph-2">
                        <div class="panel panel-primary">
                          <div class="panel-heading">DrillDown Bar Graphs</div>
                          <div class="panel-body">
                              <div class="row">
                                 <div class="col-md-12" id="atleast-one-person-trained-data"> </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="tab-pane" id="table-data">
</div>

<!-- on hold -->
<div class="row">
    <div class="col-md-4">
        <ul class="nav nav-pills nav-stacked" id="dashboard-list-summary">
          
        </ul>
    </div>
</div>

<!--Trainings Modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Trainings Definitions</h4>
      </div>
      <div class="modal-body">

        <dl class="dl-horizontal">
            <dt>LAPM</dt>
            <dd>Long acting and permanent methods</dd>

            <dt>CTU</dt>
            <dd>Contraceptive Technology Update</dd>

            <dt>EMONC</dt>
            <dd>Emergency Obstetrics and New born Care</dd>

            <dt>Compre. RH</dt>
            <dd>Comprehensive RH</dd>

            <dt>RHCM</dt>
            <dd>Reproductive health commodity management</dd>

            <dt>PRC</dt>
            <dd>Post rape care</dd>

            <dt>Youth</dt>
            <dd>Youth friendly</dd>

            <dt>SGBV</dt>
            <dd>Sexual and Gender Based Violence</dd>

            <dt>Neonatal</dt>
            <dd>Neonatal care</dd>

            <dt>FANC</dt>
            <dd>Focused antenatal care</dd>

            <dt>PAC</dt>
            <dd>Post Abortion Care</dd>

            <dt>LSS</dt>
            <dd>Life Saving Skills</dd>

            <dt>MPDR</dt>
            <dd>Maternal and Perinatal Death Review</dd>

            <dt>VIA/VILLI</dt>
            <dd>Cerival cancer screening(VIA/VILLI)</dd>

            <dt>Cryotherapy</dt>
            <dd>Cerival cancer screening(cryptotherapy)</dd>

            <dt>eMTCT</dt>
            <dd>Elimination of mother to child transmission of HIV</dd>

            <dt>PMTCT</dt>
            <dd>Prevention of mother to child transmission</dd>

            <dt>LSS-EOC</dt>
            <dd>Life saving skills and emergency obstetrics care</dd>

        </dl>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@include('centered-processing-modal')