<h3>Reproductive Health Training</h3>

<ul class="nav nav-pills">
@if (in_array(Auth::user()->usertype, [1,2]))
  <li class="active">{{ HTML::link(URL::route('reproductive-health-training-new'), 'New') }}</li>
@endif
  <li>{{ HTML::link(URL::route('reproductive-health-training-list'), 'List') }}</li>
  <li>{{ HTML::link(URL::route('reproductive-health-training-dashboard'), 'Dashboard') }}</li>
  <li>{{ HTML::link("#", 'Trainings Definitions', array('data-toggle'=>'modal', 'data-target'=>'#myModal')) }}</li>
</ul>

@if(Session::has('error-message'))
	<div class="alert-message alert-message-danger alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<p>{{ Session::get('error-message') }}</p>
	</div>
@endif

{{ Form::open(array('route' => 'add-reproductive-health-training', 'method' => 'post', 'class' => 'go-right')) }}
<div class="row">
	<div class="col-md-4">
		{{ Form::select(
				'county',
				$counties,
				'',
				array('class' => 'form-control  chosen-select-deselect', 
					'data-placeholder' => 'Select a county...', 
					'id'=>'county', 'tabindex'=>'2')
		) }}
	</div>
	<div class="col-md-4">
		{{ Form::select(
				'district',
				array(),
				'',
				array('class' => 'form-control  chosen-select-deselect', 
					'data-placeholder' => 'Select a sub-county...', 
					'id' => 'district')
		) }}
	</div>
	<div class="col-md-4">
		{{ Form::select(
				'facility-code',
				array(),
				'',
				array('class' => 'form-control  chosen-select-deselect', 
					'data-placeholder' => 'Select a facility...', 
					'id' => 'facility-code')
		) }}
	</div>
</div>
<div class="row" id="rht-input-collapsibles">
	<div class="col-md-12">
		<div class="panel-group rht-accordion" id="accordion">
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
		          Row
		        </a>
		        <div id="clone-btns">
		        	<a href="#" class="btn btn-primary pull-right collapsible-clone-btn btn-clone">+</a>
		        	<a href="#" class="btn btn-danger pull-right collapsible-clone-btn btn-unclone">-</a>
		        </div>
		      </h4>
		    </div>
		    <div id="collapseOne" class="panel-collapse collapse in">
		      <div class="panel-body">
		      	<div id="rht-inputs">
					<div class="col-md-4">
							{{ Form::text('first-name[1]', '', array('placeholder'=>'first name', 'class'=>'form-control training-inputs')) }}
							{{ Form::text('surname[1]', '', array('placeholder'=>'surname', 'class'=>'form-control training-inputs')) }}
							{{ Form::text('station[1]', '', array('placeholder'=>'station', 'class'=>'form-control training-inputs')) }}
							{{ Form::text('personnel-no[1]', '', array('placeholder'=>'personnel number', 'class'=>'form-control training-inputs')) }}
							{{ Form::text('qualification[1]', '', array('placeholder'=>'qualification', 'class'=>'form-control training-inputs')) }}
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-6">
								@foreach ($trainingsA as $id => $identifier)
									<div class="input-group">
								      <span class="input-group-addon">
										{{ Form::checkbox("training[$id][1]", $value = 1, $checked = null, $options = array('class'=>'rht-chkbox')).$identifier	}}<br>
								      </span>
								      	{{ Form::text("training-date[$id][1]", "",array("class"=>"form-control rht-date", "readonly"=>"readonly")) }}
								    </div>
								@endforeach
							</div>
							<div class="col-md-6">
								@foreach ($trainingsB as $id => $identifier)
									<div class="input-group">
								      <span class="input-group-addon">
										{{ Form::checkbox("training[$id][1]", $value = 1, $checked = null, $options = array('class'=>'rht-chkbox')).$identifier	}}<br>
								      </span>
								      {{ Form::text("training-date[$id][1]", "",array("class"=>"form-control rht-date", "readonly"=>"readonly")) }}
								    </div>
								@endforeach
							</div>
						</div>
					</div>
		      	</div>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>
{{ Form::submit('Submit form', array('class' => 'btn btn-primary', 'id' => 'rep-health')) }}
{{ Form::close() }}

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Trainings Definitions</h4>
      </div>
      <div class="modal-body">

        <dl class="dl-horizontal">
			<dt>LAPM</dt>
			<dd>Long acting and permanent methods</dd>

			<dt>CTU</dt>
			<dd>Contraceptive Technology Update</dd>

			<dt>EMONC</dt>
			<dd>Emergency Obstetrics and New born Care</dd>

			<dt>Compre. RH</dt>
			<dd>Comprehensive RH</dd>

			<dt>RHCM</dt>
			<dd>Reproductive health commodity management</dd>

			<dt>PRC</dt>
			<dd>Post rape care</dd>

			<dt>Youth</dt>
			<dd>Youth friendly</dd>

			<dt>SGBV</dt>
			<dd>Sexual and Gender Based Violence</dd>

			<dt>Neonatal</dt>
			<dd>Neonatal care</dd>

			<dt>FANC</dt>
			<dd>Focused antenatal care</dd>

			<dt>PAC</dt>
			<dd>Post Abortion Care</dd>

			<dt>LSS</dt>
			<dd>Life Saving Skills</dd>

			<dt>MPDR</dt>
			<dd>Maternal and Perinatal Death Review</dd>

			<dt>VIA/VILLI</dt>
			<dd>Cerival cancer screening(VIA/VILLI)</dd>

			<dt>Cryotherapy</dt>
			<dd>Cerival cancer screening(cryptotherapy)</dd>

			<dt>eMTCT</dt>
			<dd>Elimination of mother to child transmission of HIV</dd>

			<dt>PMTCT</dt>
			<dd>Prevention of mother to child transmission</dd>

			<dt>LSS-EOC</dt>
			<dd>Life saving skills and emergency obstetrics care</dd>

        </dl>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>