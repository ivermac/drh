<h3>Reproductive Health Training</h3>

<ul class="nav nav-pills">
  <li class="active">{{ HTML::link('#', 'Edit') }}</li>
  <li>{{ HTML::link(URL::route('reproductive-health-training-list'), 'List') }}</li>
  <li>{{ HTML::link(URL::route('reproductive-health-training-dashboard'), 'Dashboard') }}</li>
  <li>{{ HTML::link("#", 'Trainings Definitions', array('data-toggle'=>'modal', 'data-target'=>'#myModal')) }}</li>
</ul>

@if(Session::has('error-message'))
	<div class="alert-message alert-message-danger alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<p>{{ Session::get('error-message') }}</p>
	</div>
@endif

{{ Form::open(array('route' => 'edit-reproductive-health-training', 'method' => 'post')) }}
<div class="row">
	<div class="col-md-4">
		{{ Form::hidden('id', $rhtraining->id) }}
		{{ Form::select(
				'county',
				$counties,
				'',
				array('class' => 'form-control training-inputs chosen-select', 
					'data-placeholder' => 'Select a county...', 
					'id'=>'county', 'tabindex'=>'2')
		) }}

		{{ Form::select(
				'district',
				array(),
				'',
				array('class' => 'form-control training-inputs chosen-select', 
					'data-placeholder' => 'Select a district...', 
					'id' => 'district')
		) }}
		
		{{ Form::select(
				'facility-code',
				array($rhtraining->facility_code => $facility_name),
				'',
				array('class' => 'form-control training-inputs chosen-select', 
					'data-placeholder' => $rhtraining->facility_code, 
					'id' => 'facility-code')
		) }}

		{{ Form::text('first-name', $rhtraining->first_name, array('placeholder'=>'first name', 'class'=>'form-control training-inputs')) }}
		{{ Form::text('surname', $rhtraining->surname, array('placeholder'=>'surname', 'class'=>'form-control training-inputs')) }}
		{{ Form::text('station', $rhtraining->station, array('placeholder'=>'station', 'class'=>'form-control training-inputs')) }}
		{{ Form::text('personnel-no', $rhtraining->personnel_no, array('placeholder'=>'personnel number', 'class'=>'form-control training-inputs')) }}
		{{ Form::text('qualification', $rhtraining->qualification, array('placeholder'=>'qualification', 'class'=>'form-control training-inputs')) }}
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-6">
				@foreach ($trainingsA as $id => $identifier)
					<div class="input-group">
				      <span class="input-group-addon">
						{{ Form::checkbox(
										"training[$id]", 
										$value = 1, 
										$checked = (in_array($identifier, array_keys($checked_trainings))) ? true : null, 
										$options = array()).$identifier	}}<br>
				      </span>
				      {{ 
				      	Form::text(
				      		"training-date[$id]", 
				      		(in_array($identifier, array_keys($checked_trainings)) && $checked_trainings[$identifier] != 'Y') ? $checked_trainings[$identifier] : "",
				      		array("class"=>"form-control rht-date", "readonly"=>"readonly")) }}
				    </div>
				@endforeach
			</div>
			<div class="col-md-6">
				@foreach ($trainingsB as $id => $identifier)
					<div class="input-group">
				      <span class="input-group-addon">
						{{ Form::checkbox(
										"training[$id]", 
										$value = 1, 
										$checked = (in_array($identifier, array_keys($checked_trainings))) ? true : null, 
										$options = array()).$identifier	}}<br>
				      </span>
				      {{ Form::text(
				      		"training-date[$id]", 
				      		(in_array($identifier, array_keys($checked_trainings)) && $checked_trainings[$identifier] != 'Y') ? $checked_trainings[$identifier] : "",
				      		array("class"=>"form-control rht-date", "readonly"=>"readonly")) }}
				    </div>
				@endforeach
			</div>
		</div>
	</div>
</div>
{{ Form::submit('Submit form', array('class' => 'btn btn-primary', 'id' => 'rep-health')) }}
{{ Form::close() }}

<!--Trainings Modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Trainings</h4>
      </div>
      <div class="modal-body">

        <dl class="dl-horizontal">
			<dt>LAPM</dt>
			<dd>LAPM</dd>

			<dt>CTU</dt>
			<dd>CTU</dd>

			<dt>EMONC</dt>
			<dd>EMONC</dd>

			<dt>Compre. RH</dt>
			<dd>Comprehensive RH</dd>

			<dt>RHCM</dt>
			<dd>RHCM</dd>

			<dt>PRC</dt>
			<dd>PRC</dd>

			<dt>Youth</dt>
			<dd>Youth friendly</dd>

			<dt>SGBV</dt>
			<dd>SGBV</dd>

			<dt>Neonatal</dt>
			<dd>Neonatal care</dd>

			<dt>FANC</dt>
			<dd>FANC</dd>

			<dt>PAC</dt>
			<dd>PAC</dd>

			<dt>LSS</dt>
			<dd>LSS</dd>

			<dt>MPDR</dt>
			<dd>MPDR</dd>

			<dt>VIA/VILLI</dt>
			<dd>Cerival cancer screening(VIA/VILLI)</dd>

			<dt>Cryotherapy</dt>
			<dd>Cerival cancer screening(cryptotherapyI)</dd>

			<dt>eMTCT</dt>
			<dd>Elimination of mother to child transmission of HIV</dd>

			<dt>PMTCT</dt>
			<dd>Prevention of mother to child transmission</dd>

			<dt>LSS-EOC</dt>
			<dd>Life saving skills and emergency obstetrics care</dd>

        </dl>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>