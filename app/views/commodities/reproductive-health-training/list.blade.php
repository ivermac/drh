<h3>Reproductive Health Training</h3>

<ul class="nav nav-pills">
@if (in_array(Auth::user()->usertype, [1,2]))
  <li>{{ HTML::link(URL::route('reproductive-health-training-new'), 'New') }}</li>
@endif
  <li class="active">{{ HTML::link(URL::route('reproductive-health-training-list'), 'List') }}</li>
  <li>{{ HTML::link(URL::route('reproductive-health-training-dashboard'), 'Dashboard') }}</li>
</ul>

@if(Session::has('success-message'))
	<div class="alert-message alert-message-success alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<p>{{ Session::get('success-message') }}</p>
	</div>
@endif

<div class="row" id="search-row">
	<div class="col-md-3">
	    <input class="form-control" id="system-search" name="q" placeholder="Table Search (on type)" required>
	</div>
	<div class="col-md-3">
	    <button class=" form-control btn btn-primary" data-toggle="modal" data-target="#modal-search">
	      Launch search modal
	    </button>
	</div>
	<div class="col-md-3">
		{{ HTML::link(URL::route('download-rht-csv'), 'Downlaod CSV', array("class"=>" form-control btn btn-success", "id"=>"download-csv", "name"=>"download-csv")) }}
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		@if (count($results)<=0)
			<div class="alert alert-warning no-result"> Unfortunately, there are no results :-( </div>
		@else
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Surname</th>
					@if (in_array(Auth::user()->usertype, [1,2]))
					<th>Personnel No</th>
					@endif
					<th>Qualification</th>
					<th>Station</th>
					<th>Facility</th>
					<th>Sub-county</th>
					<th>County</th>
					@if (in_array(Auth::user()->usertype, [1,2]))
					<th>Action</th>
					@endif
				</tr>
			</thead>
			<tbody>
				@foreach ($results as $result)
				<tr>
					<td>{{ $result->first_name }}</td>
					<td>{{ $result->surname }}</td>
					@if (in_array(Auth::user()->usertype, [1,2]))
					<td>{{ $result->personnel_no }}</td>
					@endif
					<td>{{ $result->qualification }}</td>
					<td>{{ $result->station }}</td>
					<td>{{ $result->facility }}</td>
					<td>{{ $result->district }}</td>
					<td>{{ $result->county }}</td>
					@if (in_array(Auth::user()->usertype, [1,2]))
					<td>
						<div class="btn-group">
						  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
						    Select <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						    <li>
						    	{{ HTML::link(
						    		'#', 
						    		'Details', 
						    		array(
						    			'record-id'=>$result->id, 
						    			'data-toggle'=>'modal', 
						    			'data-target'=>'#modal-details', 
						    			'class'=>'rht-detail')
						    	) }}
						    </li>
						    <li>{{ HTML::link(URL::route('reproductive-health-training-edit', array('id'=>$result->id)), 'Edit') }}</li>
						    <li>
						    	{{ HTML::link(
						    		'#', 
						    		'Delete', 
						    		array(
						    			'record-id'=>$result->id,
						    			'data-toggle'=>'modal', 
						    			'data-target'=>'#modal-delete', 
						    			'class'=>'rht-delete')
						    	) }}
						    </li>
						  </ul>
						</div>
					</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
		{{ $results->appends($search_inputs)->links() }}
		@endif
	</div>
</div>

<!-- Details Modal -->
<div class="modal fade" id="modal-details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Record Detail</h4>
      	</div>
      	<div class="modal-body">
			<dl class="dl-horizontal">
	 			<dt>{{ Form::label('first-name', 'First Name') }}</dt>
	  			<dd id="modal-first-name">...</dd>
	
	  			<dt>{{ Form::label('surname', 'Surname') }}</dt>
	  			<dd id="modal-surname">...</dd>

	  			<dt>{{ Form::label('facility', 'Facility') }}</dt>
	  			<dd id="modal-facility">...</dd>

	  			<dt>{{ Form::label('personnel-no', 'Personnel No') }}</dt>
	  			<dd id="modal-personnel-no">...</dd>

	  			<dt>{{ Form::label('qualification', 'Qualification') }}</dt>
	  			<dd id="modal-qualification">...</dd>

	  			<dt>{{ Form::label('station', 'Station') }}</dt>
	  			<dd id="modal-station">...</dd>

	  			<dt>{{ Form::label('training', 'Training') }}</dt>
	  			<dd id="modal-training">...</dd>		  		  
			</dl>
      </div>
    </div>
  </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Confirm Record Deletion</h4>
      	</div>
      	<div class="modal-body">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			{{ HTML::link("#", 'Delete', array('class' => 'delete-rht-record btn btn-danger')) }}	
      	</div>
    </div>
  </div>
</div>

<!-- Search Modal -->
<div class="modal fade" id="modal-search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Search</h4>
      	</div>
  		{{Form::open(array('route' => 'reproductive-health-training-list', 'method' => 'get', 'class'=>'')) }}
  			{{ Form::hidden('search', 'search') }}
      	<div class="modal-body">
      		<div class="row">
      			<div class="col-md-6">
      				{{ Form::select(
      						'county',
      						$counties,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a county...', 
      							'id'=>'county', 'tabindex'=>'2')
      				) }}

      				{{ Form::select(
      						'district',
      						array(),
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a sub-county...', 
      							'id' => 'district')
      				) }}
      				
      				{{ Form::select(
      						'facility-code',
      						array(),
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a facility...', 
      							'id' => 'facility-code')
      				) }}

      			</div>
      			<div class="col-md-6">
      				{{ Form::select(
      						'commodity',
      						$trainings,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a training...', 
      							'id'=>'county', 'tabindex'=>'2')
      				) }}

      				{{ Form::select(
      						'facility-owners',
      						$facility_owners,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a facility owner...', 
      							'id'=>'facility-owners', 'tabindex'=>'2')
      				) }}

      				{{ Form::select(
      						'facility-types',
      						$facility_types,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a facility type...', 
      							'id'=>'facility-types', 'tabindex'=>'2')
      				) }}

      				
      			</div>
      	</div>
      	<div class="modal-footer">
      		{{ Form::submit('search', array('class'=>'btn btn-success btn-small')) }}
      	</div>
  		{{ Form::close() }}
    </div>
  </div>
</div>
