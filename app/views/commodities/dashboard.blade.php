<h3>Family Planning Stock Status</h3>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#stock-status" data-toggle="tab">Stock Status</a></li>
    <li><a href="#supply-chain-mos" data-toggle="tab">Supply Plan vs MoS</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="stock-status">
  	    <div class="row">
  		    <div class="col-md-6" >
                <div class="row" id="public-sector-filter">
                    <div class="col-md-4 input-group ">
                        {{ Form::text('public-sector-date-filter','', array('placeholder' => 'status as of', 'class'=>'form-control stock-status-date-filter', 'id'=>'public-sector-date-filter', 'readonly' => 'readonly') ) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::button('Filter', array('class'=>'btn btn-primary form-control stock-status-filter', 'id'=>'btn-public-sector-filter', 'date-filter' => 'public-sector-date-filter', 'store' => 'kemsa', 'container'=>'public-sector'))}}
                    </div>
                </div>
                <div class="row" id="public-sector">
                    graph 1
                </div>
            </div>
            <div class="col-md-6">
                <div class="row" id="private-sector-filter">
                    <div class="col-md-4 input-group">
                        {{ Form::text('private-sector-date-filter','', array('placeholder' => 'status as of', 'class'=>'form-control stock-status-date-filter', 'id'=>'private-sector-date-filter', 'readonly' => 'readonly') ) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::button('Filter', array('class'=>'btn btn-primary form-control stock-status-filter', 'id'=>'btn-private-sector-filter', 'date-filter' => 'private-sector-date-filter', 'store' => 'psi', 'container'=>'private-sector'))}}
                    </div>
                </div>
                <div class="row" id="private-sector">
                    graph 2
                </div>
            </div>
  	     </div>
    </div>
    <div class="tab-pane" id="supply-chain-mos">
        <div class="row" id="row-filter">
            <div class="col-md-3">
                {{ Form::select(
                    'fpc',
                    $fpc,
                    '',
                    array('class' => 'form-control  chosen-select-deselect', 
                      'data-placeholder' => 'Select a commodity...', 
                      'id'=>'fpc', 'tabindex'=>'2')
                ) }}
            </div>
            <div class="col-md-3">
                {{ Form::select(
                    'fiscal_years',
                    $fiscal_years,
                    '',
                    array('class' => 'form-control  chosen-select-deselect', 
                      'data-placeholder' => 'Select a fiscal year...', 
                      'id'=>'fiscal_years', 'tabindex'=>'2')
                ) }}
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary form-control" id="filter-supply-plan-vs-mos">Filter</button>
            </div>
        </div>
  	    <div class="row">
  		    <div class="col-md-12" id="supply-plan-vs-mos">
  			   graph 3
  		    </div>
  	    </div>
    </div>
</div>

@include('centered-processing-modal')