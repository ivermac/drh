<h3>Supply Plan Listing</h3>

@if(Session::has('success-message'))
	<div class="alert-message alert-message-success alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<p>{{ Session::get('success-message') }}</p>
	</div>
@endif
<div class="row">
	<div class="col-md-3">
		<button class=" form-control btn btn-primary" data-toggle="modal" data-target="#modal-search">
	      Launch search modal
	    </button>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		@if (count($supply_plan)<=0)
			<div class="alert alert-warning no-result"> Unfortunately, there are no results :-( </div>
		@else
		<table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<th>FP Commodity</th>
					<th>Unit</th>
					<th>Funding Source</th>
					<th>E.T.A</th>
					<th>Quantity</th>
					<th>Store</th>
					<th>Status</th>
					@if (in_array(Auth::user()->usertype, [1,2]))
					<th>Action</th>
					@endif
		  		</tr>
		  	</thead>
		  	<tbody>
		  		@foreach ($supply_plan as $row)
		  		<tr>
		  			<td>{{ $row->commodity_name }}</td>
		  			<td>{{ $row->commodity_unit }}</td>
		  			<td>{{ $row->funding_source }}</td>
		  			<td>{{ ($row->delayed_to == "0000-00-00") ? $row->ETA : $row->delayed_to }}</td>
		  			<td>{{ number_format($row->commodity_quantity) }}</td>
		  			<td>{{ $row->store_name }}</td>
					<td>{{ $replaceKemsaOrPsi($row->status) }}</td>
					@if (in_array(Auth::user()->usertype, [1,2]))
		  			<td>
		  				<div class="btn-group">
						  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
						    Select <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						    <li>{{ HTML::link(URL::route('edit-pipeline', array('id'=>$row->id)), 'Edit') }}</li>
						    <li>
						    	{{ HTML::link(
						    		'#', 
						    		'Delete', 
						    		array(
						    			'record-id'=>$row->id,
						    			'data-toggle'=>'modal', 
						    			'data-target'=>'#modal-delete', 
						    			'class'=>'supply-plan-delete')
						    	) }} 
						    </li>
						  </ul>
						</div>
		  			</td>
					@endif
		  		</tr>
		  		@endforeach
		  	</tbody>
		</table>
		{{ $supply_plan->appends($inputs)->links() }}
		@endif
	</div>
</div>

<!-- Search Modal -->
<div class="modal fade" id="modal-search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Search</h4>
      	</div>
  		{{Form::open(array('route' => 'supply-plan-listing', 'method' => 'post', 'class'=>'')) }}
  			{{ Form::hidden('search', 'search') }}
      	<div class="modal-body">
			<div class="row">
				<div class="col-md-6">
					{{ Form::select(
      						'commodity',
      						$commodities,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a commodity...', 
      							'id'=>'commodity', 'tabindex'=>'2')
      				) }}

      				{{ Form::select(
      						'funding_source',
      						$funding_sources,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a funding source...', 
      							'id'=>'funding-source', 'tabindex'=>'2')
      				) }}

      				{{ Form::select(
      						'store',
      						$stores,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a store...', 
      							'id'=>'store', 'tabindex'=>'2')
      				) }}
				</div>
				<div class="col-md-6">
					{{ Form::select(
      						'status',
      						$statuses,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a status...', 
      							'id'=>'status', 'tabindex'=>'2')
      				) }}

      				{{ Form::select(
      						'date-category',
      						$date_categories,
      						'',
      						array('class' => 'form-control training-inputs chosen-select-deselect', 
      							'data-placeholder' => 'Select a date category...', 
      							'id'=>'date-category', 'tabindex'=>'2')
      				) }}

      				<div class="input-group" id="supply-plan-listing-date-picker-div">
	      				{{ Form::text(
	      						'supply-plan-listing-date-picker', 
	      						'', 
	      						array(
	      							'placeholder'=>'select date', 
	      							'id' => 'supply-plan-listing-date-picker',
	      							'readonly' => 'readonly' ,
	      							'class'=>'form-control training-inputs')
	      				) }}
      				</div>
				</div>
			</div>      				
      	</div>
      	<div class="modal-footer">
      		{{ Form::submit('search', array('class'=>'btn btn-success btn-small')) }}
      	</div>
  		{{ Form::close() }}
    </div>
  </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Are you sure you wan't to delete this record?</h4>
      	</div>
      	<div class="modal-body">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			{{ HTML::link("#", 'Delete', array('class' => 'delete-supply-plan-record btn btn-danger')) }}	
      	</div>
    </div>
  </div>
</div>