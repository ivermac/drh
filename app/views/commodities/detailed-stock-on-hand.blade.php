<h3>Detailed Stock on Hand</h3>
<div class="row">
	<div class="col-md-12">
	{{Form::open(array('route' => 'detailed-stock-on-hand', 'method' => 'get', 'class'=>'form-inline', 'role' => 'form')) }}
  		{{ Form::hidden('search', 'search') }}
		  <div class="form-group">
		    {{ 
		    	Form::select(
			    	'month', 
			    	array(
			    		'01' => 'January', 
			    		'02' => 'February', 
			    		'03'=>'March', 
			    		'04' => 'April',
			    		'05' => 'May',
			    		'06' => 'June',
			    		'07' => 'July',
			    		'08' => 'August',
			    		'09' => 'September',
			    		'10' => 'October',
			    		'11' => 'November',
			    		'12' => 'December'
			    	), 
			    	'', 
			    	array('class'=>'form-control')
			    ) 
		    }}
		  </div>
		  <div class="form-group">
		    {{ Form::select(
		    	'year', 
		    	array(
		    		'2014'=>'2014', 
		    		'2013'=>'2013', 
		    		'2012'=>'2012', 
		    		'2011'=>'2011'
		    	), 
		    	'', 
		    	array('class'=>'form-control')) }}
		  </div>
		  <div class="form-group">
		    {{ Form::select('store', $stores, '', array('id'=>'store', 'class'=>'form-control')) }}
		  </div>
		  
		  <button type="submit" class="btn btn-primary">Filter</button>
		{{ Form::close() }}
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success no-result">
			Stock on Hand for <span class="label label-success">{{ $labelDisplayables["store"] }}</span> 
			as of <span class="label label-success">{{ $labelDisplayables["month"].", ".$labelDisplayables["year"] }}</span>
		</div>	
		@if (count($results) > 0)
		<table class="table table-striped" id="commodities-detailed-soh-table">
		  	<thead>
		  		<tr>
		  			<th>FP Commodity</th>
		  			<th>Unit</th>
		  			<th>Actual SOH</th>
		  			<th>SOH in M.O.S</th>
		  			<th>PMS</th>
		  			<th>Date as of</th>
		  			<th>Action</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		@foreach ($results as $row)
		  		<tr>
		  			<td>{{ $row->fp_name }}</td>
		  			<td>{{ $row->Unit }}</td>
		  			<td>{{ number_format($row->fp_quantity) }}</td>
		  			<td>{{ $row->soh_mos }}</td>
		  			<td>{{ number_format($row->projected_consumption) }}</td>
		  			<td>{{ $row->fp_date }}</td>
		  			<td>
		  				<div class="btn-group">
						  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
						    Select <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						    <li>
						    	{{ HTML::link(
						    		'#', 
						    		'Edit', 
						    		array(
						    			'record-id'=>$row->id,
						    			'data-toggle'=>'modal', 
						    			'data-target'=>'#modal-edit', 
						    			'class'=>'stock-on-hand-edit')
						    	) }}
						    </li>
						    <li>
						    	{{ HTML::link(
						    		'#', 
						    		'Delete', 
						    		array(
						    			'record-id'=>$row->id,
						    			'data-toggle'=>'modal', 
						    			'data-target'=>'#modal-delete', 
						    			'class'=>'stock-on-hand-delete')
						    	) }}
						    </li>
						  </ul>
						</div>
		  			</td>
		  		</tr>
		  		@endforeach
		  	</tbody>
		</table>
		{{ $results->appends($search_inputs)->links() }}
		{{-- $results->links() --}}
		@else
			<div class="alert alert-warning no-result">Data not available :-(</div>
		@endif
	</div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Are you sure you wan't to delete this record?</h4>
      	</div>
      	<div class="modal-body">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			{{ HTML::link("#", 'Delete', array('class' => 'delete-stock-on-hand-record btn btn-danger')) }}	
      	</div>
    </div>
  </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
  	{{ Form::open(array('route' => 'post-stock-on-hand', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form' )) }}
  		{{ Form::hidden('id', ''); }}
    	<div class="modal-content">
    		<div class="modal-header">
	    	  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	  	<h4 class="modal-title" id="myModalLabel">Edit Stock on Hand</h4>
    		</div>
	      	<div class="modal-body">
      			<div class="form-group">
      		    	<label for="commodity" class="col-sm-4 control-label">Commodity</label>
      		    	<div class="col-sm-8">
      		        	{{ Form::select('commodity', $commodities, '', array('id' => 'commodity', 'class'=>'form-control')) }}
      		    	</div>
      			</div>
      			<div class="form-group">
      		    	<label for="store" class="col-sm-4 control-label">Store</label>
      		    	<div class="col-sm-8">
      		        	{{ Form::select('store', $stores, '', array('id'=>'store', 'class'=>'form-control')) }}
      		    	</div>
      			</div>
      			<div class="form-group">
      		    	<label for="quantity" class="col-sm-4 control-label">Quantity</label>
      		    	<div class="col-sm-8">
      		        	{{ Form::text('quantity', '', array('id'=>'quantity', 'placeholder'=>'quantity', 'class'=>'form-control')) }}
      		    	</div>
      			</div>
      			<div class="form-group">
      		    	<label for="date-as-of" class="col-sm-4 control-label">Date as Of</label>
      		    	<div class="col-sm-8">
      		    		<div class="input-group">
      		        		{{ Form::text('date-as-of', '', array('id' => 'date-as-of', 'placeholder'=>'date as of', 'class'=>'form-control', 'readonly' => 'readonly')) }}
      		    		</div>
      		    	</div>
      			</div>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        	<button type="submit" class="btn btn-primary">Submit</button>
	      	</div>
	    </div>
  	{{ Form::close() }}
  </div>
</div>