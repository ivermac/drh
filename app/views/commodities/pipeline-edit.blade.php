<h3>Edit Supply Plan</h3>

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Receive or Delay a Consignment by editing Details below</div>
			<div class="panel-body">
				{{ Form::open(array('route' => 'post-edit-pipeline', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form' )) }}
					{{ Form::hidden('id', $result->id) }}
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="inputFPCommodity" class="col-sm-4 control-label">FP Commodity</label>
					    <div class="col-sm-8">
							{{ Form::select('fp-commodity', $commodities, $result->fpcommodity_Id, array('class'=>'form-control training-inputs')) }}
					    </div>
					  </div>

					  <div class="form-group">
					    <label for="inputStore" class="col-sm-4 control-label">Store</label>
					    <div class="col-sm-8">
							{{ Form::select('store', $stores, $result->store_id, array('class'=>'form-control training-inputs')) }}
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label for="inputFundingSource" class="col-sm-4 control-label">Funding Source</label>
					    <div class="col-sm-8">
							{{ Form::select('funding-source', $funding_source, $result->funding_source, array('class'=>'form-control training-inputs')) }}
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label for="inputQuantityExpected" class="col-sm-4 control-label">Quantity Expected</label>
					    <div class="col-sm-8">
							{{ Form::text('quantity-expected', $result->fp_quantity, array('placeholder'=>'Quantity Expected', 'class'=>'form-control training-inputs')) }}
					    </div>
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
					    <label for="inputDateExpected" class="col-sm-4 control-label">Date Expected</label>
					    <div class="col-sm-8">
					    	<div class="input-group">
							{{ Form::text('date-expected',  $result->fp_date, array('placeholder'=>'Date Expected', 'class'=>'form-control training-inputs', 'id' => 'date-expected', 'readonly' => 'readonly')) }}
							</div>
					    </div>
					  </div>

					  <div class="form-group">
					    <label for="inputStatus" class="col-sm-4 control-label">Status</label>
					    <div class="col-sm-8">
							{{ Form::select('status', $status,  $getStatus($result->transaction_type), array('class'=>'form-control training-inputs', 'id' => 'status')) }}
					    </div>
					  </div>

					  
					  
					</div>

					 <!-- modal form -->
					 <div class="modal fade" id="custom-inputs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					   <div class="modal-dialog">
					     <div class="modal-content">
					       <div class="modal-header">
					         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					         <h4 class="modal-title" id="myModalLabel">Custom Input</h4>
					       </div>
					       <div class="modal-body">
	     					    @include('commodities/pipeline-edit-custom-input')
					       </div>
					     </div>
					   </div>
					 </div>
					 <!--  -->

				  <div class="form-group">
				    <div class="col-sm-offset-4 col-sm-4">
				      <button type="submit" class="btn btn-primary form-control">Submit Change</button>
				    </div>
				  </div>

				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>




