<h3>Add Stock on hand</h3>
<div class="row">
	{{ Form::open(array('route' => 'post-stock-on-hand', 'method' => 'post', 'class' => 'go-right')) }}
	<div class="col-md-12">
		<table class="table table-striped" id="add-soh-table">
		  	<thead>
		  		<tr>
		  			<th>FP Commodity</th>
		  			<th>Store</th>
		  			<th>Date as of	</th>
		  			<th>Quantity</th>
		  			<th></th>
		  			<th></th>
		  		</tr>
		  	</thead>
		  	<tbody id="add-soh-table-body">
		  		<tr id="add-soh-row">
		  			<td><div class="input-group">{{ Form::select('commodity[]', $commodities, '', array('id'=>'commodity', 'class'=>'form-control add-soh-inputs')) }}</div></td>
		  			<td><div class="input-group">{{ Form::select('store[]', $stores, '', array('id'=>'store', 'class'=>'form-control add-soh-inputs')) }}</div></td>
		  			<td>
			  			<div class="input-group">
			  				{{ Form::text('date-as-of[]', '', array('placeholder'=>'Date as of', 'class'=>'form-control date-as-of')) }}</td>
			  			</div>
		  			<td><div class="input-group">{{ Form::text('quantity[]', '', array('placeholder'=>'Quantity', 'class'=>'form-control add-soh-inputs')) }}</div></td>
		  			<td>{{ Form::button('+', array('class'=>'btn btn-primary add-row')) }}</td>
		  			<td>{{ Form::button('-', array('class'=>'btn btn-danger remove-row')) }}</td>
		  		</tr>
		  	</tbody>
		</table>
		{{ Form::submit('Submit', array('class'=>'btn btn-primary')) }}
	</div>
	{{ Form::close() }}
</div>
