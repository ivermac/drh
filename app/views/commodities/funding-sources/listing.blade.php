<h3>Funding Sources Listing</h3>
<div class="col-md-3 col-sm-offset-9"> 
	<button class="btn btn-primary btn-xs form-control" data-toggle="modal" data-target="#funding-source">Add new</button>
</div>

<table class="table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Procuring Agency</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($funding_sources as $row)
			<tr>
				<td>{{ $row->funding_source }}</td>
				<td>{{ ($row->procuring_a == 1) ? "YES" : "NO" }}</td>
				<td>{{ ($row->service_active == 1) ? "ACTIVE" : "NOT ACTIVE" }}</td>
				<td>
					<div class="btn-group">
					  <button type="button" class="btn btn-primary btn-xs dropdown-toggle form-control" data-toggle="dropdown">
					    Select <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" role="menu">
					    <li> {{ HTML::link(
					    		'#', 
					    		'Edit', 
					    		array(
					    			'class'=> 'edit-funding-source', 
					    			"funding-source-id" => $row->id,
					    			"funding-source-name" => $row->funding_source,
					    			"procuring-agency" => $row->procuring_a,
					    			"service-status" => $row->service_active,
					    		)
					    	) }}
                </li>
					    <li>{{ HTML::link(
					    		'#', 
					    		'Delete', 
					    		array(
					    			'class' => 'delete-commodity',
					    			'delete-url' => URL::route('delete-commodity', array('id'=>$row->id))
					    		)
					    	) }} </li>
					  </ul>
					</div>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
{{ $funding_sources->links() }}
<!-- Funding Modal -->
<div class="modal fade" id="funding-source" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    {{ Form::open(array('route' => 'post-funding-sources', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form' )) }}
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Add New Funding Source</h4>
      	</div>
      	<div class="modal-body" id="funding-source-modal-body">
      		<div class="form-group">
      	    	<label for="name" class="col-sm-4 control-label">Funding Source</label>
      	    	<div class="col-sm-8">
      	        	<input type="text" class="form-control" placeholder="funding source comes here" name="funding-source">
      	    	</div>
      		</div>
      		<div class="form-group">
      	    	<label for="inputPassword3" class="col-sm-4 control-label">Procuring Agency</label>
      	    	<div class="col-sm-8">
      	        	<select name="procuring-agency" class="form-control">
      	        		<option value="1">YES</option>
      	        		<option value="2">NO</option>
      	        	</select>
      	    	</div>
      		</div>
      		<div class="form-group">
      	    	<label for="inputPassword3" class="col-sm-4 control-label">Service Status</label>
      	    	<div class="col-sm-8">
      	        	<select name="service-status" class="form-control">
      	        		<option value="1">ACTIVE</option>
      	        		<option value="2">NOT ACTIVE</option>
      	        	</select>
      	    	</div>
      		</div>
      	</div>
      	<div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        	<button type="submit" class="btn btn-primary">Submit</button>
      	</div>
	{{ Form::close() }}
    </div>
  </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Delete Funding Source</h4>
      	</div>
      	<div class="modal-body">
      		<p>Are you sure you want to delete this row?</p>
      	</div>
      	<div class="modal-footer">
        	<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
        	<a href="" class="btn btn-danger" id="delete-funding-source-btn">Yes</a>
      	</div>
    </div>
  </div>
</div>