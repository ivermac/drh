<h3>Received Commodities (Public Sector Pipeline)</h3>
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<input class="form-control" id="system-search" name="q" placeholder="Table Search (on type)" required="">
	</div>
</div>
@if(Session::has('success-message'))
	<div class="alert-message alert-message-success alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<p>{{ Session::get('success-message') }}</p>
	</div>
@endif
<div class="row">
	<div class="col-md-12">
		@if (count($receivedCommodities)<=0)
			<div class="alert alert-warning no-result"> Unfortunately, there are no results :-( </div>
		@else
		<table class="table table-striped">
		  	<thead>
		  		<tr>
		  			<th>Commodity</th>
					<th>Unit</th>
					<th>Funding Source</th>
					<th>E.T.A</th>
					<th>Date Received</th>
					<th>No. of Days Delayed</th>
					<th>Quantity Expected</th>
					<th>Quantity Received</th>
					<th>Quantity Remaining</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		@foreach ($receivedCommodities as $row)
		  		<tr>
		  			<td>{{ $row->commodity }}</td>
		  			<td>{{ $row->unit }}</td>
		  			<td>{{ $row->funding_source }}</td>
		  			<td>{{ $row->ETA }}</td>
		  			<td>{{ $row->date_received }}</td>
		  			<td>{{ $row->days_delayed }}</td>
		  			<td>{{ number_format($row->qty_expected) }}</td>
		  			<td>{{ number_format($row->qty_received) }}</td>
					<td>{{ number_format($row->qty_remaining) }}</td>
					@if (in_array(Auth::user()->usertype, [1,2]))
					<td>
		  				<div class="btn-group">
						  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
						    Select <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						    <li>{{ HTML::link(URL::route('edit-pipeline', array('id'=>$row->id)), 'Edit') }}</li>
						    <li>
						    	{{ HTML::link(
						    		'#', 
						    		'Delete', 
						    		array(
						    			'record-id'=>$row->id,
						    			'data-toggle'=>'modal', 
						    			'data-target'=>'#modal-delete', 
						    			'class'=>'supply-plan-delete')
						    	) }} 
						    </li>
						  </ul>
						</div>
		  			</td>
					@endif
		  		</tr>
		  		@endforeach
		  	</tbody>
		</table>
		{{ $receivedCommodities->links() }}
		@endif
	</div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Are you sure you wan't to delete this record?</h4>
      	</div>
      	<div class="modal-body">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			{{ HTML::link("#", 'Delete', array('class' => 'delete-supply-plan-record btn btn-danger')) }}	
      	</div>
    </div>
  </div>
</div>