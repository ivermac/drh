<!-- custom inputs -->
<div class="form-group" id="custom-date-cancelled">
	<label for="inputDateCancelled" class="col-sm-4 control-label">Date Cancelled</label>
	<div class="col-sm-8">
		<div class="input-group">
			{{ Form::text('date-cancelled', $result->cancel_date, array('placeholder'=>'Date Cancelled', 'class'=>'form-control training-inputs', 'id'=>'date-cancelled')) }}
		</div>
	</div>
</div>

<div id="custom-date-delayed-to">
	<div class="form-group" >
		<label for="inputDateCancelled" class="col-sm-4 control-label">Date Delayed To</label>
		<div class="col-sm-8">
			<div class="input-group">
				{{ Form::text('date-delayed-to', $result->delay_to, array('placeholder'=>'Date Delayed To', 'class'=>'form-control training-inputs', 'id'=>'date-delayed-to')) }}
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="inputDateCancelled" class="col-sm-4 control-label">Comment</label>
		<div class="col-sm-8">
			<div class="input-group">
				{{ Form::textarea('comment', $result->comment, array('placeholder'=>'Comment', 'class'=>'form-control training-inputs', 'id'=>'comment')) }}
			</div>
		</div>
	</div>
</div>

<div id="custom-incountry">
	<div class="form-group">
		<label for="inputDateCancelled" class="col-sm-4 control-label">Date Incountry</label>
		<div class="col-sm-8">
			<div class="input-group">
				{{ Form::text('date-incountry', $result->date_incountry, array('placeholder'=>'Date Incountry', 'class'=>'form-control training-inputs', 'id'=>'date-incountry')) }}
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="inputDateCancelled" class="col-sm-4 control-label">Quantity Incountry</label>
		<div class="col-sm-8">
			<div class="input-group">
				{{ Form::text('quantity-incountry', $result->qty_incountry, array('placeholder'=>'Quantity Incountry', 'class'=>'form-control training-inputs', 'id'=>'quantity-incountry')) }}
			</div>
		</div>
	</div>
</div>

<div id="custom-received">
	<div class="form-group">
    	<label for="inputDateReceived" class="col-sm-4 control-label">Date Received</label>
    	<div class="col-sm-8">
    		<div class="input-group">
				{{ Form::text('date-received', $result->date_receive, array('placeholder'=>'Date Received', 'class'=>'form-control training-inputs', 'id' => 'date-received')) }}
			</div>
    	</div>
  	</div>
  	<div class="form-group">
    	<label for="inputQuantityReceived" class="col-sm-4 control-label">Quantity Recieved</label>
    	<div class="col-sm-8">
			{{ Form::text('quantity-received', $result->qty_receive, array('placeholder'=>'Quantity Received', 'class'=>'form-control training-inputs')) }}
    	</div>
  	</div>
</div>