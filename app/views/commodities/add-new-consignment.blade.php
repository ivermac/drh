<h3>Add New Consignment</h3>
<div class="row">
	{{ Form::open(array('route' => 'post-new-consignment', 'method' => 'post', 'class' => 'go-right')) }}
	<div class="col-md-12">
		<table class="table table-striped" id="add-new-consignment-table">
		  	<thead>
		  		<tr>
		  			<th>FP Commodity</th>
		  			<th>Store</th>
		  			<th>Funding Source</th>
		  			<th>E.T.A</th>
		  			<th>Procuring Agency</th>
		  			<th>Quantity</th>
		  		</tr>
		  	</thead>
		  	<tbody id="add-new-consignment-table-body">
		  		<tr id="add-new-consignment-row">
			  		<div class="row">
			  			<td><div class="input-group">{{ Form::select('commodity[]', $commodities, '', array('id'=>'commodity', 'class'=>'add-new-consignment-inputs')) }}</div></td>
			  			<td><div class="input-group">{{ Form::select('store[]', $stores, '', array('id'=>'store', 'class'=>'add-new-consignment-inputs')) }}</div></td>
			  			<td><div class="input-group">{{ Form::select('funding-source[]', $funding_sources, '', array('id'=>'store', 'class'=>'add-new-consignment-inputs')) }}</div></td>
			  			<td>
				  			<div class="input-group input-group-sm">
				  				{{ Form::text('ETA[]', '', array('placeholder'=>'E.T.A', 'class'=>'ETA')) }}
				  			</div>
				  		</td>
			  			<td><div class="input-group">{{ Form::select('procuring-agency[]', $procuring_agencies, '', array('id'=>'store', 'class'=>'add-new-consignment-inputs')) }}</div></td>
			  			<td><div class="input-group">{{ Form::text('quantity[]', '', array('placeholder'=>'Quantity', 'class'=>'add-new-consignment-inputs')) }}</div></td>
			  			<td>{{ Form::button('+', array('class'=>'btn btn-primary add-row')) }}</td>
			  			<td>{{ Form::button('-', array('class'=>'btn btn-danger remove-row')) }}</td>
			  		</div>
		  		</tr>
		  	</tbody>
		</table>
		{{ Form::submit('Submit', array('class'=>'btn btn-primary')) }}
	</div>
	{{ Form::close() }}
</div>