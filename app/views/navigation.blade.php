<ul class="nav navbar-nav pull-right">
    @if (in_array(Auth::user()->usertype, [1,3]))
    <li class="{{ ($active != 'service-statistics') ? : 'active' }}">{{ HTML::link(URL::route('family-planning-stock-status'), 'Service Statistics') }}</li>
    @endif

    @if (Auth::user()->email != "guest@admin.com" && in_array(Auth::user()->usertype, [1,2]))
    <li class="{{ ($active != 'rht') ? : 'active' }}">{{ HTML::link(URL::route('reproductive-health-training-new'), 'RHT') }}</li>
    @elseif (Auth::user()->usertype == 3)
    <li class="{{ ($active != 'rht') ? : 'active' }}">{{ HTML::link(URL::route('reproductive-health-training-list'), 'RHT') }}</li>
    @endif

    @if (in_array(Auth::user()->usertype, [1,3]))
    <li class="dropdown {{ ($active != 'commodities') ? : 'active' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Commodities <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>{{ HTML::link(URL::route('commodities-dashboard'), 'Dashboard') }}</li>
            <li class="divider"></li>
            @if (in_array(Auth::user()->usertype, [1]))
            <li>{{ HTML::link(URL::route('add-stock-on-hand'), 'Enter new Stock on Hand') }}</li>
            <li class="divider"></li>
            @endif
            @if (in_array(Auth::user()->usertype, [1]))
            <li>{{ HTML::link(URL::route('add-new-consignment'), 'Add New Consignment') }}</li>
            <li class="divider"></li>
            @endif
            @if (in_array(Auth::user()->usertype, [1,3]))
            <li>{{ HTML::link(URL::route('supply-plan-listing'), 'Supply Plan Listing') }}</li>
            <li class="divider"></li>
            @endif
            <li>{{ HTML::link(URL::route('detailed-stock-on-hand'), 'Detailed Stock on Hand') }}</li>
            <li class="divider"></li>
            <li>{{ HTML::link(URL::route('received-commodities-listing'), 'Received Commodities Listing') }}</li>
            @if (in_array(Auth::user()->usertype, [1,3]))
            <li class="divider"></li>
            <li><a href="#downloads" data-toggle="modal" data-target="#download-modal">Report</a></li>
            @endif
        </ul>
    </li> 
    @endif

    @if (in_array(Auth::user()->usertype, [1,3]))
        <li class=" dropdown {{ ($active != 'LMIS') ? : 'active' }}">{{ HTML::link(URL::route('lmis-home'), 'LMIS') }}</li>
    @endif

    @if (in_array(Auth::user()->usertype, [1]))
    <li class=" dropdown {{ ($active != 'settings') ? : 'active' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <!-- <li>{{ HTML::link(URL::route('supply-chain'), 'Supply Chain') }}</li>
            <li class="divider"></li>
            <li>{{ HTML::link(URL::route('stock-on-hand'), 'Stock on Hand') }}</li>
            <li class="divider"></li> 
            <li>{{ HTML::link(URL::route('other-settings'), 'Other Settings') }}</li>
            <li class="divider"></li> -->
            <li>{{ HTML::link(URL::route('commodities-listing'), 'Commodities Listing') }}</li>
            <li class="divider"></li>
            <li>{{ HTML::link(URL::route('funding-sources-listing'), 'Funding Sources Listing') }}</li>
            <li class="divider"></li>
            <li>{{ HTML::link(URL::route('list-training'), 'Trainings') }}</li>
        </ul>
    </li>
    @endif

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="profile-details">
            {{ HTML::decode('<span class="glyphicon glyphicon-user"></span> '.Auth::user()->firstname." ".Auth::user()->lastname) }} 
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li>
                <p class="profile-details">{{ '<span class="glyphicon glyphicon-envelope"></span> '.Auth::user()->email }}</p>
            </li>
            <!-- <li>
                <p class="profile-details">{{ '<span class="glyphicon glyphicon-earphone"></span> '.Auth::user()->phonenum }}</p>
            </li> -->
            @if (in_array(Auth::user()->usertype, [1,2]))
            <li class="divider"></li>
            <li>{{ HTML::decode(HTML::link('users/logout', '<span class="glyphicon glyphicon-log-out"></span> Edit Settings')) }}</li>
            <li>{{ HTML::decode(HTML::link('users/logout', '<span class="glyphicon glyphicon-log-out"></span> Change Password')) }}</li>
            @endif
            <li>{{ HTML::decode(HTML::link('users/logout', '<span class="glyphicon glyphicon-log-out"></span> logout')) }}</li>
        </ul>
    </li>
</ul>