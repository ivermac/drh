<h3>Add New Training</h3>

<ul class="nav nav-pills">
  <li>{{ HTML::link(URL::route('list-training'), 'Listing') }}</li>
  <li class="active">{{ HTML::link(URL::route('new-training'), 'New') }}</li>
</ul>

<div class="row">
    <div class="col-md-12">
        {{ Form::open(array('route' => 'training-add', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form' )) }}
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="name comes here" name="training-name">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Identifier</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="identifier comes here" name="identifier">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>