<h3>Edit Training</h3>

<ul class="nav nav-pills">
  <li class="active">{{ HTML::link(URL::route('list-training'), 'Listing') }}</li>
</ul>

<div class="row">
    <div class="col-md-12">
        {{ Form::open(array('route' => 'training-edit', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form' )) }}
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                {{ Form::text('training-name', $training->name, array('placeholder'=>'training name comes here', 'class'=>'form-control training-inputs')) }}
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Identifier</label>
                <div class="col-sm-10">
                {{ Form::text('identifier', $training->identifier, array('placeholder'=>'identifier comes here', 'class'=>'form-control training-inputs')) }}
                </div>
            </div>
            {{ Form::hidden('record-id', $training->id); }}
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>