<h3>Training Listing</h3>
<ul class="nav nav-pills">
  <li class="active">{{ HTML::link(URL::route('list-training'), 'Listing') }}</li>
  <li>{{ HTML::link(URL::route('new-training'), 'New') }}</li>
</ul>

<div>
	@if(Session::has('splash-message'))
	<div class="alert-message alert-message-success alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<p>{{ Session::get('splash-message') }}</p>
	</div>
	@endif
</div>
@if (count($training)<=0)
	<div class="alert alert-warning no-result"> Unfortunately, there are no results :-( </div>
@else

<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<th>Name</th>
					<th>Identifier</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($training as $row)
				<tr>
					<td>{{ $row->name }}</td>
					<td>{{ $row->identifier }}</td>
					<td>
						<div class="btn-group">
						  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
						    Select <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						    <li>
						    	{{ HTML::link(
						    		'#', 
						    		'Details', 
						    		array(
						    			'record-id'=>$row->id, 
						    			'data-toggle'=>'modal', 
						    			'data-target'=>'#modal-details', 
						    			'class'=>'rht-detail')
						    	) }}
						    </li>
						    <li> {{ HTML::link(URL::route('edit-training', array('id'=>$row->id)), 'Edit') }}</li>
						    <li> {{ HTML::link(URL::route('delete-training', array('id'=>$row->id)), 'Delete') }} </li>
						  </ul>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{{ $training->links() }}
	</div>
</div>
@endif