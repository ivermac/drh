@if (count($facilities)<=0)
	<div class="alert alert-warning no-result"> Unfortunately, there are no results :-( </div>
@else
<table class="table table-striped table-bordered table-condensed">
	<thead>
		<tr>
			<th>Facility Code</th>
			<th>Facility Name</th>
			<th>District</th>
			<th>County</th>
			<th>Owner</th>
			<th>Type</th>
			<th>Level</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($facilities as $row)
		<tr>
			<td>{{ $row->facility_code }}</td>
			<td>{{ $row->facility_name }}</td>
			<td>{{ $row->district }}</td>
			<td>{{ $row->county }}</td>
			<td>{{ $row->owner }}</td>
			<td>{{ $row->type }}</td>
			<td>{{ $row->level }}</td>
			<td>
				<div class="btn-group">
				  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
				    Select <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li>
				    	{{ HTML::link(
				    		'#', 
				    		'Details', 
				    		array(
				    			'record-id'=>$row->facility_code, 
				    			'data-toggle'=>'modal', 
				    			'data-target'=>'#modal-details', 
				    			'class'=>'rht-detail')
				    	) }}
				    </li>
				    <li>{{-- HTML::link(URL::route('edit-facility', array('id'=>$row->facility_code)), 'Edit') --}}</li>
				    <li>
				    	{{ HTML::link(
				    		'#', 
				    		'Delete', 
				    		array(
				    			'record-id'=>$row->facility_code,
				    			'data-toggle'=>'modal', 
				    			'data-target'=>'#modal-delete', 
				    			'class'=>'rht-delete')
				    	) }}
				    </li>
				  </ul>
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{ $facilities->links() }}
@endif