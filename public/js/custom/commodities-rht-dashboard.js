/*global 
    $, jQuery, 
    urlGetFacilityOwnershipRHTTrainingBreakdown,
    urlGetPercentageOfStaffTrained,
    urlGetPercentageOfFacilitiesWithAtleastOnePersonTrained,
    urlGetCountiesInProvince,
    urlGetRHTDashboardData
*/
$(function () {
    'use strict';
    var
        selected, url, data, 
        chart1, chart2, setChart, tab_width = $('#graph-1').width() - 20,
        ajaxCall = function (url, data, callback) {
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: data,
                beforeSend: function () {
                     $("#processing-modal").modal('show');
                },
                success: function (data) {
                     $("#processing-modal").modal('hide');
                    callback(data);
                },
                error: function (xhr, text, error) {
                    console.log("xhr >>> ", xhr);
                    console.log("text >>> ", text);
                    console.log("error >>> ", error);
                },
            });
        },
        setSelected = function(selectedCommodity) {
            selected = selectedCommodity;
            if (selected === "Cerival cancer screening(VIA/VILLI)") {
                selected = selected.slice(0, selected.indexOf('/'))
            }
            url = urlRHTCountySummary + "/" + selected;
            $("#county-summary-link").attr('href', url);
        },
        setChartAndDownloadLink = function(url) {
            url = url;
            setSelected($("#trainings :selected").val());
            
            data = {
                training: $("#trainings :selected").val(),
                owner: $("#owner :selected").text(),
                "former-province": $("#former-province :selected").text(),
                county: $("#county :selected").text()
            };
            ajaxCall(url, data, trainingCallback);

        },
        onLoading = function () {
            setChartAndDownloadLink(urlGetPercentageOfStaffTrained);
        },
        drillDownColumnChart = function (drillDownData, selector, theTitle, theChart, training) {

            function setChart(name, categories, data, color, title) {
                theChart.xAxis[0].setCategories(categories);
                theChart.series[0].remove();
                theChart.setTitle({ text: title });
                
                theChart.addSeries({
                    name: name,
                    data: data,
                    color: 'rgb(47, 126, 216)'
                });
            }

            theChart = new Highcharts.Chart({
               chart: {
                  renderTo: selector, 
                  type: 'column',
                  width: tab_width,
               },
               title: {
                  text: theTitle,
                  style: {
                      fontFamily: "Century Gothic",
                      fontWeight: "normal",
                      fontSize: "15px",
                  },
               },
               xAxis: {
                  categories: true,
                  labels: {
                      style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "15px",
                        }
                  }
               },
               yAxis: {
                  min: 0,
                  title: {
                     text: ''
                  },
                  labels: {
                      style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "15px",
                        }
                  }
               },
               legend: {
                    enabled: false
               },
               plotOptions: {
                  column: {
                     cursor: 'pointer',
                     point: {
                        events: {
                           click: function() {
                              var drilldown = this.drilldown;
                              console.log("drilldown >>> ", drilldown);
                              if (drilldown) { // drill down
                                  setChart(drilldown.name, true, drilldown.data, drilldown.color, (theTitle + " in " + drilldown.name ));
                              } else { // restore
                                 setChart(name, true, drillDownData, null, theTitle);
                              }
                           },
                           mouseOver: function() {
                                if ( selector === "atleast-one-person-trained-data" ) {
                                    var postValues;
                                    if (this.drilldown) {
                                        postValues = {
                                            "region": this.drilldown.name,
                                            "level": this.drilldown.level
                                        }
                                       
                                    } else {
                                        postValues = {
                                            "region": this.name,
                                            "level": 3
                                        }
                                    }
                                    postValues["training"] = training;

                                    $.ajax({
                                        type: "POST",
                                        url: urlGetFacilityOwnershipRHTTrainingBreakdown,
                                        dataType: 'json',
                                        data: postValues,
                                        beforeSend: function () {
                                            $("#facility-ownership-breakdown").empty();
                                        },
                                        success: function (data) {
                                            $.each({"Training: ": training, "Region: ": postValues.region}, function(key, value) {
                                                $("#facility-ownership-breakdown").append(
                                                    $("<p>", { text: key }).append(
                                                        $("<span>", { class: "label label-primary", text: value })
                                                    )
                                                )
                                            })

                                            var badge = $("<ul>", { class: "nav nav-pills nav-stacked"});
                                            $.each(data, function(owner, total) {
                                                badge.append(
                                                    $("<li>", { class: "active" }).append(
                                                        $("<a>", { text: owner }).append(
                                                            $("<span>", { class: "badge pull-right", text: total })
                                                        )
                                                    )
                                                )
                                            });
                                            $("#facility-ownership-breakdown").append( badge )
                                        },
                                        error: function (xhr, text, error) {
                                            console.log("xhr >>> ", xhr);
                                            console.log("text >>> ", text);
                                            console.log("error >>> ", error);
                                        },
                                    }).done(function( data ) {
                                        if ( console && console.log ) {
                                            // console.log( "tooltip data:", data );
                                        }
                                        return data;
                                    });
                                }
                           }
                        }
                     },
                     dataLabels: {
                        enabled: true,
                        // color: colors[0],
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "15px",
                        },
                        formatter: function() {
                           return this.y +'%';
                        }
                     }               
                  }
               },
               tooltip: {
                  formatter: function() {
                     var point = this.point, s = '';

                        if (point.drilldown) {
                            s = point.name + ':<b>' + this.y + ' %</b><br/>';
                            s += 'Click to view ' + point.name + ' drilldown';
                        } else {
                            s = point.name + ':<b>' + this.y + '</b><br/>';
                            s += 'Click to return to initial view';
                        }

                    return s;
                  },
                  style: {
                      fontFamily: "Century Gothic",
                      fontWeight: "normal",
                      fontSize: "15px",
                  },
               },
               series: [{
                  name: name,
                  data: drillDownData,
               }],
               exporting: {
                  enabled: false
               }
            });
        },

        columnGraph = function(data, selector, title, theChart, yAxisTitle) {
            console.log("data 3 : ", data)
            var categories = [], series = [];
            $.each(data, function (key, value){
                if (value['former_province'])
                    categories.push(value['former_province'])
                else if (value['county'])
                    categories.push(value['county'])
                else if (value['district'])
                    categories.push(value['district'])

                if (value['no_of_people_trained'] >= 0)
                    series.push(value['no_of_people_trained'])
                else if (value['facilities_with_atleast_one_person_trained'] >= 0)
                    series.push(value['facilities_with_atleast_one_person_trained'])
            })
            console.log("data 4: ", series)
            theChart = new Highcharts.Chart({
                chart: {
                    renderTo: selector, 
                    type: 'column',
                },
                title: {
                    text: title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },    
                xAxis: {
                    categories: categories,
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                yAxis: {
                    // type: 'logarithmic',
                    min: 0,
                    title: {
                        text: yAxisTitle,
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span>'+
                                  '<table>',
                    pointFormat: '<tr>'+
                                    '<td style="color:{series.color};padding:0"><b>{point.y:.2f} %</b></td>' +
                                  '</tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        // colorByPoint: true,
                        borderWidth: 0
                    }
                },
                legend: {
                    enabled: false,
                },
                series: [{
                    data: series
                }]
            });
        },
        trainingCallback = function (data) {
            if (data['Personnel Data']) {
                columnGraph(
                    data['Personnel Data'], 
                    "personnel-training-data",
                    "% of staff trained in " + data['Training'] + " ("+data['Owner']+")",
                    chart1,
                    'Percentage'
                )
            } else if (data['Atleast 1 Person Trained']) {
                columnGraph(
                    data['Atleast 1 Person Trained'], 
                    "atleast-one-person-trained-data",
                    "% of facilities with at least 1 person trained in " + data['Training'] + " ("+ data['Owner']+")",
                    chart2,
                    'Percentage'
                )
            }

        },
        setCounties =  function (province) {
            var callback = function (data) {
                    $("#county").empty().append(
                        $("<option>", { value: "all" }).text("--All Counties--")
                    );
                    $.each(data, function(name1, name2) {
                        $("#county").append(
                            $("<option>", { value:name1 }).text(name2)
                        )
                    })
                };

            url = urlGetCountiesInProvince;
            data = {
                id: province
            };
            ajaxCall(url, data, callback);
        },
        events = function () {
            $("#trainings").on("change", function() {
                setSelected($("#trainings :selected").val());
            });

            $("#graph-data a").click(function (e) {
                var tab = $(this).attr('data-tab');
                if (tab === "staff-trained") {
                    setChartAndDownloadLink(urlGetPercentageOfStaffTrained);
                } else if (tab === "facilities-with-atleast-one=person-trained") {
                    setChartAndDownloadLink(urlGetPercentageOfFacilitiesWithAtleastOnePersonTrained);
                }
            });

            $('#btn-filter').click(function (e) {
                console.log("i was clicked")
                var tab = $('ul.nav.nav-pills li.active a[data-tab]').attr('data-tab');
                if (tab === "staff-trained") {
                    setChartAndDownloadLink(urlGetPercentageOfStaffTrained);
                } else if (tab === "facilities-with-atleast-one=person-trained") {
                    setChartAndDownloadLink(urlGetPercentageOfFacilitiesWithAtleastOnePersonTrained);
                }
            })

            $("#former-province").on("change", function(){
                var province = $(this).val();
                setCounties(province);
            });
        },
        init = function () {
            events();
            onLoading();
        }();
});