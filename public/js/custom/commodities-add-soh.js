/*global
    $, jQuery,
    urlCalImagePath,
*/
$(function () {
    'use strict';
    var row, all_rows, date_field, calendar_img,
        init = function () {
            $(".date-as-of").datepicker({
                showOn: "button",
                buttonImage: urlCalImagePath,
                buttonImageOnly: true,
                dateFormat: "yy-mm-dd"
            }).each(function () {
                date_field = $(this);
                calendar_img = $(this).next();

                date_field.parent().append($('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img));
            });

            $('.add-row').on("click", function () {
            	row = $('#add-soh-row').clone(true);

            	row.find('.date-as-of')
                    .removeAttr('id')
                    .removeClass('hasDatepicker')
                    .removeData('datepicker')
                    .unbind()
                    .datepicker({
                    showOn: "button",
                    buttonImage: urlCalImagePath,
                    buttonImageOnly: true,
                    dateFormat: "yy-mm-dd"
                }).each(function () {
                    date_field = $(this),
                    calendar_img = $(this).next();
                    date_field.parent()
                        .find('.datepicker-img').remove().end()
                        .append($('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img));
                }).end()
                $('#add-soh-table-body').append(row);
            });

            $('.remove-row').on("click", function() {
                all_rows = parseInt($('#add-soh-table-body tr').length);
                row = $(this).closest("#add-soh-row");
                if (all_rows > 1) {
                    row.remove();
                }
            })
        }();
});