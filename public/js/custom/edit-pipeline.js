$(function() {
	var
		date_field, calendar_img, selected_value, modal,
        setDatePicker = function(selector) {
            $(selector).datepicker({
                showOn: "button",
                buttonImage: urlCalImagePath,
                buttonImageOnly: true,
                dateFormat: "yy-mm-dd"
            }).each(function () {
                date_field = $(this),
                calendar_img = $(this).next();
                date_field.parent().append($('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img));
            });
        },
		init = function() {
            // hide the custom inputs
            $('#custom-date-cancelled, #custom-date-delayed-to, #custom-incountry, #custom-received').hide();

            // set the datepickers
            setDatePicker("#date-expected, #date-received, #date-cancelled, #date-delayed-to, #date-incountry");

            $('#status').on("change", function() {
                selected_value = $(this).val();

                modal = $('#custom-inputs .modal-body').children().hide().end(); 
                if (selected_value === "CANCELLED") {
                    modal.append(
                        $('#custom-date-cancelled').show()
                    )
                } else if (selected_value === "DELAYED") {
                    modal.append(
                        $('#custom-date-delayed-to').show()
                    )
                } else if (selected_value === "INCOUNTRY") {
                    modal.append(
                        $('#custom-incountry').show()
                    )
                } else if (selected_value === "RECEIVED") {
                    modal.append(
                        $('#custom-received').show()
                    )
                } else if (selected_value === "PENDING") {
                    return true;
                }
                
                $('#custom-inputs').modal({
                    show: true,
                })
            });

		}();
});