/*global
    $, jQuery,
    Highcharts,
    loadingImage,
    urlDeliverChart,
    urlLoadingImage,
    urlGetExportCsvToMysql,
    urlDashboard,
    urlFamilyPlanningStockDelete,
    urlGeneratePieChartNationalData,
    urlGenerateBarGraphByCounty,
    urlGenerateBarGraphByConsumption,
    urlUploadProcessor,
    urlGetLineGraphConsumptionTrend
*/
var service_statistics = (function() {
    'use strict';
    var
        url, title, data, selectedCounty, selectedYear, selectedMonth, selectedCommodity, uploded_file_name,
        tab_width = $('#stock-status').width(), theChart, currentChartName,
        months = {
            "01": "January", "02": "February", "03": "March", "04": "April", "05": "May", "06": "June",
            "07": "July", "08": "August", "09": "September", "10": "October", "11": "November", "12": "December",
        },
        ajaxCall = function (url, data, callback, title, year) {
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    $("#processing-modal").modal('show');
                },
                success: function (serverData) {
                    $('#graph-section').empty();
                    if (title.indexOf('percentage of clients by method')>=0) {
                        $('#removals-section').show()
                    } else {
                        $('#removals-section').hide()
                    }

                    if (title.indexOf('Comparison within county for')>=0 || title.indexOf('percentage of clients by method')>=0) {
                        $('#graph-comments').show()
                    } else {
                        $('#graph-comments').hide()
                    }
                    callback(serverData, title, year);
                },
                complete: function() {
                    $("#processing-modal").modal('hide');
                },
                error: function (xhr, text, error) {
                    console.log("xhr >>> ", xhr);
                    console.log("text >>> ", text);
                    console.log("error >>> ", error);
                },
            });
        },
        /*rand = function () {
            return Math.random().toString(36).substr(2); // remove `0.`
        },
        generateToken = function () {
            return rand() + rand(); // to make it longer
        },*/
        initialDashboardLoad = function () {
            url = urlGeneratePieChartNationalData;
            selectedYear = new Date().getFullYear();
            title = "National percentage of clients by method in " + selectedYear;
            data = {
                year: selectedYear,
                county: 'all'
            };
            ajaxCall(url, data, demoChart, title);
        },
        exportCSVToMysqlOutput = function(serverData) {
            console.log("export csv to mysql output >>> ", serverData);
            initialDashboardLoad();
        },
        fileUploader = function () {
            $('#fileupload').fileupload({
                type: "POST",
                url: urlUploadProcessor,
                dataType: 'json',
                done: function (e, data) {
                    /*jslint unparam: true*/
                    $.each(data.result.files, function (index, file) {
                        $('<p/>').text(file.name).appendTo('#files');
                    });
                    /*jslint unparam: false*/
                    uploded_file_name = data.files[0].name;
                    url = urlGetExportCsvToMysql;
                    data = {
                        filename: uploded_file_name,
                        module: "service-statistics"
                    };
                    ajaxCall(url, data, exportCSVToMysqlOutput);
                },
                progress: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        },
        demoChart = function (data, title) {
            $('#removals-column-graph').highcharts({
                chart: {
                    type: 'bar',
                    height: 200,
                },
                title: {
                    text: "Removals",
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },    
                xAxis: {
                    categories: ["IUCD Removals", "Implants Removals"],
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                yAxis: {
                    // type: 'logarithmic',
                    min: 0,
                    title: {
                        text: '',
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span>'+
                                  '<table>',
                    pointFormat: '<tr>'+
                                    '<td style="color:{series.color};padding:0"><b>{point.y:.0f}</b></td>' +
                                  '</tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        // colorByPoint: true,
                        borderWidth: 0
                    }
                },
                legend: {
                    enabled: false,
                },
                credits: {
                    position: {
                        align: 'left',
                        x: 20
                    },
                    href: "https://hiskenya.org/dhis-web-commons/security/login.action;jsessionid=E3A9BAAED48BB18C9A379CEB5E07CCE7",
                    text: "Source: DHIS-2"
                },
                series: [{
                    data: [data.removals['IUCD Removals'], data.removals['Implants Removals']]
                }]
            });

            currentChartName = "national-consumption";
            theChart = new Highcharts.Chart({
                chart: {
                    renderTo: 'graph-section',
                    defaultSeriesType: 'pie',
                    // borderWidth: 1
                },
                tooltip: {
                    formatter: function() {
                        var desc = { 'Injections': 'Adjusted to account for the number of visits by typical user of injection (Total Clients/4)',
                                     'COCs': 'Adjusted to account for the number of visits by typical user of injection (Total Clients/4)',
                                     'POPs': 'Adjusted to account for the number of visits by typical user of injection (Total Clients/4)',
                                     'Condoms': 'Adjusted to account for the number of visits by typical user of injection (Total Clients/12)'}
                        if (desc[this.key] !== undefined)
                            return "Commodity: <b>" + this.key + "</b><br>Total: <b>" + this.y.toLocaleString() + "</b><br><i>" + desc[this.key] + "</i>"
                        return "Commodity: <b>" + this.key + "</b><br>Total: <b>" + this.y.toLocaleString() + "</b>"
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            style: {
                                fontFamily: "Century Gothic",
                                fontWeight: "normal",
                                fontSize: "13px",
                            },
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                title: {
                    text: title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },
                credits: {
                    position: {
                        align: 'left',
                        x: 20
                    },
                    href: urlDashboard,
                    text: 'Reproductive and maternal health services unit'
                },
                series: [{
                    name: 'Share ',
                    data: data.totals
                }]
            }, function(chart) {
            });
        },
        demoCompoundBarGraph = function (data, title) {
            currentChartName = "county-comparison";
            theChart = new Highcharts.Chart({
                chart: {
                    renderTo: 'graph-section',
                    type: 'column',
                    width: tab_width,
                    // borderWidth: 1
                },
                title: {
                    text: title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },
                credits: {
                    position: {
                        align: 'left',
                        x: 20
                    },
                    href: urlDashboard,
                    text: 'Reproductive and maternal health services unit'
                },
                xAxis: {
                    categories: data.districts,
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                yAxis: {
                    title: {
                        text: 'Percentage',
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "18px",
                        },
                    },
                    // type: 'logarithmic'
                },
                legend: {
                    enabled: true,
                    borderRadius: 0,
                    itemStyle: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "13px",
                    },
                },
                plotOptions: {
                    /*series: {
                        pointPadding: 0.2,
                        groupPadding: 0.1
                    },*/
                    column: {
                        stacking: 'percent',
                    }
                },
                colors: [
                    '#2f7ed8',
                    '#0d233a',
                    '#8bbc21',
                    '#910000',
                    '#1aadce',
                    '#492970',
                    '#f28f43',
                    '#B28552',
                    '#0B07FA',
                    '#07FA9D',
                    '#276C20',
                    '#B0B252'
                ],
                // colors: [
                //     '#ff0000',
                //     '#00ff00',
                //     '#0000ff'
                // ],
                series: data.all_data
            }, function(chart) {
            });
        },
        demoBarGraph = function (data, title) {
            var
                series1 = [], series2 = [];
            $.each(data.drilldown, function (former_province, county_and_data) {
                series1.push({
                    id: former_province,
                    name: former_province,
                    data: county_and_data
                });
            });

            $.each(data.totals, function (former_province, total) {
                series2.push({
                    name: former_province,
                    y: total,
                    drilldown: former_province
                });
            });

            currentChartName = "counties-services";
            theChart = new Highcharts.Chart({
                chart: {
                    renderTo: 'graph-section',
                    type: 'column',
                },
                lang: {
                    drillUpText: '◁ Back to {series.name}'
                },
                title: {
                    text: title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },
                legend: {
                    enabled: false
                },
                subtitle: {
                    text: 'Click the columns to view county data'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                yAxis: {
                    title: {
                        text: 'Total services',
                        enabled: true,
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "18px",
                        }
                    },
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            style: {
                                fontFamily: "Century Gothic",
                                fontWeight: "normal",
                                fontSize: "13px",
                            },
                            format: '<b>{point.y}</b>'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name} Total </span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> <br/> '
                }, 

                series: [{
                    name: $("#consumption-by-counties :selected").text() ,
                    colorByPoint: true,
                    data: series2
                }],
                drilldown: {
                    series: series1
                }
                /*chart: {
                    height: 300,
                    renderTo: 'container',
                    type: 'pie',
                },

                lang: {
                    drillUpText: '◁ Back to {series.name}'
                },

                title: {
                    text: title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },

                xAxis: {
                    categories: true
                },

                drilldown: {
                    series: series1
                },

                credits: {
                    href: urlDashboard,
                    text: 'Reproductive and maternal health services unit'
                },

                legend: {
                    enabled: false
                },

                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true
                        },
                        shadow: false
                    },
                    pie: {
                        size: '80%',
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            style: {
                                fontFamily: "Century Gothic",
                                fontWeight: "normal",
                                fontSize: "13px",
                            },
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },

                series: [{
                    name: 'Overview',
                    colorByPoint: true,
                    data: series2
                }]*/
            }, function(chart) {
            });
        },
        demoLineGraph = function (data, title, year) {
            var
                concatYearMonth = [], series = [],
                month = [ '01', '02', '03', '04', '05', '06',
                    '07', '08', '09', '10', '11', '12'];
            /*jslint unparam: true*/
            $.each(month, function (key, value) {
                concatYearMonth.push(year + value);
            });

            $.each(concatYearMonth, function (key, value) {
                if (data[value] === null || data[value] === "" || data[value] === undefined) {
                    series.push(0);
                } else {
                    series.push(data[value]);
                }
            });
            /*jslint unparam: false*/

            currentChartName = "consumption-trend";
            theChart = new Highcharts.Chart({
                chart: {
                    renderTo: 'graph-section',
                },
                title: {
                    text: title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },
                credits: {
                    position: {
                        align: 'left',
                        x: 20
                    },
                    href: urlDashboard,
                    text: 'Reproductive and maternal health services unit'
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total Services',
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
                    labels: {
                        step: 1,
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    }
                },
                legend: {
                    enabled: true,
                    borderRadius: 0,
                    itemStyle: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "13px",
                    },
                },
                series: [{
                    name: 'Consumption trend',
                    data: series
                }]
            }, function(chart) {
            });
        },
        deliverChart = function(chart, chartName) { 
            var svg = chart.getSVG(),
                callback = function(data) {
                    initialDashboardLoad()
                };
            data = {
                svg: svg,
                chartName: chartName
            }
            url = urlDeliverChart
            ajaxCall(url, data, callback)
        },
        eventTriggers = function () {
            $("#save-chart-as-image").on("click", function(){
                deliverChart(theChart, currentChartName)
            })

            $('#generate-pie-chart-national-data').on('click', function () {
                selectedYear = $('#national-data-years').val().trim();
                selectedCounty = $('#national-data-county').val().trim();
                url = urlGeneratePieChartNationalData;
                if (selectedCounty == 'all')
                    title = "National percentage of clients by method in " + selectedYear;
                else
                    title = selectedCounty + " percentage of clients by method in " + selectedYear;
                data = {
                    year: selectedYear,
                    county: selectedCounty
                };
                ajaxCall(url, data, demoChart, title);
            });

            $('#generate-bar-graph-by-county').on('click', function () {
                selectedCounty = $('#compare-by-counties').val().trim();
                selectedYear = $('#compare-by-county-years').val().trim();
                selectedMonth = $('#compare-by-county-months').val().trim();
                url = urlGenerateBarGraphByCounty;
                if (selectedMonth !== "00") {
                    title = "Comparison within county for " + months[selectedMonth] + ", "+ selectedYear;
                } else {
                    title = "Comparison within county for " + selectedYear;
                }
                data = {
                    county: selectedCounty,
                    year: selectedYear,
                    month: selectedMonth
                };
                ajaxCall(url, data, demoCompoundBarGraph, title);
            });

            $('#generate-bar-graph-by-consumption').on('click', function () {
                selectedCommodity = $('#consumption-by-counties').val().trim();
                selectedYear = $('#consumption-by-county-years').val().trim();
                selectedMonth = $('#consumption-by-county-months').val().trim();
                url = urlGenerateBarGraphByConsumption;
                if (selectedMonth !== "00") {
                    title = $('#consumption-by-counties :selected').text() + " services for " + months[selectedMonth] + ", "+ selectedYear;
                } else {
                    title = $('#consumption-by-counties :selected').text() + " services for " + selectedYear;
                }
                data = {
                    commodity: selectedCommodity,
                    year: selectedYear,
                    month: selectedMonth
                };
                ajaxCall(url, data, demoBarGraph, title);
            });

            $('#generate-line-graph-consumption-trends').on('click', function () {
                selectedCounty = $('#consumption-trend-county').val().trim();
                selectedYear = $('#consumption-trend-year').val().trim();
                selectedCommodity = $('#consumption-trend-commodity :selected');
                url = urlGetLineGraphConsumptionTrend;
                title = selectedCommodity.text() + " consumption trend in " + selectedCounty + " in " + selectedYear;
                data = {
                    commodity: selectedCommodity.val().trim(),
                    year: selectedYear,
                    county: selectedCounty,
                };
                ajaxCall(url, data, demoLineGraph, title, selectedYear);
            });

            $("#delete-service-statistics-period-data").on("click", function() {
                var period = $("#service-statistics-year").val() + $("#service-statistics-month").val(),
                    callback = function(data) {
                        var alert = $("#delete-modal-alert");
                        alert.empty()
                        if (data > 0) {
                            alert.append("Records Deleted: " + data)
                        } else {
                            alert.append("No Data for period submitted" )
                        }
                        alert.show();
                        initialDashboardLoad();
                    };
                url = urlFamilyPlanningStockDelete;
                data = {
                    period: period
                };

                ajaxCall(url, data, callback);
            })
        },
        onloading = function () {
            $("#delete-modal-alert").hide();
        },
        init = function () {
            onloading();
            fileUploader();
            eventTriggers();
            initialDashboardLoad();
        };
        return { init:init };
}());