$(function () {
    'use strict';
    var
    	date_field, calendar_img, row, all_rows,
    	init = function () {
    		$(".ETA").datepicker({
    		    showOn: "button",
    		    buttonImage: urlCalImagePath,
    		    buttonImageOnly: true,
    		    dateFormat: "yy-mm-dd"
    		}).each(function () {
                date_field = $(this),
                calendar_img = $(this).next();

                date_field.parent().append($('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img));
            });

    		$('.add-row').on("click", function () {
            	row = $('#add-new-consignment-row').clone(true);

            	row.find('.ETA')
            	    .removeAttr('id')
            	    .removeClass('hasDatepicker')
            	    .removeData('datepicker')
            	    .unbind()
            	    .datepicker({
            	    showOn: "button",
            	    buttonImage: urlCalImagePath,
            	    buttonImageOnly: true,
            	    dateFormat: "yy-mm-dd"
            	}).each(function () {
            	    date_field = $(this),
            	    calendar_img = $(this).next();
            	    date_field.parent()
            	        .find('.datepicker-img').remove().end()
            	        .append($('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img));
            	}).end()
            	$('#add-new-consignment-table-body').append(row);
    		});

    		$('.remove-row').on("click", function() {
    			all_rows = parseInt($('#add-new-consignment-table-body tr').length);
    			row = $(this).closest("#add-new-consignment-row");
    			if (all_rows > 1) {
    			    row.remove();
    			}
    		})
    	}();
});