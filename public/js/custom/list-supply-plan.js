/*global 
    $, jQuery, 
    urlCalImagePath,
    urlDeleteSupplyPlanRecord
*/
$(function () {
    'use strict';
    var
    	chosenDropdown = function () {
            var
                config = {
                    '.chosen-select'           : {},
                    '.chosen-select-deselect'  : { allow_single_deselect: true },
                    '.chosen-select-no-single' : { disable_search_threshold: 10 },
                    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                    '.chosen-select-width'     : { width: "95%" }
                }, selector;
            for (selector in config) {
                if (config.hasOwnProperty(selector)) {
                    $(selector).chosen(config[selector]);
                }
            }
        },
        onLoading = function () {
            var
                the_datepicker_field = $("#supply-plan-listing-date-picker"),
                calendar_img;

            the_datepicker_field.datepicker({
                showOn: "button",
                buttonImage: urlCalImagePath,
                buttonImageOnly: true,
                dateFormat: "yy-mm-dd"
            });
            calendar_img = the_datepicker_field.next();
            the_datepicker_field
                .parent()
                    .append(
                        $('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img)
                    );
            $("#supply-plan-listing-date-picker-div").hide();
        },
        events = function () {
        	// use elvis operator
            $("#date-category").on("change", function() {
                var
                    selected = $(this),
                    datepicker_div = $('#supply-plan-listing-date-picker-div');
                if (selected.val() === null || selected.val() === "") {
                    datepicker_div.hide();
                } else {
                    datepicker_div.show();
                }
            });

            $('.supply-plan-delete').on('click', function () {
            	var
                	recordId = $(this).attr('record-id');
                $('.delete-supply-plan-record').attr('href', urlDeleteSupplyPlanRecord + "/" + recordId);
            });
        },
    	init = function() {
    		chosenDropdown();
    		onLoading();
    		events();
    	}();
});