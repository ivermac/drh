/*global 
    $, jQuery, 
    urlDeleteSupplyPlanRecord,
    urlLoadingImage,
	loadingImage
*/
$(function() {
	'use strict';
    var
        url, data,
        ajaxCall = function (url, data, callback, container) {
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    $("#processing-modal").modal('show');
                },
                success: function (data) {
                    $("#processing-modal").modal('hide');
                    callback(data);
                },
                error: function (xhr, text, error) {
                    console.log("xhr >>> ", xhr);
                    console.log("text >>> ", text);
                    console.log("error >>> ", error);
                },
            });
        },

        onLoading = function () {
            
        },
        events = function () {
            $('.supply-plan-delete').on('click', function () {
                var
                    recordId = $(this).attr('record-id');
                $('.delete-supply-plan-record').attr('href', urlDeleteSupplyPlanRecord + "/" + recordId);
            });
        },
        realTimeTableSearch = function () {
            //something is entered in search form
            $('#system-search').keyup(function () {
                var
                    that = this,
                // affect all table rows on in systems table
                    tableBody = $('.table tbody'),
                    tableRowsClass = $('.table tbody tr'),
                    rowText, inputText;
                $('.search-sf').remove();
                tableRowsClass.each(function (i, val) {
                    //Lower text for case insensitive
                    rowText = $(val).text().toLowerCase();
                    inputText = $(that).val().toLowerCase();
                    if (inputText !== '') {
                        $('.search-query-sf').remove();
                        tableBody.prepend('<tr class="search-query-sf"><td colspan="17"><strong>Searching for: "'
                            + $(that).val()
                            + '"</strong></td></tr>');
                    } else {
                        $('.search-query-sf').remove();
                    }

                    if (rowText.indexOf(inputText) === -1) {
                        //hide rows
                        tableRowsClass.eq(i).hide();
                    } else {
                        $('.search-sf').remove();
                        tableRowsClass.eq(i).show();
                    }
                });
                //all tr elements are hidden
                if (tableRowsClass.children(':visible').length === 0) {
                    tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="17">No entries found.</td></tr>');
                }
            });
        },
        init = function () {
            events();
            onLoading();
            realTimeTableSearch();
        }();
});