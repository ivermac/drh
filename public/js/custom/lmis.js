/*global
    $, jQuery,
    loadingImage,
    urlLoadingImage,
    urlGetLMISMOSData,
    urlGetDistrictsInCounty,
    urlLMISDataDelete,
    urlUploadProcessor,
*/
$(function () {
    'use strict';
    var file_name, url, data, csvregex,
        ajaxCall = function (url, data, callback, element_name, showLoading) {
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    if (showLoading)
                        $("#processing-modal").modal('show');
                },
                success: function (serverData) {
                    if (showLoading)
                        $('#lmis-graph-section').empty();
                    callback(serverData, element_name);
                },
                complete: function() {
                    if (showLoading)
                        $("#processing-modal").modal('hide');
                },
                error: function (xhr, text, error) {
                    console.log("xhr >>> ", xhr);
                    console.log("text >>> ", text);
                    console.log("error >>> ", error);
                },
            });
        },
        exportCSVToMysqlOutput = function(serverData, element_name) {
            $("[name='" + element_name + "']").append(
                $("<p>", { class: "pull-right" }).append(
                    $('<span>', { class: "badge" }).append(serverData)
                )
            );
        }, 
        fileUploader = function () {
            $('#LMIS-CSV-upload').fileupload({
                type: "POST",
                url: urlUploadProcessor,
                dataType: 'json',
                done: function (e, data) {
                    /*jslint unparam: true*/
                    $.each(data.result.files, function (index, file) {
                        $('<p/>', { name: file.name }).text(file.name).appendTo('#files');
                        file_name = data.files[0].name;
                        url = urlGetExportCsvToMysql;
                        data = {
                            filename: file_name,
                            module: "lmis"
                        };
                        ajaxCall(url, data, exportCSVToMysqlOutput, file.name, true);
                    });
                    /*jslint unparam: false*/
                },
                progress: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                },
                submit: function (e, data) {
                    // csv file format should be LMIS_YYYYMM.csv e.g. LMIS_201405.csv
                    csvregex = /[lL][mM][iI][sS]_20\d{2}(0[1-9]|1[0-1])\.[cC][sS][vV]/i;
                    for (var i = 0; i < data.files.length; i++) {
                        file_name = data.files[i].name;
                        if (!file_name.match(csvregex)) {
                            $("#non-csv").append(
                                $("<p>").append(file_name)
                            )
                            $("#non-csv").show();
                            return false;
                        }
                        $("#files").show();
                    };
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        },
        columnGraph = function(categories, data, graph_title) {
            $('#lmis-graph-section').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: graph_title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },    
                xAxis: {
                    categories: categories,
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                yAxis: {
                    // type: 'logarithmic',
                    min: 0,
                    title: {
                        text: 'Month of stock',
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span>'+
                                  '<table>',
                    pointFormat: '<tr>'+
                                    '<td style="color:{series.color};padding:0"><b>{point.y:.2f}</b></td>' +
                                  '</tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        // colorByPoint: true,
                        borderWidth: 0
                    }
                },
                legend: {
                    enabled: false,
                },
                credits: {
                    position: {
                        align: 'left',
                        x: 20
                    },
                    href: "https://hiskenya.org/dhis-web-commons/security/login.action;jsessionid=E3A9BAAED48BB18C9A379CEB5E07CCE7",
                    text: "Source: DHIS-2"
                },
                series: [{
                    data: data
                }]
            });
        },
        onloading = function() {
            var today = new Date().toJSON(),
                callback = function(data) {
                    columnGraph(data['commodities'], data['mos'], getColumnGraphTitle(data))
                };
                
                url = urlGetLMISMOSData;
                data = {
                    county: "all",
                    district: "all",
                    year: today.slice(0,4),
                    month: today.slice(5,7)
                }

                ajaxCall(url, data, callback, null, true)
        },
        getColumnGraphTitle = function(data) {
            var d = new Date(data['period']),
                month = {
                    0: "January",
                    1: "February",
                    2: "March",
                    3: "April",
                    4: "May",
                    5: "June",
                    6: "July",
                    7: "August",
                    8: "September",
                    9: "October",
                    10: "November",
                    11: "December"
                },
                title = "Month of stock - ";
            if (data['district'] !== "") {
                title += data['district'] + ", ";
            }
            if (data['county'] !== "") {
                title += data['county'] + " : ";
            } else {
                title += "All Counties" + " : ";
            }
            title += month[d.getMonth()] + ", " + d.getFullYear();
            return title;
        },
        events = function () {
            $("#lmis-filter").on("click", function() {
                var selectedCounty = $("#county :selected").val(),
                    selectedDistrict = $("#district :selected").val(),
                    selectedYear = $("#year :selected").val(),
                    selectedMonth = $("#month :selected").val(), 
                    callback = function(data) {
                        columnGraph(data['commodities'], data['mos'], getColumnGraphTitle(data))
                    };

                url = urlGetLMISMOSData;
                data = {
                    county: selectedCounty,
                    district: selectedDistrict,
                    year: selectedYear,
                    month: selectedMonth
                }

                ajaxCall(url, data, callback, null, true)
            });

            $("#county").on("change", function() {
                var selectedCounty = $(this).val(),
                    callback = function(data) {
                        $("#district").empty().append(
                            $("<option>", { value: "all", text: "--All Districts--"})
                        )

                        $.each(data, function(districtKey, districtValue) {
                            $("#district").append(
                                $("<option>", { value: districtKey, text: districtValue})
                            )
                        });
                    };
                if (selectedCounty !== "all") {
                    url = urlGetDistrictsInCounty;
                    data = {
                        "county-name": selectedCounty
                    }
                    
                    ajaxCall(url, data, callback, null, false)
                } else {
                    $("#district").empty().append(
                        $("<option>", { value: "all", text: "--All Districts--"})
                    )
                }
            });

            $("#delete-LMIS-period-data").on("click", function() {
                var period = $("#LMIS-year").val() + $("#LMIS-month").val(),
                    callback = function(data) {
                        var alert = $("#delete-modal-alert");
                        alert.empty()
                        if (data > 0) {
                            alert.append("Records Deleted: " + data)
                        } else {
                            alert.append("No Data for period submitted" )
                        }
                        alert.show();
                        onloading();
                    };
                url = urlLMISDataDelete;
                data = {
                    period: period
                };

                ajaxCall(url, data, callback);
            })
        },
        init = function() {
            $("#files").hide();
            $("#non-csv").hide();
            $("#delete-modal-alert").hide();
            
            events();
            onloading();
            fileUploader();
        }();
});