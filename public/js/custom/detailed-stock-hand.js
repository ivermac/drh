/*global 
    $, jQuery, 
    urlCalImagePath,
    urlGetStockOnHandRecord,
    urlDeleteStockOnHandRecord
*/
$(function () {
    'use strict';
    var
    	ajaxCall = function (url, data, callback) {
    	    $.ajax({
    	        type: "POST",
    	        url: url,
    	        dataType: 'json',
    	        data: data,
    	        beforeSend: function () {
    	        },
    	        success: function (data) {
    	            callback(data);
    	        },
    	        error: function (xhr, text, error) {
    	            console.log("xhr >>> ", xhr);
    	            console.log("text >>> ", text);
    	            console.log("error >>> ", error);
    	        },
    	    });
    	},
    	events = function () {
            $('.stock-on-hand-delete').on('click', function () {
            	var
                	recordId = $(this).attr('record-id');
                $('.delete-stock-on-hand-record').attr('href', urlDeleteStockOnHandRecord + "/" + recordId);
            });

            $('.stock-on-hand-edit').on('click', function() {
            	var 
            		id = $(this).attr('record-id'),
            		data = {
            			id: id
            		},
            		callback = function(response) {
            			console.log(response)
            			var 
            				commodity = "#commodity option[value='" + response['fpcommodity_Id'] + "']",
            				store = "#store option[value='" + response['store_id'] + "']";

            			$(commodity).attr("selected", "selected");
            			$(store).attr("selected", "selected");
            			$("#quantity").val(response['fp_quantity']);
            			$("#date-as-of").val(response['fp_date']);
            			$("[name='id']").val(response['id']);
            		};

            	ajaxCall(urlGetStockOnHandRecord, data, callback)
            })
        },
        onloading =  function() {
        	!function() {
	        	var 
	        		date_field = $("#date-as-of"),
	        		calendar_img;
	        	$("#date-as-of").datepicker({
	    		    showOn: "button",
	    		    buttonImage: urlCalImagePath,
	    		    buttonImageOnly: true,
	    		    dateFormat: "yy-mm-dd"
	    		})
	            
	            calendar_img = date_field.next();
	            date_field.parent().append($('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img));
            }();
        },
    	init = function() {
    		onloading();
    		events();
    	}();
});