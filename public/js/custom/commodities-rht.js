/*global 
    $, jQuery, 
    urlCalImagePath,
    urlGetDistrictsInCounty,
    urlGetFacilitiesInDistricts,
    urlDeleteRHTRecord,
    urlViewDetailOfRHTRecord
*/
$(function () {
    'use strict';
    var
        selected, url, recordId, data, row_num = 2,percentage_of_trained_hcw,
        percentage_of_facilities_with_atleast_one_person_trained, percentage_values,
        chart1, chart2, setChart, tab_width = $('#graph-1').width() - 20,
        ajaxCall = function (url, data, callback) {
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: data,
                beforeSend: function () {
                     $("#processing-modal").modal('show');
                },
                success: function (data) {
                     $("#processing-modal").modal('hide');
                    callback(data);
                },
                error: function (xhr, text, error) {
                    console.log("xhr >>> ", xhr);
                    console.log("text >>> ", text);
                    console.log("error >>> ", error);
                },
            });
        },
        onLoading = function () {
            var
                calendar_img;

            $(".rht-date").datepicker({
                showOn: "button",
                buttonImage: urlCalImagePath,
                buttonImageOnly: true,
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                yearRange: "1990:2014"
            }).each(function () {
                var
                    date_field = $(this),
                    calendar_img = $(this).next();

                date_field.parent().append($('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img));
            });
        },
        chosenDropdown = function () {
            var
                config = {
                    '.chosen-select'           : {},
                    '.chosen-select-deselect'  : { allow_single_deselect: true },
                    '.chosen-select-no-single' : { disable_search_threshold: 10 },
                    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                    '.chosen-select-width'     : { width: "95%" }
                }, selector;
            for (selector in config) {
                if (config.hasOwnProperty(selector)) {
                    $(selector).chosen(config[selector]);
                }
            }
        },
        appendDistricts = function (jsonData) {
            var
                districts = $('#district');
            districts.empty().append(
                $('<option>').val("")
            );
            $.each(jsonData, function (id, district) {
                districts.append(
                    $('<option>').val(id).text(district)
                );
            });
            districts.trigger("chosen:updated");
            chosenDropdown();
        },
        appendFacilities = function (jsonData) {
            var facility_code = $('#facility-code');
            facility_code.empty().append(
                $('<option>').val("")
            );
            $.each(jsonData, function (id, district) {
                facility_code.append(
                    $('<option>').val(id).text(district)
                );
            });
            facility_code.trigger("chosen:updated");
            chosenDropdown();
        },
        appendRHTDetails = function (data) {
            var
                html_objects = {
                    'modal-first-name': data.first_name,
                    'modal-surname': data.surname,
                    'modal-facility': data.facility_name,
                    'modal-personnel-no': data.personnel_no,
                    'modal-qualification': data.qualification,
                    'modal-station': data.station,
                },
                labels = ['default', 'primary', 'success', 'info', 'warning', 'danger'],
                count = 0,
                label;

            $.each(html_objects, function (identifier, value) {
                $('#' + identifier).empty().append(value);
            });

            $('#modal-training').empty();
            $.each(data.training, function (training) {
                label = $('<span>', {class : 'label label-' + labels[count], text : training});
                $('#modal-training').append(label);
                count = (count === 5) ? 0 : count + 1;
            });
        },
        cloning = function () {
            var
                collapsible = $('<div>', {class: 'panel panel-default'}),
                form_object = $("#rht-inputs").clone(true),
                clone_btns = $("#clone-btns").clone(true),
                date_field, calendar_img, splitted_name, new_name, input_field;
            form_object.attr('class', 'cloned-rht-inputs');

            form_object
                .find('.rht-date')
                    .removeAttr('id')
                    .removeClass('hasDatepicker')
                    .removeData('datepicker')
                    .unbind()
                    .datepicker({
                    showOn: "button",
                    buttonImage: urlCalImagePath,
                    buttonImageOnly: true,
                    dateFormat: "yy-mm-dd"
                }).each(function () {
                    date_field = $(this);
                    calendar_img = $(this).next();
                    date_field.parent()
                        .find('.datepicker-img').remove().end()
                        .append($('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img));
                }).end()
                .find('.training-inputs').each(function () {
                    input_field = $(this);
                    splitted_name = input_field.attr('name').split('[');
                    new_name = splitted_name[0] + "[" + row_num + "]";
                    input_field.attr('name', new_name).text('');
                }).end()
                .find('.rht-date, .rht-chkbox').each(function () {
                    input_field = $(this);
                    splitted_name = input_field.attr('name').split('[');
                    new_name = splitted_name[0] + "[" + splitted_name[1] + "[" + row_num + "]";
                    input_field.attr('name', new_name).text('');
                }).end();
                
            collapsible.append(
                $('<div>', {class: 'panel-heading'}).append(
                    $('<h4>', {class: "panel-title"}).append(
                        $('<a>', {
                            'data-toggle': 'collapse',
                            'data-parent': '#accordion',
                            'href': '#collapse' + row_num
                        }).text("Row "),
                        clone_btns
                    )
                )
            );
            collapsible.append(
                $('<div>', {id: 'collapse' + row_num, class: 'panel-collapse collapse'}).append(
                    $('<div>', {class: 'panel-body'}).append(form_object)
                )
            );
            row_num = row_num + 1;
            return collapsible;
        },
        
        drillDownColumnChart = function (drillDownData, selector, theTitle, theChart, training) {

            function setChart(name, categories, data, color, title) {
                theChart.xAxis[0].setCategories(categories);
                theChart.series[0].remove();
                theChart.setTitle({ text: title });
                
                theChart.addSeries({
                    name: name,
                    data: data,
                    color: 'rgb(47, 126, 216)'
                });
            }

            theChart = new Highcharts.Chart({
               chart: {
                  renderTo: selector, 
                  type: 'column',
                  width: ( selector === "atleast-one-person-trained-data" ) ? tab_width * 0.7 : tab_width,
               },
               title: {
                  text: theTitle,
                  style: {
                      fontFamily: "Century Gothic",
                      fontWeight: "normal",
                      fontSize: "15px",
                  },
               },
               xAxis: {
                  categories: true,
                  labels: {
                      style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "15px",
                        }
                  }
               },
               yAxis: {
                  title: {
                     text: ''
                  },
                  labels: {
                      style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "15px",
                        }
                  }
               },
               legend: {
                    enabled: false
               },
               plotOptions: {
                  column: {
                     cursor: 'pointer',
                     point: {
                        events: {
                           click: function() {
                              var drilldown = this.drilldown;
                              console.log("drilldown >>> ", drilldown);
                              if (drilldown) { // drill down
                                  setChart(drilldown.name, true, drilldown.data, drilldown.color, (theTitle + " in " +drilldown.name ));
                              } else { // restore
                                 setChart(name, true, drillDownData, null, theTitle);
                              }
                           },
                           mouseOver: function() {
                                if ( selector === "atleast-one-person-trained-data" ) {
                                    var postValues;
                                    if (this.drilldown) {
                                        postValues = {
                                            "region": this.drilldown.name,
                                            "level": this.drilldown.level
                                        }
                                       
                                    } else {
                                        postValues = {
                                            "region": this.name,
                                            "level": 3
                                        }
                                    }
                                    postValues["training"] = training;

                                    $.ajax({
                                        type: "POST",
                                        url: urlGetFacilityOwnershipRHTTrainingBreakdown,
                                        dataType: 'json',
                                        data: postValues,
                                        beforeSend: function () {
                                            $("#facility-ownership-breakdown").empty();
                                        },
                                        success: function (data) {
                                            $.each({"Training: ": training, "Region: ": postValues.region}, function(key, value) {
                                                $("#facility-ownership-breakdown").append(
                                                    $("<p>", { text: key }).append(
                                                        $("<span>", { class: "label label-primary", text: value })
                                                    )
                                                )
                                            })

                                            var badge = $("<ul>", { class: "nav nav-pills nav-stacked"});
                                            $.each(data, function(owner, total) {
                                                badge.append(
                                                    $("<li>", { class: "active" }).append(
                                                        $("<a>", { text: owner }).append(
                                                            $("<span>", { class: "badge pull-right", text: total })
                                                        )
                                                    )
                                                )
                                            });
                                            $("#facility-ownership-breakdown").append( badge )
                                        },
                                        error: function (xhr, text, error) {
                                            console.log("xhr >>> ", xhr);
                                            console.log("text >>> ", text);
                                            console.log("error >>> ", error);
                                        },
                                    }).done(function( data ) {
                                        if ( console && console.log ) {
                                            // console.log( "tooltip data:", data );
                                        }
                                        return data;
                                    });
                                }
                           }
                        }
                     },
                     dataLabels: {
                        enabled: true,
                        // color: colors[0],
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "15px",
                        },
                        formatter: function() {
                           return this.y +'%';
                        }
                     }               
                  }
               },
               tooltip: {
                  formatter: function() {
                     var point = this.point, s = '';

                        if (point.drilldown) {
                            s = point.name + ':<b>' + this.y + ' %</b><br/>';
                            s += 'Click to view ' + point.name + ' drilldown';
                        } else {
                            s = point.name + ':<b>' + this.y + '</b><br/>';
                            s += 'Click to return to initial view';
                        }

                    return s;
                  },
                  style: {
                      fontFamily: "Century Gothic",
                      fontWeight: "normal",
                      fontSize: "15px",
                  },
               },
               series: [{
                  name: name,
                  data: drillDownData,
               }],
               exporting: {
                  enabled: false
               }
            });
        },
        drillDownPieChart = function (drillDownData, selector, title, theChart) {
            setChart = function (name, categories, data, color, theChart) {
                theChart.series[0].remove();

                theChart.addSeries({
                    name: name,
                    data: data,
                    // pointPadding: -0.3,
                    borderWidth: 0,
                    pointWidth: 15,
                    shadow: false,
                    color: color || 'white'
                });

            },

            theChart = new Highcharts.Chart({
                
                chart: {
                    renderTo: selector,
                    type: 'pie',
                    /* changes bar size */
                    pointPadding: -0.3,
                    borderWidth: 0,
                    pointWidth: 10,
                    shadow: false,
                    // backgroundColor: '#e2dfd3'
                },
                title: {
                    text: title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },
                subtitle: {
                    text: 'Province, County, District Breakdown',
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "13px",
                    },
                },
                xAxis: {
                    categories: true
                },
                yAxis: {
                    title: {
                        text: 'Total Brand Value',
                        categories: true
                    }
                },
                //drilldown plot
                plotOptions: {
                    pie: {
                        cursor: 'pointer',
                        allowPointSelect: true,
                        point: {
                            events: {
                                click: function() {
                                    var drilldown = this.drilldown;
                                    if (drilldown) { // drill down
                                        setChart(drilldown.name, null, drilldown.data, drilldown.color, theChart);
                                    } else { // restore
                                        setChart("initial", null, drillDownData, null, theChart);
                                    }
                                }
                            }
                        },
                        dataLabels: {
                            enabled: true,
                            color: '#000',
                            style: {
                                fontFamily: "Century Gothic",
                                fontWeight: "normal",
                                fontSize: "13px",
                            },
                            //label colors
                            connectorColor: '#000',
                            // connector label colors
                            formatter: function() {
                                return this.point.name + ' (' + this.point.percentage.toFixed(1) + '%)';

                            }
                        }
                    }
                },
                //formatting over hover tooltip
                tooltip: {
                    formatter: function() {
                        var point = this.point,
                            s = "";
                        console.log("point >>> ", point)
                        if (point.drilldown) {
                            s = point.name + ':<b>' + this.y + '</b><br/>';
                            s += 'Click to view ' + point.name + ' drilldown';
                        } else {
                            s = point.name + ':<b>' + this.y + '</b><br/>';
                            s += 'Click to return to initial view';
                        }
                        return s;
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: name,
                    data: drillDownData,
                    /* changes bar size */
                    pointPadding: -0.3,
                    borderWidth: 0,
                    pointWidth: 15,
                    shadow: false,
                    color: 'black' //Sectors icon
                    }],
                exporting: {
                    enabled: true
                }
            });
        },
        trainingCallback = function (data) {
            console.log("data >>>", data);
            var row,
                tbody = $('#rht-dashboard-table-tbody');

            drillDownColumnChart(
                data['Personnel Data Drill Down'], 
                "personnel-training-data",
                "% of staff trained in " + data['Training'],
                chart1,
                data["Training"]
            )

            drillDownColumnChart(
                data['Atleast 1 Person Trained Drilldown'], 
                "atleast-one-person-trained-data",
                "% of facilities with atleast 1 person trained in " + data['Training'],
                chart2,
                data["Training"]
            )
            
            tbody.empty();
            $.each(data['rhtDashboardTable'], function(county, county_data) {
                row = $('<tr>').append(
                        $('<td>').text(county),
                        $('<td>').text(county_data['facilities in county']),
                        $('<td>').text(county_data['personnel in county']),
                        $('<td>').text(county_data['facilities with atleast one person trained']),
                        $('<td>').text(county_data['trained personnel']),
                        $('<td>').text(county_data['% facilites with trained personnel']),
                        $('<td>').text(county_data['% personnel trained'])
                    );
                tbody.append(row);
            });

            /*$('#rht-dashboard-table').dataTable( {
                "sScrollY": "300px",
               "bPaginate": false,
               "bScrollCollapse": true
            });*/
        },
        events = function () {
            $("[name^='training[']").on("change", function () {
                var checkbox = $(this);
                if (!checkbox.is(':checked')) {
                    checkbox.parent().next().val("");
                }
            });

            $("#county").on("change", function () {
                $.each([ 'district', 'facilities' ], function (value) {
                    $("#" + value).empty();
                });

                selected = $("#county :selected").val();
                url = urlGetDistrictsInCounty;
                data = {
                    id: selected,
                };
                ajaxCall(url, data, appendDistricts);
            });

            $("#trainings").on("change", function() {
                url = urlGetRHTDashboardData;
                selected = $("#trainings :selected").val();
                data = {
                    training: selected,
                };
                ajaxCall(url, data, trainingCallback);
            });

            $("#district").on("change", function () {
                selected = $("#district :selected").val();
                url = urlGetFacilitiesInDistricts;
                data = {
                    id: selected,
                };
                ajaxCall(url, data, appendFacilities);
            });

            $('#training-dropdown').on("change", function () {
                console.log("i changed :)");
            });

            $('.rht-detail').on('click', function () {
                var modal_objects = [
                    'modal-first-name',
                    'modal-surname',
                    'modal-facility',
                    'modal-personnel-no',
                    'modal-qualification',
                    'modal-station',
                    'modal-training',
                ];

                $.each(modal_objects, function (value) {
                    $('#' + value).empty().append("Loading...");
                });

                recordId = $(this).attr('record-id');
                url = urlViewDetailOfRHTRecord;
                data = {
                    id: recordId,
                };
                ajaxCall(url, data, appendRHTDetails);
            });

            $('.rht-delete').on('click', function () {
                recordId = $(this).attr('record-id');
                url = urlDeleteRHTRecord;
                $('.delete-rht-record').attr('href', url + "/" + recordId);
            });

            $('.btn-clone').on('click', function () {
                $('#accordion').append(cloning());
            });

            $('.btn-unclone').on('click', function () {
                var
                    parent_panel = $(this).closest(".panel"),
                    clone = parent_panel.find(".cloned-rht-inputs");
                if (clone.length > 0) {
                    parent_panel.remove();
                }
            });
        },
        realTimeTableSearch = function () {
            //something is entered in search form
            $('#system-search').keyup(function () {
                var
                    that = this,
                // affect all table rows on in systems table
                    tableBody = $('.table tbody'),
                    tableRowsClass = $('.table tbody tr'),
                    rowText, inputText;
                $('.search-sf').remove();
                tableRowsClass.each(function (i, val) {
                    //Lower text for case insensitive
                    rowText = $(val).text().toLowerCase();
                    inputText = $(that).val().toLowerCase();
                    if (inputText !== '') {
                        $('.search-query-sf').remove();
                        tableBody.prepend('<tr class="search-query-sf"><td colspan="17"><strong>Searching for: "'
                            + $(that).val()
                            + '"</strong></td></tr>');
                    } else {
                        $('.search-query-sf').remove();
                    }

                    if (rowText.indexOf(inputText) === -1) {
                        //hide rows
                        tableRowsClass.eq(i).hide();
                    } else {
                        $('.search-sf').remove();
                        tableRowsClass.eq(i).show();
                    }
                });
                //all tr elements are hidden
                if (tableRowsClass.children(':visible').length === 0) {
                    tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="17">No entries found.</td></tr>');
                }
            });
        },
        init = function () {
            chosenDropdown();
            realTimeTableSearch();
            events();
            onLoading();
        }();
});