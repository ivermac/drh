/*global 
    $, jQuery, 
    urlCalImagePath,
	urlGetStockStatusData,
	urlGetSupplyPlanVsMos,
	loadingImage
*/
var commodities_dashboard = (function() {
	'use strict';
    var
        dateSelected, url, recordId, data, row_num = 2, thisButton, store, container,
        ajaxCall = function (url, data, callback, container) {
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    $("#processing-modal").modal('show');
                },
                success: function (data) {
                    $("#processing-modal").modal('hide');
                    callback(data);
                },
                error: function (xhr, text, error) {
                    console.log("xhr >>> ", xhr);
                    console.log("text >>> ", text);
                    console.log("error >>> ", error);
                },
            });
        },

        demoCompoundBarGraph = function (jsonData) {
            var
                commodity_names = [],
                commodity_ids = [],
                actual_stock = [],
                pending_stock = [],
                implants = [];
            $.each(jsonData.commodities, function(id, name){
                if (!name.match(/Implants/)) {
                    commodity_names.push(name);
                    commodity_ids.push(id);
                } else {
                    implants.push(id);
                }
            });
            commodity_names.push('Implants');

            function setGraphData(json_stock, array_stock) {
                var implantsTotal = 0;
                $.each(commodity_ids, function (key, value) {
                    if (jsonData[json_stock][value] === null || jsonData[json_stock][value] === "" || jsonData[json_stock][value] === undefined) {
                        array_stock.push(0);
                    } else {
                        array_stock.push(parseFloat(jsonData[json_stock][value]));
                    }
                });
                $.each(implants, function (key, value) {
                    if (jsonData[json_stock][value] === null || jsonData[json_stock][value] === "" || jsonData[json_stock][value] === undefined) {
                        implantsTotal += 0;
                    } else {
                        implantsTotal += parseFloat(jsonData[json_stock][value]);
                    }
                });
                array_stock.push(implantsTotal);
            }
            $.each({"pending_stock": pending_stock, "actual_stock": actual_stock}, function(name, array) {
                setGraphData(name, array);
            })

            var theChart = new Highcharts.Chart({
            // $("#" + jsonData.container).highcharts({
                chart: {
                    type: 'bar',
                    renderTo: jsonData.container,
                },
                title: {
                    text: jsonData.title,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    },
                },
                subtitle: {
                    text: 'Source: nascop.org',
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "12px",
                    },
                },
                xAxis: {
                    categories: commodity_names,
                    title: {
                        text: null
                    },
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Months of Stock',
                        style: {
                                fontFamily: "Century Gothic",
                                fontWeight: "normal",
                                fontSize: "13px",
                        },
                        align: 'middle'
                    },
                    labels: {
                        overflow: 'justify',
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },

                    },
                    plotLines: [{
                        value: 16,
                        color: 'red',
                        width: 2,
                        label: {
                            text: 'Minimum Value',
                            align: 'left',
                            style: {
                                color: 'gray',
                                fontFamily: "Century Gothic",
                                fontWeight: "normal",
                                fontSize: "13px",
                            }
                        }
                    }, {
                        value: 22,
                        color: 'green',
                        width: 2,
                        label: {
                            text: 'Maximum Value',
                            align: 'left',
                            style: {
                                color: 'gray',
                                fontFamily: "Century Gothic",
                                fontWeight: "normal",
                                fontSize: "13px",
                            }
                        }
                    }]  
                },
                tooltip: {
                    valueSuffix: '(MoS)'
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                legend: {
                    borderWidth: 1,
                    borderRadius: 0,
                    backgroundColor: '#FFFFFF',
                    shadow: true,
                    itemStyle: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "13px",
                    },
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Pending Stock',
                    data: pending_stock
                }, {
                    name: 'Actual Stock',
                    data: actual_stock
                }]
            });
            return theChart;
        },
        demoLineAndBarGraph = function(jsonData) {
            $('#supply-plan-vs-mos').highcharts({
                chart: {
                    zoomType: 'xy',
                },
                title: {
                    text: 'Supply Plan vs Actual MOS for '+jsonData.commodity,
                    style: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "18px",
                    }
                },
                xAxis: {
                    categories: jsonData.fiscalYear,
                    labels: {
                        style: {
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                    },
                },
                yAxis: { 
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1],
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        }
                    },
                    title: {
                        text: 'Months of Stock',
                        style: {
                            color: Highcharts.getOptions().colors[1],
                            fontFamily: "Century Gothic",
                            fontWeight: "normal",
                            fontSize: "13px",
                        },
                        align: 'middle'
                    }
                },
                tooltip: {
                    shared: true
                },
                legend: {
                    borderRadius: 0,
                    shadow: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
                    itemStyle: {
                        fontFamily: "Century Gothic",
                        fontWeight: "normal",
                        fontSize: "13px",
                    },
                },
                series: [{
                    name: 'Stock of Hand',
                    type: 'column',
                    
                    data: jsonData.SOHKEMSA,
                    tooltip: {
                        valueSuffix: ' MOS'
                    }
        
                }, {
                    name: 'Supply Plan',
                    type: 'spline',
                    data: jsonData.SPKEMSA,
                    tooltip: {
                        valueSuffix: 'MOS'
                    }
                }]
            });
        },
        onLoading = function () {
            dateSelected = new Date().toJSON().slice(0,10);
            $.each({'public-sector':'kemsa', 'private-sector':'psi'}, function(container, value) {
                url = urlGetStockStatusData;
                data = {
                    dateSelected: dateSelected,
                    supplier: value,
                };
                ajaxCall(url, data, demoCompoundBarGraph, container);
            });

            url = urlGetSupplyPlanVsMos;
            data = {
                year: 2013,
                commodity_id: 2,
            };
            ajaxCall(url, data, demoLineAndBarGraph);
        },
        chosenDropdown = function () {
            var
                config = {
                    '.chosen-select'           : {},
                    '.chosen-select-deselect'  : { allow_single_deselect: true },
                    '.chosen-select-no-single' : { disable_search_threshold: 10 },
                    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                    '.chosen-select-width'     : { width: "95%" }
                }, selector;
            for (selector in config) {
                if (config.hasOwnProperty(selector)) {
                    $(selector).chosen(config[selector]);
                }
            }
        },
        events = function () {
            $('#fpcommodities').on('hidden.bs.modal', function (e) {
                $("[name='commodity-name']").val("");
                $("[name='commodity-description']").val("");
                $("[name='commodity-unit']").val("");
                $("[name='monthly-consumption-kemsa']").val("");
                $("[name='monthly-consumption-psi']").val("");
                $("[name='commodity-date-as-of']").val("");
            });

             $('#funding-source').on('hidden.bs.modal', function (e) {
                $("[name='funding-source']").val("");
                $("[name='procuring-agency']").val("");
                $("[name='service-status']").val("");
            })

            $(".edit-funding-source").on("click", function() {
                var field = $(this),
                    funding_source_id = field.attr('funding-source-id'),
                    funding_source_name = field.attr('funding-source-name'),
                    procuring_agency = field.attr('procuring-agency'),
                    service_status = field.attr('service-status');

                $("[name='funding-source']").val(funding_source_name);
                $("[name='procuring-agency']").val(procuring_agency);
                $("[name='service-status']").val(procuring_agency);

                $('#funding-source-modal-body').append(
                    $("<input>", { type: "hidden", value: funding_source_id, name: "funding-source-id"})
                );

                $('#funding-source').modal('show')
            });

            $(".edit-commodity").on("click", function() {
                var field = $(this),
                    commodity_id = field.attr('commodity-id'),
                    commodity_name = field.attr('commodity-name'),
                    description = field.attr('description'),
                    unit = field.attr('unit'),
                    kemsa = field.attr('kemsa'),
                    psi = field.attr('psi'),
                    as_of = field.attr('as-of');

                $("[name='commodity-name']").val(commodity_name);
                $("[name='commodity-description']").val(description);
                $("[name='commodity-unit']").val(unit);
                $("[name='monthly-consumption-kemsa']").val(kemsa);
                $("[name='monthly-consumption-psi']").val(psi);
                $("[name='commodity-date-as-of']").val(as_of);

                $('#commodity-body-modal').append(
                    $("<input>", { type: "hidden", value: commodity_id, name: "commodity-id"})
                );

                $('#fpcommodities').modal('show')
            });

            $(".delete-funding-source").on("click", function() {
                var delete_url = $(this).attr('delete-url');
                $("#delete-funding-source-btn").attr("href", delete_url);
                $('#delete-modal').modal('show')
            });

            $(".delete-commodity").on("click", function() {
                var delete_url = $(this).attr('delete-url');
                $("#delete-commodity-btn").attr("href", delete_url);
                $('#delete-modal').modal('show')
            })

            $(".stock-status-filter").on("click", function() {
            	thisButton = $(this),
            	dateSelected = $("#"+thisButton.attr('date-filter')).val(),
            	store = thisButton.attr('store'),
            	container = thisButton.attr('container');;

                url = urlGetStockStatusData;
                data = {
                    dateSelected: dateSelected,
                    supplier: store,
                };
                ajaxCall(url, data, demoCompoundBarGraph, container);
            });

            $("#commodity-date-as-of").datepicker({
                showOn: "button",
                buttonImage: urlCalImagePath,
                buttonImageOnly: true,
                dateFormat: "yy-mm-dd"
            })
            var commodity_datepicker = $("#commodity-date-as-of"),
                commodity_datepicker_img = commodity_datepicker.next();
            commodity_datepicker.parent().append($('<span>', {class : 'input-group-addon datepicker-img'}).append(commodity_datepicker_img));

            $(".stock-status-date-filter").datepicker({
                showOn: "button",
                buttonImage: urlCalImagePath,
                buttonImageOnly: true,
                dateFormat: "yy-mm-dd"
            }).each(function () {
                var
                    date_field = $(this),
                    dateToday = new Date().toJSON().slice(0,10),
                    calendar_img = $(this).next();
                date_field.val(dateToday)
                    .parent().append(
                        $('<span>', {class : 'input-group-addon datepicker-img'}).append(calendar_img)
                    );
            });

            $("#filter-supply-plan-vs-mos").on("click", function() {
                var
                    commodity = $("#fpc").val(),
                    fiscal_year = $("#fiscal_years").val();

                url = urlGetSupplyPlanVsMos;
                data = {
                    year: (fiscal_year === "" || fiscal_year == 0 || fiscal_year === null) ? new Date().toJSON().slice(0,4) : fiscal_year,
                    commodity_id: (commodity === "" || commodity == 0 || commodity === null) ? 1 : commodity,
                };
                ajaxCall(url, data, demoLineAndBarGraph);
            });
        },
        test = function() {
            console.log("this is a test call")
        },
        init = function () {
            chosenDropdown();
            events();
            onLoading();
        };
        return { 
            init:init,
            demoCompoundBarGraph:demoCompoundBarGraph,
            test:test
        };
}());